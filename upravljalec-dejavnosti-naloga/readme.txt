
# Create database
CREATE DATABASE upravljalec_dejavnosti_naloga CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

# Build project
mvn clean package

# Create table in db
updateDb.bat

# Build docker image
docker build . -t upravljalec-dejavnosti-naloga:1.0

# Create container and run it (change datasource url, credentials, ports and KLIENTI_OSEBA_SERVICE_URL)
docker run --name upravljalec-dejavnosti-naloga-1-0 -e SPRING_DATASOURCE_URL='jdbc:mysql://172.17.0.2:3306/upravljalec_dejavnosti_naloga?useUnicode=true&characterEncoding=utf8&serverTimezone=UTC' -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=password1! -e KLIENTI_OSEBA_SERVICE_URL=172.17.0.3 -e KLIENTI_OSEBA_SERVICE_PORT=6560 -e GRPC_PORT=6563 -d -p 6563:6563 upravljalec-dejavnosti-naloga:1.0
