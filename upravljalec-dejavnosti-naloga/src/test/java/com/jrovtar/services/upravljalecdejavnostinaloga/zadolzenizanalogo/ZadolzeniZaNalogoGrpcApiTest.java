package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "klienti.osebaServicePort=6565",
        "klienti.osebaServiceUrl=localhost",
        "grpc.port=0",
        "grpc.shutdownGrace=-1"
})
public class ZadolzeniZaNalogoGrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private ZadolzeniZaNalogoService zadolzeniZaNalogoService;

    private NalogaZadolzeniServiceGrpc.NalogaZadolzeniServiceFutureStub zadolzeniServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        zadolzeniServiceFutureStub = NalogaZadolzeniServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void dodaj_ok() {

        // priprava testnih poatkov
        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setNalogaId(2);
        zznTest.setOsebaId(3);
        zznTest.setDodeljeneUre(BigDecimal.valueOf(11.5));

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoService.dodaj(ArgumentMatchers.any(ZadolzeniZaNalogoDTO.class)))
                .thenAnswer((Answer<ZadolzeniZaNalogo>) invocation -> {
                    ZadolzeniZaNalogoDTO dto = invocation.getArgument(0);
                    ZadolzeniZaNalogo z = new ZadolzeniZaNalogo();
                    z.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
                    z.getZadolzeniZaNalogoPk().setNalogaId(dto.getNalogaId());
                    z.getZadolzeniZaNalogoPk().setOsebaId(dto.getOsebaId());
                    z.setDodeljeneUre(dto.getDodeljeneUre());
                    return z;
                });

        // klic testirane metode
        NalogaZadolzeniDodajZahteva zahteva = NalogaZadolzeniDodajZahteva.newBuilder()
                .setOsebaId(zznTest.getOsebaId())
                .setNalogaId(zznTest.getNalogaId())
                .setDodeljeneUre(zznTest.getDodeljeneUre().doubleValue())
                .build();
        NalogaZadolzeniOdgovor odgovor = zadolzeniServiceFutureStub.dodaj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(zznTest.getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(zznTest.getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals(0, zznTest.getDodeljeneUre().compareTo(BigDecimal.valueOf(odgovor.getDodeljeneUre())));
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_napaka() {

        // priprava testnih poatkov
        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setNalogaId(2);
        zznTest.setOsebaId(3);
        zznTest.setDodeljeneUre(BigDecimal.valueOf(11.5));

        Exception exception = new NalogaNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoService.dodaj(ArgumentMatchers.any(ZadolzeniZaNalogoDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        NalogaZadolzeniDodajZahteva zahteva = NalogaZadolzeniDodajZahteva.newBuilder()
                .setOsebaId(zznTest.getOsebaId())
                .setNalogaId(zznTest.getNalogaId())
                .setDodeljeneUre(zznTest.getDodeljeneUre().doubleValue())
                .build();
        NalogaZadolzeniOdgovor odgovor = zadolzeniServiceFutureStub.dodaj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getNalogaId());
        Assert.assertEquals(0, odgovor.getOsebaId());
        Assert.assertEquals(0.0, odgovor.getDodeljeneUre(), 0);
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_ok() {

        // priprava testnih poatkov
        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
        zznTest.getZadolzeniZaNalogoPk().setNalogaId(2);
        zznTest.getZadolzeniZaNalogoPk().setOsebaId(3);
        zznTest.setDodeljeneUre(BigDecimal.valueOf(11.5));

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoService.izbrisi(ArgumentMatchers.any(ZadolzeniZaNalogoDTO.class)))
                .thenAnswer((Answer<ZadolzeniZaNalogo>) invocation -> zznTest);

        // klic testirane metode
        NalogaZadolzeniIzbrisiZahteva zahteva = NalogaZadolzeniIzbrisiZahteva.newBuilder()
                .setOsebaId(zznTest.getZadolzeniZaNalogoPk().getOsebaId())
                .setNalogaId(zznTest.getZadolzeniZaNalogoPk().getNalogaId())
                .build();
        NalogaZadolzeniOdgovor odgovor = zadolzeniServiceFutureStub.izbrisi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(zznTest.getZadolzeniZaNalogoPk().getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(zznTest.getZadolzeniZaNalogoPk().getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_napaka() {

        // priprava testnih poatkov
        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
        zznTest.getZadolzeniZaNalogoPk().setNalogaId(2);
        zznTest.getZadolzeniZaNalogoPk().setOsebaId(3);
        zznTest.setDodeljeneUre(BigDecimal.valueOf(11.5));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoService.izbrisi(ArgumentMatchers.any(ZadolzeniZaNalogoDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        NalogaZadolzeniIzbrisiZahteva zahteva = NalogaZadolzeniIzbrisiZahteva.newBuilder()
                .setOsebaId(zznTest.getZadolzeniZaNalogoPk().getOsebaId())
                .setNalogaId(zznTest.getZadolzeniZaNalogoPk().getNalogaId())
                .build();
        NalogaZadolzeniOdgovor odgovor = zadolzeniServiceFutureStub.izbrisi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getNalogaId());
        Assert.assertEquals(0, odgovor.getOsebaId());
        Assert.assertEquals(0.0, odgovor.getDodeljeneUre(), 0);
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_ok() {

        // priprava testnih poatkov
        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setNalogaId(2);
        zznTest.setOsebaId(3);
        zznTest.setDodeljeneUre(BigDecimal.valueOf(11.5));

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoService.posodobi(ArgumentMatchers.any(ZadolzeniZaNalogoDTO.class)))
                .thenAnswer((Answer<ZadolzeniZaNalogo>) invocation -> {
                    ZadolzeniZaNalogoDTO dto = invocation.getArgument(0);
                    ZadolzeniZaNalogo z = new ZadolzeniZaNalogo();
                    z.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
                    z.getZadolzeniZaNalogoPk().setNalogaId(dto.getNalogaId());
                    z.getZadolzeniZaNalogoPk().setOsebaId(dto.getOsebaId());
                    z.setDodeljeneUre(dto.getDodeljeneUre());
                    return z;
                });

        // klic testirane metode
        NalogaZadolzeniPosodobiZahteva zahteva = NalogaZadolzeniPosodobiZahteva.newBuilder()
                .setOsebaId(zznTest.getOsebaId())
                .setNalogaId(zznTest.getNalogaId())
                .setDodeljeneUre(zznTest.getDodeljeneUre().doubleValue())
                .build();
        NalogaZadolzeniOdgovor odgovor = zadolzeniServiceFutureStub.posodobi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(zznTest.getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(zznTest.getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals(0, zznTest.getDodeljeneUre().compareTo(BigDecimal.valueOf(odgovor.getDodeljeneUre())));
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_napaka() {

        // priprava testnih poatkov
        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setNalogaId(2);
        zznTest.setOsebaId(3);
        zznTest.setDodeljeneUre(BigDecimal.valueOf(11.5));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoService.posodobi(ArgumentMatchers.any(ZadolzeniZaNalogoDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        NalogaZadolzeniPosodobiZahteva zahteva = NalogaZadolzeniPosodobiZahteva.newBuilder()
                .setOsebaId(zznTest.getOsebaId())
                .setNalogaId(zznTest.getNalogaId())
                .setDodeljeneUre(zznTest.getDodeljeneUre().doubleValue())
                .build();
        NalogaZadolzeniOdgovor odgovor = zadolzeniServiceFutureStub.posodobi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getNalogaId());
        Assert.assertEquals(0, odgovor.getOsebaId());
        Assert.assertEquals(0.0, odgovor.getDodeljeneUre(), 0);
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }
}
