package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaRepository;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogo;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoPk;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoRepository;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RunWith(SpringRunner.class)
public class NalogaServiceImplTests {

    @MockBean
    private NalogaRepository nalogaRepository;

    @MockBean
    private PorabaCasaRepository porabaCasaRepository;

    @MockBean
    private ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository;

    private NalogaService nalogaService;

    @Before
    public void init() {
        nalogaService = new NalogaServiceImpl(
                nalogaRepository,
                zadolzeniZaNalogoRepository,
                porabaCasaRepository
        );
    }

    @SneakyThrows
    @Test
    public void dodaj_ok() {

        // priprava testnih podatkov
        NalogaDTO dto = new NalogaDTO();
        dto.setNaziv("Naziv");
        dto.setDatumZacetka(LocalDate.now());
        dto.setDatumZakljucka(LocalDate.now());

        // mock metod
        Mockito
                .when(nalogaRepository.save(ArgumentMatchers.any(Naloga.class)))
                .thenAnswer((Answer<Naloga>) invocation -> {
                            Naloga s = invocation.getArgument(0);
                            s.setId(new Random().nextInt(10));
                            return s;
                        }
                );

        // klic testirane metode
        Naloga naloga = nalogaService.dodaj(dto);

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).save(ArgumentMatchers.any(Naloga.class));
        Assert.assertNotNull(naloga.getId());
        Assert.assertEquals(dto.getNaziv(), naloga.getNaziv());
        Assert.assertEquals(dto.getDatumZacetka(), naloga.getDatumZacetka());
        Assert.assertEquals(dto.getDatumZakljucka(), naloga.getDatumZakljucka());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_null() {

        nalogaService.dodaj(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_brezAnotacijeValidated() {

        Method method = NalogaServiceImpl.class.getDeclaredMethod("dodaj", NalogaDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(NalogaDTO.ObDodajanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void vrni_ok_vrniZadolzene() {

        // priprava testnih podatkov
        Naloga naloga = new Naloga();
        naloga.setId(1);
        naloga.setNaziv("Naloga");
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        ZadolzeniZaNalogo zzn1 = new ZadolzeniZaNalogo();
        zzn1.setNalogaId(naloga.getId());
        zzn1.setOsebaId(2);
        zzn1.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
        zzn1.getZadolzeniZaNalogoPk().setNalogaId(zzn1.getNalogaId());
        zzn1.getZadolzeniZaNalogoPk().setOsebaId(zzn1.getOsebaId());
        zzn1.setDodeljeneUre(BigDecimal.valueOf(10.4));

        List<ZadolzeniZaNalogo> zznList = List.of(zzn1);

        PorabaCasa pc = new PorabaCasa();
        pc.setId(3);
        pc.setOsebaId(zzn1.getOsebaId());
        pc.setNalogaId(zzn1.getNalogaId());
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(BigDecimal.valueOf(5));

        List<PorabaCasa> pcList = List.of(pc);

        // mock metod
        Mockito.when(nalogaRepository.findById(naloga.getId()))
                .thenReturn(java.util.Optional.of(naloga));

        Mockito.when(zadolzeniZaNalogoRepository.findByNalogaId(naloga.getId()))
                .thenReturn(zznList);

        Mockito.when(porabaCasaRepository.findByNalogaIdAndOsebaId(zzn1.getNalogaId(), zzn1.getOsebaId()))
                .thenReturn(pcList);

        // klic testirane metode
        Naloga n = nalogaService.vrni(naloga.getId(), true);

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).findById(naloga.getId());
        Mockito.verify(zadolzeniZaNalogoRepository, Mockito.times(1)).findByNalogaId(naloga.getId());
        Mockito.verify(porabaCasaRepository, Mockito.times(1))
                .findByNalogaIdAndOsebaId(zzn1.getNalogaId(), zzn1.getOsebaId());
        Assert.assertEquals(naloga.getId(), n.getId());
        Assert.assertEquals(naloga.getNaziv(), n.getNaziv());
        Assert.assertEquals(naloga.getDatumZacetka(), n.getDatumZacetka());
        Assert.assertEquals(naloga.getDatumZakljucka(), n.getDatumZakljucka());
        Assert.assertNotNull(n.getZadolzeniZaNalogoList());
        Assert.assertEquals(zznList.size(), n.getZadolzeniZaNalogoList().size());

        for (int i = 0; i < zznList.size(); i++) {

            Assert.assertEquals(
                    zznList.get(i).getZadolzeniZaNalogoPk().getNalogaId(),
                    n.getZadolzeniZaNalogoList().get(i).getZadolzeniZaNalogoPk().getNalogaId());

            Assert.assertEquals(
                    zznList.get(i).getZadolzeniZaNalogoPk().getOsebaId(),
                    n.getZadolzeniZaNalogoList().get(i).getZadolzeniZaNalogoPk().getOsebaId());

            Assert.assertEquals(
                    zznList.get(i).getDodeljeneUre(),
                    n.getZadolzeniZaNalogoList().get(i).getDodeljeneUre());

            Assert.assertNotNull(n.getZadolzeniZaNalogoList().get(0).getPorabaCasaList());
            Assert.assertEquals(
                    zznList.get(i).getPorabaCasaList().size(),
                    n.getZadolzeniZaNalogoList().get(i).getPorabaCasaList().size());

            List<PorabaCasa> pcListTest = zznList.get(i).getPorabaCasaList();
            List<PorabaCasa> pcListResTest = n.getZadolzeniZaNalogoList().get(i).getPorabaCasaList();

            for (int j = 0; j < pcListTest.size(); j++) {
                Assert.assertEquals(pcListTest.get(j).getId(), pcListResTest.get(j).getId());
                Assert.assertEquals(pcListTest.get(j).getNalogaId(), pcListResTest.get(j).getNalogaId());
                Assert.assertEquals(pcListTest.get(j).getOsebaId(), pcListResTest.get(j).getOsebaId());
                Assert.assertEquals(pcListTest.get(j).getDatum(), pcListResTest.get(j).getDatum());
                Assert.assertEquals(pcListTest.get(j).getSteviloUr(), pcListResTest.get(j).getSteviloUr());
            }
        }
    }

    @SneakyThrows
    @Test
    public void vrni_ok_neVrniZadolzene() {

        // priprava testnih podatkov
        Naloga naloga = new Naloga();
        naloga.setId(1);
        naloga.setNaziv("Naloga");
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        ZadolzeniZaNalogo zzn1 = new ZadolzeniZaNalogo();
        zzn1.setNalogaId(naloga.getId());
        zzn1.setOsebaId(2);
        zzn1.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
        zzn1.getZadolzeniZaNalogoPk().setNalogaId(zzn1.getNalogaId());
        zzn1.getZadolzeniZaNalogoPk().setOsebaId(zzn1.getOsebaId());
        zzn1.setDodeljeneUre(BigDecimal.valueOf(10.4));

        List<ZadolzeniZaNalogo> zznList = List.of(zzn1);

        PorabaCasa pc = new PorabaCasa();
        pc.setId(3);
        pc.setOsebaId(zzn1.getOsebaId());
        pc.setNalogaId(zzn1.getNalogaId());
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(BigDecimal.valueOf(5));

        List<PorabaCasa> pcList = List.of(pc);

        // mock metod
        Mockito.when(nalogaRepository.findById(naloga.getId()))
                .thenReturn(java.util.Optional.of(naloga));

        Mockito.when(zadolzeniZaNalogoRepository.findByNalogaId(naloga.getId()))
                .thenReturn(zznList);

        Mockito.when(porabaCasaRepository.findByNalogaIdAndOsebaId(zzn1.getNalogaId(), zzn1.getOsebaId()))
                .thenReturn(pcList);

        // klic testirane metode
        Naloga n = nalogaService.vrni(naloga.getId(), false);

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).findById(naloga.getId());
        Mockito.verify(zadolzeniZaNalogoRepository, Mockito.times(0)).findByNalogaId(naloga.getId());
        Mockito.verify(porabaCasaRepository, Mockito.times(0))
                .findByNalogaIdAndOsebaId(zzn1.getNalogaId(), zzn1.getOsebaId());
        Assert.assertEquals(naloga.getId(), n.getId());
        Assert.assertEquals(naloga.getNaziv(), n.getNaziv());
        Assert.assertEquals(naloga.getDatumZacetka(), n.getDatumZacetka());
        Assert.assertEquals(naloga.getDatumZakljucka(), n.getDatumZakljucka());
        Assert.assertNull(n.getZadolzeniZaNalogoList());
    }

    @SneakyThrows
    @Test(expected = NalogaNeObstajaException.class)
    public void vrni_neObstaja() {

        nalogaService.vrni(0, true);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void vrni_idNull() {

        nalogaService.vrni(null, true);
    }

    @SneakyThrows
    @Test
    public void posodobi_ok() {

        // priprava testnih podatkov
        Naloga naloga = new Naloga();
        naloga.setId(1);
        naloga.setNaziv("Naloga");
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        NalogaDTO dto = NalogaDTO.kreirajDTO(naloga);

        // mock metod
        Mockito.when(nalogaRepository.findById(naloga.getId()))
                .thenReturn(Optional.of(naloga));

        Mockito.when(nalogaRepository.save(ArgumentMatchers.any(Naloga.class)))
                .thenReturn(naloga);

        // klic testirane metode
        Naloga n = nalogaService.posodobi(dto);

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).findById(naloga.getId());
        Assert.assertEquals(naloga.getId(), n.getId());
        Assert.assertEquals(naloga.getNaziv(), n.getNaziv());
        Assert.assertEquals(naloga.getDatumZacetka(), n.getDatumZacetka());
        Assert.assertEquals(naloga.getDatumZakljucka(), n.getDatumZakljucka());
        Assert.assertNull(n.getZadolzeniZaNalogoList());
    }

    @SneakyThrows
    @Test(expected = NalogaNeObstajaException.class)
    public void posodobi_neObstaja() {

        // priprava testnih podatkov
        NalogaDTO dto = new NalogaDTO();
        dto.setId(1);
        dto.setNaziv("Naloga");
        dto.setDatumZacetka(LocalDate.now());
        dto.setDatumZakljucka(LocalDate.now());

        // klic testirane metode
        nalogaService.posodobi(dto);

    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void posodobi_null() {

        nalogaService.posodobi(null);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeValidated() {

        Method method = NalogaServiceImpl.class.getDeclaredMethod("posodobi", NalogaDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(NalogaDTO.ObPosodabljanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void izbrisi_ok() {

        // priprava testnih podatkov
        Naloga naloga = new Naloga();
        naloga.setId(1);
        naloga.setNaziv("Naloga");
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        ZadolzeniZaNalogo zzn1 = new ZadolzeniZaNalogo();
        zzn1.setNalogaId(naloga.getId());
        zzn1.setOsebaId(2);
        zzn1.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
        zzn1.getZadolzeniZaNalogoPk().setNalogaId(zzn1.getNalogaId());
        zzn1.getZadolzeniZaNalogoPk().setOsebaId(zzn1.getOsebaId());
        zzn1.setDodeljeneUre(BigDecimal.valueOf(10.4));

        List<ZadolzeniZaNalogo> zznList = List.of(zzn1);

        PorabaCasa pc = new PorabaCasa();
        pc.setId(3);
        pc.setOsebaId(zzn1.getOsebaId());
        pc.setNalogaId(zzn1.getNalogaId());
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(BigDecimal.valueOf(5));

        List<PorabaCasa> pcList = List.of(pc);

        // mock metod
        Mockito.when(nalogaRepository.findById(naloga.getId()))
                .thenReturn(java.util.Optional.of(naloga));

        Mockito.when(zadolzeniZaNalogoRepository.findByNalogaId(naloga.getId()))
                .thenReturn(zznList);

        Mockito.when(porabaCasaRepository.findByNalogaIdAndOsebaId(zzn1.getNalogaId(), zzn1.getOsebaId()))
                .thenReturn(pcList);

        // klic testirane metode
        Naloga n = nalogaService.izbrisi(naloga.getId());

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).findById(naloga.getId());
        Mockito.verify(zadolzeniZaNalogoRepository, Mockito.times(1)).findByNalogaId(naloga.getId());
        Mockito.verify(porabaCasaRepository, Mockito.times(1))
                .findByNalogaIdAndOsebaId(zzn1.getNalogaId(), zzn1.getOsebaId());
        Mockito.verify(porabaCasaRepository, Mockito.times(1)).deleteById(pc.getId());
        Mockito.verify(zadolzeniZaNalogoRepository, Mockito.times(1)).deleteById(zzn1.getZadolzeniZaNalogoPk());

        Assert.assertEquals(naloga.getId(), n.getId());
        Assert.assertEquals(naloga.getNaziv(), n.getNaziv());
        Assert.assertEquals(naloga.getDatumZacetka(), n.getDatumZacetka());
        Assert.assertEquals(naloga.getDatumZakljucka(), n.getDatumZakljucka());
        Assert.assertNotNull(n.getZadolzeniZaNalogoList());
        Assert.assertEquals(zznList.size(), n.getZadolzeniZaNalogoList().size());
    }

    @SneakyThrows
    @Test(expected = NalogaNeObstajaException.class)
    public void izbrisi_neObstaja() {

        // klic testirane metode
        nalogaService.izbrisi(1);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void izbrisi_idNull() {

        // klic testirane metode
        nalogaService.izbrisi(null);
    }

    @SneakyThrows
    @Test
    public void izbrisi_brezAnotacijeTransactional() {

        Method method = NalogaServiceImpl.class.getDeclaredMethod("izbrisi", Integer.class);
        Transactional annotation = method.getAnnotation(Transactional.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(Propagation.REQUIRED, annotation.propagation());
    }

    @Test
    public void filtriraj_ok_filtriNull() {

        // pripava testnih podatkov
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        Object[] o1 = new Object[3];
        o1[0] = "Naziv 1";
        o1[1] = new BigDecimal(20.4);
        o1[2] = new BigDecimal(15.2);

        Object[] o2 = new Object[3];
        o2[0] = "Naziv 2";
        o2[1] = new BigDecimal(134.2);
        o2[2] = new BigDecimal(78.7);

        List<Object[]> testList = List.of(o1, o2);

        // mock metod
        Mockito.when(nalogaRepository.filtriraj(null, null, null, null, null, null))
                .thenReturn(testList);

        // klic testirane metode
        List<FiltrirajNalogeDTO> list = nalogaService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).filtriraj(null, null, null, null, null, null);
        Assert.assertNotNull(list);
        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i)[0], list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i)[1], list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i)[2], list.get(i).getOpravljeneUre());
        }
    }

    @Test
    public void filtriraj_ok_filtriNotNull() {

        // pripava testnih podatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = -1.0;
        Double maxDodeljeneUre = -1.0;
        Double minOpravljeneUre = -1.0;
        Double maxOpravljeneUre = -1.0;

        Object[] o1 = new Object[3];
        o1[0] = "Naziv 1";
        o1[1] = new BigDecimal(20.4);
        o1[2] = new BigDecimal(15.2);

        Object[] o2 = new Object[3];
        o2[0] = "Naziv 2";
        o2[1] = new BigDecimal(134.2);
        o2[2] = new BigDecimal(78.7);

        List<Object[]> testList = List.of(o1, o2);

        // mock metod
        Mockito.when(nalogaRepository.filtriraj(null, null, null, null, null, null))
                .thenReturn(testList);

        // klic testirane metode
        List<FiltrirajNalogeDTO> list = nalogaService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(nalogaRepository, Mockito.times(1)).filtriraj(null, null, null, null, null, null);
        Assert.assertNotNull(list);
        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i)[0], list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i)[1], list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i)[2], list.get(i).getOpravljeneUre());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_ok_napakaDodeljeneUre() {

        // pripava testnih podatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = 2.0;
        Double maxDodeljeneUre = 1.0;
        Double minOpravljeneUre = -1.0;
        Double maxOpravljeneUre = -1.0;

        // klic testirane metode
        nalogaService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_ok_napakaOpravljeneUre() {

        // pripava testnih podatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = -2.0;
        Double maxDodeljeneUre = -1.0;
        Double minOpravljeneUre = 3.0;
        Double maxOpravljeneUre = 1.0;

        // klic testirane metode
        nalogaService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre);
    }
}
