package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.klienti.OsebaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.Naloga;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaRepository;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaRepository;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class ZadolzeniZaNalogoServiceImplTests {

    @MockBean
    private NalogaRepository nalogaRepository;

    @MockBean
    private ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository;

    @MockBean
    private OsebaServiceKlient osebaServiceKlient;

    @MockBean
    private PorabaCasaRepository porabaCasaRepository;

    private ZadolzeniZaNalogoService zadolzeniZaNalogoService;

    @Before
    public void init() {
        zadolzeniZaNalogoService = new ZadolzeniZaNalogoServiceImpl(
                nalogaRepository,
                zadolzeniZaNalogoRepository,
                osebaServiceKlient,
                porabaCasaRepository);
    }

    @SneakyThrows
    @Test
    public void dodaj_ok() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        Naloga n = new Naloga();
        n.setId(dto.getNalogaId());

        // mock metod
        Mockito
                .when(nalogaRepository.findById(n.getId()))
                .thenReturn(Optional.of(n));

        Mockito
                .when(osebaServiceKlient.vrni(dto.getOsebaId()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        Mockito
                .when(zadolzeniZaNalogoRepository.save(ArgumentMatchers.any(ZadolzeniZaNalogo.class)))
                .thenAnswer((Answer<ZadolzeniZaNalogo>) invocation -> invocation.getArgument(0));

        // klic testirane metode
        ZadolzeniZaNalogo zzn = zadolzeniZaNalogoService.dodaj(dto);

        Assert.assertNotNull(zzn);
        Assert.assertNotNull(zzn.getZadolzeniZaNalogoPk());
        Assert.assertEquals(dto.getOsebaId(), zzn.getZadolzeniZaNalogoPk().getOsebaId());
        Assert.assertEquals(dto.getNalogaId(), zzn.getZadolzeniZaNalogoPk().getNalogaId());
    }

    @SneakyThrows
    @Test(expected = NalogaNeObstajaException.class)
    public void dodaj_nalogaNeObstaja() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        // klic testirane metode
        zadolzeniZaNalogoService.dodaj(dto);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void dodaj_osebaNeObstaja() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        Naloga n = new Naloga();
        n.setId(dto.getNalogaId());

        // mock metod
        Mockito
                .when(nalogaRepository.findById(n.getId()))
                .thenReturn(Optional.of(n));

        Mockito
                .when(osebaServiceKlient.vrni(dto.getOsebaId()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        zadolzeniZaNalogoService.dodaj(dto);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void dodaj_osebaServiceNapaka() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        Naloga n = new Naloga();
        n.setId(dto.getNalogaId());

        // mock metod
        Mockito
                .when(nalogaRepository.findById(n.getId()))
                .thenReturn(Optional.of(n));

        Mockito
                .when(osebaServiceKlient.vrni(dto.getOsebaId()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaServiceException").build()
                );

        // klic testirane metode
        zadolzeniZaNalogoService.dodaj(dto);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_null() {

        zadolzeniZaNalogoService.dodaj(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_brezAnotacijeValidated() {

        Method method = ZadolzeniZaNalogoServiceImpl.class.getDeclaredMethod("dodaj", ZadolzeniZaNalogoDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(ZadolzeniZaNalogoDTO.ObDodajanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void posodobi_ok() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setNalogaId(dto.getNalogaId());
        pk.setOsebaId(dto.getOsebaId());

        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setZadolzeniZaNalogoPk(pk);

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoRepository.findById(pk))
                .thenReturn(Optional.of(zznTest));

        Mockito
                .when(zadolzeniZaNalogoRepository.save(ArgumentMatchers.any(ZadolzeniZaNalogo.class)))
                .thenAnswer((Answer<ZadolzeniZaNalogo>) invocation -> invocation.getArgument(0));

        // klic testirane metode
        ZadolzeniZaNalogo zzn = zadolzeniZaNalogoService.posodobi(dto);

        Assert.assertNotNull(zzn);
        Assert.assertNotNull(zzn.getZadolzeniZaNalogoPk());
        Assert.assertEquals(dto.getOsebaId(), zzn.getZadolzeniZaNalogoPk().getOsebaId());
        Assert.assertEquals(dto.getNalogaId(), zzn.getZadolzeniZaNalogoPk().getNalogaId());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void posodobi_null() {

        zadolzeniZaNalogoService.posodobi(null);
    }

    @SneakyThrows
    @Test(expected = ZadolzeniZaNalogoNeObstajaException.class)
    public void posodobi_neObstaja() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setNalogaId(dto.getNalogaId());
        pk.setOsebaId(dto.getOsebaId());

        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setZadolzeniZaNalogoPk(pk);

        // klic testirane metode
        zadolzeniZaNalogoService.posodobi(dto);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeValidated() {

        Method method = ZadolzeniZaNalogoServiceImpl.class.getDeclaredMethod("posodobi", ZadolzeniZaNalogoDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(ZadolzeniZaNalogoDTO.ObPosodabljanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void izbrisi_ok() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setNalogaId(dto.getNalogaId());
        pk.setOsebaId(dto.getOsebaId());

        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setZadolzeniZaNalogoPk(pk);

        PorabaCasa pc1 = new PorabaCasa();
        pc1.setId(1);

        PorabaCasa pc2 = new PorabaCasa();
        pc2.setId(2);

        List<PorabaCasa> pcList = List.of(pc1, pc2);


        // mock metod
        Mockito
                .when(zadolzeniZaNalogoRepository.findById(pk))
                .thenReturn(Optional.of(zznTest));

        Mockito
                .when(porabaCasaRepository.findByNalogaIdAndOsebaId(zznTest.getNalogaId(), zznTest.getOsebaId()))
                .thenReturn(pcList);

        // klic testirane metode
        ZadolzeniZaNalogo zzn = zadolzeniZaNalogoService.izbrisi(dto);

        Mockito.verify(porabaCasaRepository, Mockito.times(1)).deleteById(pc1.getId());
        Mockito.verify(porabaCasaRepository, Mockito.times(1)).deleteById(pc2.getId());
        Mockito.verify(zadolzeniZaNalogoRepository, Mockito.times(1)).delete(zznTest);
        Assert.assertNotNull(zzn);
        Assert.assertNotNull(zzn.getZadolzeniZaNalogoPk());
        Assert.assertEquals(dto.getOsebaId(), zzn.getZadolzeniZaNalogoPk().getOsebaId());
        Assert.assertEquals(dto.getNalogaId(), zzn.getZadolzeniZaNalogoPk().getNalogaId());
    }

    @SneakyThrows
    @Test(expected = ZadolzeniZaNalogoNeObstajaException.class)
    public void izbrisi_neObstaja() {

        // priprava testnih podatkov
        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setOsebaId(1);
        dto.setNalogaId(2);
        dto.setDodeljeneUre(BigDecimal.valueOf(1.15));

        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setNalogaId(dto.getNalogaId());
        pk.setOsebaId(dto.getOsebaId());

        ZadolzeniZaNalogo zznTest = new ZadolzeniZaNalogo();
        zznTest.setZadolzeniZaNalogoPk(pk);

        // klic testirane metode
        zadolzeniZaNalogoService.izbrisi(dto);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void izbrisi_null() {

        zadolzeniZaNalogoService.izbrisi(null);
    }

    @SneakyThrows
    @Test
    public void izbrisi_brezAnotacijeValidated() {

        Method method = ZadolzeniZaNalogoServiceImpl.class.getDeclaredMethod("izbrisi", ZadolzeniZaNalogoDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(ZadolzeniZaNalogoDTO.ObBrisanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void izbrisi_brezAnotacijeTransactional() {

        Method method = ZadolzeniZaNalogoServiceImpl.class.getDeclaredMethod("izbrisi", ZadolzeniZaNalogoDTO.class);
        Transactional annotation = method.getAnnotation(Transactional.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(Propagation.REQUIRED, annotation.propagation());
    }

    @SneakyThrows
    @Test
    public void preveriObstojOsebe_ok() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenReturn(OsebaOdgovor.newBuilder().setNapaka("").build());

        // klic testirane metode
        zadolzeniZaNalogoService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void preveriObstojOsebe_osebaNeObstaja() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        zadolzeniZaNalogoService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void preveriObstojOsebe_osebaServiceNapaka() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("Exception").build()
                );

        // klic testirane metode
        zadolzeniZaNalogoService.preveriObstojOsebe(1);
    }
}
