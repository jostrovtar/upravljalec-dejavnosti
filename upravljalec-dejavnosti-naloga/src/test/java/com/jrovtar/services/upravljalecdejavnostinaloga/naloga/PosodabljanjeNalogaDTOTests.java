package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaDTO;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class PosodabljanjeNalogaDTOTests {

    private static Validator validator;
    private static Class[] group;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        group = new Class[]{NalogaDTO.ObPosodabljanju.class};
    }

    @Test
    public void idNull() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(null);
        naloga.setNaziv("Naziv");
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("id");
    }

    @Test
    public void nazivNull() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(1);
        naloga.setNaziv(null);
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void nazivPrazen() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(1);
        naloga.setNaziv("");
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void nazivPredolg() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(1);
        naloga.setNaziv(StringUtils.repeat("*", 256));
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now());

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void datumZacetkaNull() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(1);
        naloga.setNaziv(StringUtils.repeat("*", 255));
        naloga.setDatumZacetka(null);
        naloga.setDatumZakljucka(LocalDate.now());

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("datumZacetka");
    }

    @Test
    public void datumZakljuckaNull() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(1);
        naloga.setNaziv(StringUtils.repeat("*", 255));
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(null);

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("datumZakljucka");
    }

    @Test
    public void notDatumZacetkaPredDatumomZakljucka() {

        NalogaDTO naloga = new NalogaDTO();
        naloga.setId(1);
        naloga.setNaziv(StringUtils.repeat("*", 255));
        naloga.setDatumZacetka(LocalDate.now());
        naloga.setDatumZakljucka(LocalDate.now().minus(1, ChronoUnit.DAYS));

        Set<ConstraintViolation<NalogaDTO>> violations =
                validator.validate(naloga, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("datumZacetkaPredDatumomZakljucka");
    }
}
