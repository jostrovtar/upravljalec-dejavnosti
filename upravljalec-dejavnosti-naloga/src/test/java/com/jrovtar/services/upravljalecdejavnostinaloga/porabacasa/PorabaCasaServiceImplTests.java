package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.PorabaCasaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogo;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoPk;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoRepository;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.annotation.Validated;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Random;

@RunWith(SpringRunner.class)
public class PorabaCasaServiceImplTests {

    @MockBean
    private PorabaCasaRepository porabaCasaRepository;

    @MockBean
    private ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository;

    private PorabaCasaService porabaCasaService;

    @Before
    public void init() {
        porabaCasaService = new PorabaCasaServiceImpl(
                porabaCasaRepository,
                zadolzeniZaNalogoRepository
        );
    }

    @SneakyThrows
    @Test
    public void dodaj_ok() {

        // priprava testnih podatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        PorabaCasaDTO dto = PorabaCasaDTO.kreirajDTO(pc);


        ZadolzeniZaNalogoPk zznPk = new ZadolzeniZaNalogoPk();
        zznPk.setNalogaId(pc.getNalogaId());
        zznPk.setOsebaId(pc.getOsebaId());

        ZadolzeniZaNalogo zzn = new ZadolzeniZaNalogo();
        zzn.setZadolzeniZaNalogoPk(zznPk);

        // mock metod
        Mockito
                .when(zadolzeniZaNalogoRepository.findById(zznPk))
                .thenReturn(Optional.of(zzn));

        Mockito
                .when(porabaCasaRepository.save(ArgumentMatchers.any(PorabaCasa.class)))
                .thenAnswer((Answer<PorabaCasa>) invocation -> {
                    PorabaCasa p = invocation.getArgument(0);
                    p.setId(new Random().nextInt(10));
                    return p;
                });

        // klic testirane metode
        PorabaCasa porabaCasa = porabaCasaService.dodaj(dto);

        // preverjanje rezultatov
        Mockito.verify(porabaCasaRepository, Mockito.times(1)).save(ArgumentMatchers.any(PorabaCasa.class));
        Assert.assertNotNull(porabaCasa.getId());
        Assert.assertEquals(pc.getOsebaId(), porabaCasa.getOsebaId());
        Assert.assertEquals(pc.getNalogaId(), porabaCasa.getNalogaId());
        Assert.assertEquals(pc.getDatum(), porabaCasa.getDatum());
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(porabaCasa.getSteviloUr()));
    }

    @SneakyThrows
    @Test(expected = ZadolzeniZaNalogoNeObstajaException.class)
    public void dodaj_zznNeObstaja() {

        // priprava testnih podatkov
        PorabaCasaDTO pc = new PorabaCasaDTO();
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        // klic testirane metode
        porabaCasaService.dodaj(pc);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_null() {

        porabaCasaService.dodaj(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_brezAnotacijeValidated() {

        Method method = PorabaCasaServiceImpl.class.getDeclaredMethod("dodaj", PorabaCasaDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(PorabaCasaDTO.ObDodajanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void vrni_ok() {

        // priprava testnih podatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(4);
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        // mock metod
        Mockito
                .when(porabaCasaRepository.findById(pc.getId()))
                .thenReturn(Optional.of(pc));

        // klic testirane metode
        PorabaCasa porabaCasa = porabaCasaService.vrni(pc.getId());

        // preverjanje rezultatov
        Assert.assertEquals(pc.getId(), porabaCasa.getId());
        Assert.assertEquals(pc.getOsebaId(), porabaCasa.getOsebaId());
        Assert.assertEquals(pc.getNalogaId(), porabaCasa.getNalogaId());
        Assert.assertEquals(pc.getDatum(), porabaCasa.getDatum());
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(porabaCasa.getSteviloUr()));
    }

    @SneakyThrows
    @Test(expected = PorabaCasaNeObstajaException.class)
    public void vrni_neObstaja() {

        // priprava testnih podatkov
        PorabaCasaDTO pc = new PorabaCasaDTO();
        pc.setId(4);
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        // klic testirane metode
        porabaCasaService.vrni(pc.getId());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void vrni_null() {

        porabaCasaService.vrni(null);
    }

    @SneakyThrows
    @Test
    public void posodobi_ok() {

        // priprava testnih podatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(4);
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        PorabaCasaDTO dto = PorabaCasaDTO.kreirajDTO(pc);

        // mock metod
        Mockito
                .when(porabaCasaRepository.findById(pc.getId()))
                .thenReturn(Optional.of(pc));

        Mockito
                .when(porabaCasaRepository.save(ArgumentMatchers.any(PorabaCasa.class)))
                .thenAnswer((Answer<PorabaCasa>) invocation -> invocation.getArgument(0));

        // klic testirane metode
        PorabaCasa porabaCasa = porabaCasaService.posodobi(dto);

        // preverjanje rezultatov
        Assert.assertEquals(pc.getId(), porabaCasa.getId());
        Assert.assertEquals(pc.getOsebaId(), porabaCasa.getOsebaId());
        Assert.assertEquals(pc.getNalogaId(), porabaCasa.getNalogaId());
        Assert.assertEquals(pc.getDatum(), porabaCasa.getDatum());
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(porabaCasa.getSteviloUr()));
    }

    @SneakyThrows
    @Test(expected = PorabaCasaNeObstajaException.class)
    public void posodobi_neObstaja() {

        // priprava testnih podatkov
        PorabaCasaDTO pc = new PorabaCasaDTO();
        pc.setId(4);
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        // klic testirane metode
        porabaCasaService.posodobi(pc);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void posodobi_null() {

        porabaCasaService.posodobi(null);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeValidated() {

        Method method = PorabaCasaServiceImpl.class.getDeclaredMethod("posodobi", PorabaCasaDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(PorabaCasaDTO.ObPosodabljanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void izbrisi_ok() {

        // priprava testnih podatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(4);
        pc.setNalogaId(1);
        pc.setOsebaId(2);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(new BigDecimal(8.5));

        // mock metod
        Mockito
                .when(porabaCasaRepository.findById(pc.getId()))
                .thenReturn(Optional.of(pc));

        // klic testirane metode
        PorabaCasa porabaCasa = porabaCasaService.izbrisi(pc.getId());

        // preverjanje rezultatov
        Mockito.verify(porabaCasaRepository, Mockito.times(1)).deleteById(pc.getId());
        Assert.assertEquals(pc.getId(), porabaCasa.getId());
        Assert.assertEquals(pc.getOsebaId(), porabaCasa.getOsebaId());
        Assert.assertEquals(pc.getNalogaId(), porabaCasa.getNalogaId());
        Assert.assertEquals(pc.getDatum(), porabaCasa.getDatum());
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(porabaCasa.getSteviloUr()));
    }

    @SneakyThrows
    @Test(expected = PorabaCasaNeObstajaException.class)
    public void izbrisi_neObstaja() {

        porabaCasaService.izbrisi(1);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void izbrisi_null() {

        porabaCasaService.izbrisi(null);
    }
}
