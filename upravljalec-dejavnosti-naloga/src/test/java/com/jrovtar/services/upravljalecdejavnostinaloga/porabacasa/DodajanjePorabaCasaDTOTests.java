package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class DodajanjePorabaCasaDTOTests {

    private static Validator validator;
    private static Class[] group;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        group = new Class[]{PorabaCasaDTO.ObDodajanju.class};
    }

    @Test
    public void nalogaIdNull() {

        PorabaCasaDTO porabaCasa = new PorabaCasaDTO();
        porabaCasa.setNalogaId(null);
        porabaCasa.setOsebaId(1);
        porabaCasa.setDatum(LocalDate.now());
        porabaCasa.setSteviloUr(BigDecimal.ONE);

        Set<ConstraintViolation<PorabaCasaDTO>> violations =
                validator.validate(porabaCasa, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("nalogaId");
    }

    @Test
    public void osebaIdNull() {

        PorabaCasaDTO porabaCasa = new PorabaCasaDTO();
        porabaCasa.setNalogaId(1);
        porabaCasa.setOsebaId(null);
        porabaCasa.setDatum(LocalDate.now());
        porabaCasa.setSteviloUr(BigDecimal.ONE);

        Set<ConstraintViolation<PorabaCasaDTO>> violations =
                validator.validate(porabaCasa, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("osebaId");
    }

    @Test
    public void steviloUrNull() {

        PorabaCasaDTO porabaCasa = new PorabaCasaDTO();
        porabaCasa.setNalogaId(1);
        porabaCasa.setOsebaId(1);
        porabaCasa.setDatum(LocalDate.now());
        porabaCasa.setSteviloUr(null);

        Set<ConstraintViolation<PorabaCasaDTO>> violations =
                validator.validate(porabaCasa, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("steviloUr");
    }

    @Test
    public void steviloUrMinimum() {

        PorabaCasaDTO porabaCasa = new PorabaCasaDTO();
        porabaCasa.setNalogaId(1);
        porabaCasa.setOsebaId(1);
        porabaCasa.setDatum(LocalDate.now());
        porabaCasa.setSteviloUr(BigDecimal.ZERO);

        Set<ConstraintViolation<PorabaCasaDTO>> violations =
                validator.validate(porabaCasa, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("steviloUr");
    }

    @Test
    public void steviloUrMaximum() {

        PorabaCasaDTO porabaCasa = new PorabaCasaDTO();
        porabaCasa.setNalogaId(1);
        porabaCasa.setOsebaId(1);
        porabaCasa.setDatum(LocalDate.now());
        porabaCasa.setSteviloUr(BigDecimal.valueOf(24.1));

        Set<ConstraintViolation<PorabaCasaDTO>> violations =
                validator.validate(porabaCasa, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("steviloUr");
    }

}
