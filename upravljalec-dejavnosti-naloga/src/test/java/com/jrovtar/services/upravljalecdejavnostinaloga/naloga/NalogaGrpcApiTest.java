package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogo;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoPk;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "klienti.osebaServicePort=6565",
        "klienti.osebaServiceUrl=localhost",
        "grpc.port=0",
        "grpc.shutdownGrace=-1"
})
public class NalogaGrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private NalogaService nalogaService;

    private NalogaServiceGrpc.NalogaServiceFutureStub nalogaServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        nalogaServiceFutureStub = NalogaServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void dodaj_ok() {

        // priprava testnih podatkov
        Naloga n = new Naloga();
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        // mock metod
        Mockito
                .when(nalogaService.dodaj(ArgumentMatchers.any(NalogaDTO.class)))
                .thenAnswer((Answer<Naloga>) invocation -> {
                    NalogaDTO nalDTO = invocation.getArgument(0);
                    Naloga nal = new Naloga();
                    nal.setId(new Random().nextInt(10) + 1);
                    nal.setNaziv(nalDTO.getNaziv());
                    nal.setDatumZacetka(nalDTO.getDatumZacetka());
                    nal.setDatumZakljucka(nalDTO.getDatumZakljucka());
                    return nal;
                });

        // klic testirane metode
        NalogaDodajZahteva zahteva = NalogaDodajZahteva.newBuilder()
                .setNaziv(n.getNaziv())
                .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                .build();
        NalogaOdgovor odgovor = nalogaServiceFutureStub.dodaj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertFalse(odgovor.getId() == 0);
        Assert.assertEquals(n.getNaziv(), odgovor.getNaziv());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZacetka()), odgovor.getDatumZacetka());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZakljucka()), odgovor.getDatumZakljucka());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_napaka() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(nalogaService.dodaj(ArgumentMatchers.any(NalogaDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        NalogaDodajZahteva zahteva = NalogaDodajZahteva.newBuilder()
                .setNaziv(n.getNaziv())
                .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                .build();
        NalogaOdgovor odgovor = nalogaServiceFutureStub.dodaj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals("", odgovor.getNaziv());
        Assert.assertEquals("", odgovor.getDatumZacetka());
        Assert.assertEquals("", odgovor.getDatumZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_ok() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setId(5);
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        // mock metod
        Mockito
                .when(nalogaService.izbrisi(ArgumentMatchers.any(Integer.class)))
                .thenAnswer((Answer<Naloga>) invocation -> n);

        // klic testirane metode
        NalogaIzbrisiZahteva zahteva = NalogaIzbrisiZahteva.newBuilder()
                .setId(n.getId())
                .build();
        NalogaOdgovor odgovor = nalogaServiceFutureStub.izbrisi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(n.getId(), Integer.valueOf(odgovor.getId()));
        Assert.assertEquals(n.getNaziv(), odgovor.getNaziv());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZacetka()), odgovor.getDatumZacetka());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZakljucka()), odgovor.getDatumZakljucka());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_napaka() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setId(5);
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(nalogaService.izbrisi(ArgumentMatchers.any(Integer.class)))
                .thenThrow(exception);

        // klic testirane metode
        NalogaIzbrisiZahteva zahteva = NalogaIzbrisiZahteva.newBuilder()
                .setId(n.getId())
                .build();
        NalogaOdgovor odgovor = nalogaServiceFutureStub.izbrisi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals("", odgovor.getNaziv());
        Assert.assertEquals("", odgovor.getDatumZacetka());
        Assert.assertEquals("", odgovor.getDatumZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_ok() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setId(3);
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        // mock metod
        Mockito
                .when(nalogaService.posodobi(ArgumentMatchers.any(NalogaDTO.class)))
                .thenAnswer((Answer<Naloga>) invocation -> n);

        // klic testirane metode
        NalogaPosodobiZahteva zahteva = NalogaPosodobiZahteva.newBuilder()
                .setId(n.getId())
                .setNaziv(n.getNaziv())
                .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                .build();
        NalogaOdgovor odgovor = nalogaServiceFutureStub.posodobi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(n.getId(), Integer.valueOf(odgovor.getId()));
        Assert.assertEquals(n.getNaziv(), odgovor.getNaziv());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZacetka()), odgovor.getDatumZacetka());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZakljucka()), odgovor.getDatumZakljucka());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_napaka() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setId(3);
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(nalogaService.posodobi(ArgumentMatchers.any(NalogaDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        NalogaPosodobiZahteva zahteva = NalogaPosodobiZahteva.newBuilder()
                .setId(n.getId())
                .setNaziv(n.getNaziv())
                .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                .build();
        NalogaOdgovor odgovor = nalogaServiceFutureStub.posodobi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals("", odgovor.getNaziv());
        Assert.assertEquals("", odgovor.getDatumZacetka());
        Assert.assertEquals("", odgovor.getDatumZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void vrni_ok() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setId(3);
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        ZadolzeniZaNalogo zzn = new ZadolzeniZaNalogo();
        zzn.setZadolzeniZaNalogoPk(new ZadolzeniZaNalogoPk());
        zzn.getZadolzeniZaNalogoPk().setOsebaId(3);
        zzn.setDodeljeneUre(BigDecimal.valueOf(34.5));
        n.setZadolzeniZaNalogoList(List.of(zzn));

        PorabaCasa pc = new PorabaCasa();
        pc.setId(8);
        pc.setDatum(LocalDate.now());
        pc.setSteviloUr(BigDecimal.valueOf(8.5));
        zzn.setPorabaCasaList(List.of(pc));

        // mock metod
        Mockito
                .when(nalogaService.vrni(n.getId(), true))
                .thenAnswer((Answer<Naloga>) invocation -> n);

        // klic testirane metode
        NalogaVrniZahteva zahteva = NalogaVrniZahteva.newBuilder()
                .setId(n.getId())
                .build();
        NalogaVrniOdgovor odgovor = nalogaServiceFutureStub.vrni(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(n.getId(), Integer.valueOf(odgovor.getId()));
        Assert.assertEquals(n.getNaziv(), odgovor.getNaziv());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZacetka()), odgovor.getDatumZacetka());
        Assert.assertEquals(NalogaUtils.formatiraj(n.getDatumZakljucka()), odgovor.getDatumZakljucka());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
        Assert.assertEquals(n.getZadolzeniZaNalogoList().size(), odgovor.getOsebeCount());

        for (int i = 0; i < n.getZadolzeniZaNalogoList().size(); i++) {

            ZadolzeniZaNalogo zznExp = n.getZadolzeniZaNalogoList().get(i);
            NalogaVrniOseba zznAct = odgovor.getOsebeList().get(i);

            Assert.assertEquals(zznExp.getZadolzeniZaNalogoPk().getOsebaId(), Integer.valueOf(zznAct.getOsebaId()));
            Assert.assertEquals(zznExp.getDodeljeneUre(), BigDecimal.valueOf(zznAct.getDodeljeneUre()));

            for (int j = 0; j < zznExp.getPorabaCasaList().size(); j++) {

                PorabaCasa pcExp = zznExp.getPorabaCasaList().get(j);
                NalogaOsebaCas pcAct = zznAct.getPorabljenCasList().get(j);

                Assert.assertEquals(pcExp.getId(), Integer.valueOf(pcAct.getId()));
                Assert.assertEquals(NalogaUtils.formatiraj(pcExp.getDatum()), pcAct.getDatum());
                Assert.assertEquals(pcExp.getSteviloUr(), BigDecimal.valueOf(pcAct.getSteviloUr()));
            }
        }
    }

    @SneakyThrows
    @Test
    final public void vrni_napaka() {

        // priprava testnih poatkov
        Naloga n = new Naloga();
        n.setId(3);
        n.setNaziv("Naloga 1");
        n.setDatumZacetka(LocalDate.now());
        n.setDatumZakljucka(LocalDate.now().plus(1, ChronoUnit.DAYS));

        Exception exception = new NalogaNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(nalogaService.vrni(n.getId(), true))
                .thenThrow(exception);

        // klic testirane metode
        NalogaVrniZahteva zahteva = NalogaVrniZahteva.newBuilder()
                .setId(n.getId())
                .build();
        NalogaVrniOdgovor odgovor = nalogaServiceFutureStub.vrni(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals("", odgovor.getNaziv());
        Assert.assertEquals("", odgovor.getDatumZacetka());
        Assert.assertEquals("", odgovor.getDatumZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
        Assert.assertEquals(0, odgovor.getOsebeCount());
    }

    @SneakyThrows
    @Test
    final public void filtriraj_ok() {

        // priprava testnih poatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = -1.0;
        Double maxDodeljeneUre = -1.0;
        Double minOpravljeneUre = -1.0;
        Double maxOpravljeneUre = -1.0;

        List<FiltrirajNalogeDTO> fnList = List.of(
                new FiltrirajNalogeDTO("Naloga 1", BigDecimal.valueOf(10), BigDecimal.valueOf(5)),
                new FiltrirajNalogeDTO("Naloga 2", BigDecimal.valueOf(35), BigDecimal.valueOf(16))
        );

        // mock metod
        Mockito
                .when(nalogaService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenAnswer((Answer<List<FiltrirajNalogeDTO>>) invocation -> fnList);

        // klic testirane metode
        NalogaFiltrirajZahteva zahteva = NalogaFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxOpravljeneUre)
                .build();
        NalogaFiltrirajOdgovor odgovor = nalogaServiceFutureStub.filtriraj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(fnList.size(), odgovor.getItemsCount());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());

        for (int i = 0; i < fnList.size(); i++) {
            Assert.assertEquals(fnList.get(i).getNaziv(), odgovor.getItems(i).getNaziv());
            Assert.assertEquals(0,
                    fnList.get(i).getDodeljeneUre().compareTo(BigDecimal.valueOf(odgovor.getItems(i).getDodeljeneUre())));
            Assert.assertEquals(0,
                    fnList.get(i).getOpravljeneUre().compareTo(BigDecimal.valueOf(odgovor.getItems(i).getOpravljeneUre())));
        }
    }

    @SneakyThrows
    @Test
    final public void filtriraj_napaka() {

        // priprava testnih poatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = -1.0;
        Double maxDodeljeneUre = -1.0;
        Double minOpravljeneUre = -1.0;
        Double maxOpravljeneUre = -1.0;

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(nalogaService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenThrow(exception);

        // klic testirane metode
        NalogaFiltrirajZahteva zahteva = NalogaFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxOpravljeneUre)
                .build();
        NalogaFiltrirajOdgovor odgovor = nalogaServiceFutureStub.filtriraj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getItemsCount());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

}
