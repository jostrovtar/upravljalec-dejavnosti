package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaUtils;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.PorabaCasaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "klienti.osebaServicePort=6565",
        "klienti.osebaServiceUrl=localhost",
        "grpc.port=0",
        "grpc.shutdownGrace=-1"
})
public class PorabaCasaGrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private PorabaCasaService porabaCasaService;

    private PorabaCasaServiceGrpc.PorabaCasaServiceFutureStub porabaCasaServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        porabaCasaServiceFutureStub = PorabaCasaServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void dodaj_ok() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        // mock metod
        Mockito
                .when(porabaCasaService.dodaj(ArgumentMatchers.any(PorabaCasaDTO.class)))
                .thenAnswer((Answer<PorabaCasa>) invocation -> {
                    PorabaCasaDTO pcDTO = invocation.getArgument(0);
                    PorabaCasa p = new PorabaCasa();
                    p.setId(new Random().nextInt(10) + 1);
                    p.setNalogaId(pcDTO.getNalogaId());
                    p.setOsebaId(pcDTO.getOsebaId());
                    p.setSteviloUr(pcDTO.getSteviloUr());
                    p.setDatum(pcDTO.getDatum());
                    return p;
                });

        // klic testirane metode
        PorabaCasaDodajZahteva zahteva = PorabaCasaDodajZahteva.newBuilder()
                .setOsebaId(pc.getOsebaId())
                .setNalogaId(pc.getNalogaId())
                .setSteviloUr(pc.getSteviloUr().doubleValue())
                .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.dodaj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertNotEquals(0, odgovor.getId());
        Assert.assertEquals(pc.getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(pc.getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(BigDecimal.valueOf(odgovor.getSteviloUr())));
        Assert.assertEquals(NalogaUtils.formatiraj(pc.getDatum()), odgovor.getDatum());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_napaka() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        Exception exception = new ZadolzeniZaNalogoNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(porabaCasaService.dodaj(ArgumentMatchers.any(PorabaCasaDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        PorabaCasaDodajZahteva zahteva = PorabaCasaDodajZahteva.newBuilder()
                .setOsebaId(pc.getOsebaId())
                .setNalogaId(pc.getNalogaId())
                .setSteviloUr(pc.getSteviloUr().doubleValue())
                .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.dodaj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals(0, odgovor.getNalogaId());
        Assert.assertEquals(0, odgovor.getOsebaId());
        Assert.assertEquals(Double.valueOf(0.0), Double.valueOf(odgovor.getSteviloUr()));
        Assert.assertEquals("", odgovor.getDatum());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void vrni_ok() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(6);
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        // mock metod
        Mockito
                .when(porabaCasaService.vrni(pc.getId()))
                .thenAnswer((Answer<PorabaCasa>) invocation -> pc);

        // klic testirane metode
        PorabaCasaVrniZahteva zahteva = PorabaCasaVrniZahteva.newBuilder()
                .setId(pc.getId())
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.vrni(zahteva).get();

        // preverjanje rezultatov
        Assert.assertNotEquals(pc.getId(), Integer.valueOf(odgovor.getId()));
        Assert.assertEquals(pc.getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(pc.getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(BigDecimal.valueOf(odgovor.getSteviloUr())));
        Assert.assertEquals(NalogaUtils.formatiraj(pc.getDatum()), odgovor.getDatum());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void vrni_napaka() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(6);
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        Exception exception = new PorabaCasaNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(porabaCasaService.vrni(pc.getId()))
                .thenThrow(exception);

        // klic testirane metode
        PorabaCasaVrniZahteva zahteva = PorabaCasaVrniZahteva.newBuilder()
                .setId(pc.getId())
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.vrni(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals(0, odgovor.getNalogaId());
        Assert.assertEquals(0, odgovor.getOsebaId());
        Assert.assertEquals(Double.valueOf(0.0), Double.valueOf(odgovor.getSteviloUr()));
        Assert.assertEquals("", odgovor.getDatum());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_ok() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(6);
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        // mock metod
        Mockito
                .when(porabaCasaService.izbrisi(pc.getId()))
                .thenAnswer((Answer<PorabaCasa>) invocation -> pc);

        // klic testirane metode
        PorabaCasaIzbrisiZahteva zahteva = PorabaCasaIzbrisiZahteva.newBuilder()
                .setId(pc.getId())
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.izbrisi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertNotEquals(pc.getId(), Integer.valueOf(odgovor.getId()));
        Assert.assertEquals(pc.getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(pc.getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(BigDecimal.valueOf(odgovor.getSteviloUr())));
        Assert.assertEquals(NalogaUtils.formatiraj(pc.getDatum()), odgovor.getDatum());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_napaka() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(6);
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        Exception exception = new PorabaCasaNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(porabaCasaService.izbrisi(pc.getId()))
                .thenThrow(exception);

        // klic testirane metode
        PorabaCasaIzbrisiZahteva zahteva = PorabaCasaIzbrisiZahteva.newBuilder()
                .setId(pc.getId())
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.izbrisi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getId());
        Assert.assertEquals(0, odgovor.getNalogaId());
        Assert.assertEquals(0, odgovor.getOsebaId());
        Assert.assertEquals(Double.valueOf(0.0), Double.valueOf(odgovor.getSteviloUr()));
        Assert.assertEquals("", odgovor.getDatum());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_ok() {

        // priprava testnih poatkov
        PorabaCasa pc = new PorabaCasa();
        pc.setId(6);
        pc.setNalogaId(2);
        pc.setOsebaId(3);
        pc.setSteviloUr(BigDecimal.valueOf(1.5));
        pc.setDatum(LocalDate.now());

        // mock metod
        Mockito
                .when(porabaCasaService.posodobi(ArgumentMatchers.any(PorabaCasaDTO.class)))
                .thenAnswer((Answer<PorabaCasa>) invocation -> pc);

        // klic testirane metode
        PorabaCasaPosodobiZahteva zahteva = PorabaCasaPosodobiZahteva.newBuilder()
                .setId(pc.getId())
                .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                .setSteviloUr(pc.getSteviloUr().doubleValue())
                .build();
        PorabaCasaOdgovor odgovor = porabaCasaServiceFutureStub.posodobi(zahteva).get();

        // preverjanje rezultatov
        Assert.assertNotEquals(pc.getId(), Integer.valueOf(odgovor.getId()));
        Assert.assertEquals(pc.getNalogaId(), Integer.valueOf(odgovor.getNalogaId()));
        Assert.assertEquals(pc.getOsebaId(), Integer.valueOf(odgovor.getOsebaId()));
        Assert.assertEquals(0, pc.getSteviloUr().compareTo(BigDecimal.valueOf(odgovor.getSteviloUr())));
        Assert.assertEquals(NalogaUtils.formatiraj(pc.getDatum()), odgovor.getDatum());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());
    }
}
