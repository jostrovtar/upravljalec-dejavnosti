package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class BrisanjeZadolzeniZaNalogDTOTests {

    private static Validator validator;
    private static Class[] group;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        group = new Class[]{ZadolzeniZaNalogoDTO.ObBrisanju.class};
    }

    @Test
    public void nalogaIdNull() {

        ZadolzeniZaNalogoDTO zzn = new ZadolzeniZaNalogoDTO();
        zzn.setNalogaId(null);
        zzn.setOsebaId(1);
        zzn.setDodeljeneUre(BigDecimal.valueOf(1));

        Set<ConstraintViolation<ZadolzeniZaNalogoDTO>> violations =
                validator.validate(zzn, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("nalogaId");
    }

    @Test
    public void osebaIdNull() {

        ZadolzeniZaNalogoDTO zzn = new ZadolzeniZaNalogoDTO();
        zzn.setNalogaId(1);
        zzn.setOsebaId(null);
        zzn.setDodeljeneUre(BigDecimal.valueOf(1));

        Set<ConstraintViolation<ZadolzeniZaNalogoDTO>> violations =
                validator.validate(zzn, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("osebaId");
    }
}
