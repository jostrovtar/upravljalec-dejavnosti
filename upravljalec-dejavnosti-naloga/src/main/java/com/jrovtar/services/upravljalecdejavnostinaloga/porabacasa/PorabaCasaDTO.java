package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaUtils;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.PorabaCasaDodajZahteva;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.PorabaCasaPosodobiZahteva;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PorabaCasaDTO {

    @NotNull(
            groups = {ObPosodabljanju.class},
            message = "Id porabe časa je obveyen."
    )
    private Integer id;

    @NotNull(
            groups = {ObDodajanju.class},
            message = "Id naloge je obvezen."
    )
    private Integer nalogaId;

    @NotNull(
            groups = {ObDodajanju.class},
            message = "Id osebe je obvezen."
    )
    private Integer osebaId;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Datum je obvezen."
    )
    private LocalDate datum;

    @DecimalMin(
            value = "0.0",
            inclusive = false,
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Število ur mora biti večje od {value}.")
    @DecimalMax(
            value = "24.0",
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Število ur ne sme biti večje od {value}.")
    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Število ur je obvezno."
    )
    private BigDecimal steviloUr;

    /**
     * Kreiraj PorabaCasaDTO iz PorabaCasaDodajZahteva.
     *
     * @param porabaCasaDodajZahteva instanca tipa PorabaCasaDodajZahteva
     * @return instanca tipa NalogaDTO
     */
    public static PorabaCasaDTO kreirajDTO(PorabaCasaDodajZahteva porabaCasaDodajZahteva) {

        if (porabaCasaDodajZahteva == null) {
            return null;
        }

        PorabaCasaDTO dto = new PorabaCasaDTO();
        dto.setNalogaId(porabaCasaDodajZahteva.getNalogaId());
        dto.setOsebaId(porabaCasaDodajZahteva.getOsebaId());
        dto.setDatum(
                StringUtils.isEmpty(porabaCasaDodajZahteva.getDatum()) ?
                        null : NalogaUtils.kreirajLocalDate(porabaCasaDodajZahteva.getDatum()));
        dto.setSteviloUr(BigDecimal.valueOf(porabaCasaDodajZahteva.getSteviloUr()));

        return dto;
    }

    /**
     * Kreiraj PorabaCasaDTO iz PorabaCasa.
     *
     * @param porabaCasa instanca tipa PorabaCasa
     * @return instanca tipa NalogaDTO
     */
    public static PorabaCasaDTO kreirajDTO(PorabaCasa porabaCasa) {

        if (porabaCasa == null) {
            return null;
        }

        PorabaCasaDTO dto = new PorabaCasaDTO();
        dto.setId(porabaCasa.getId());
        dto.setNalogaId(porabaCasa.getNalogaId());
        dto.setOsebaId(porabaCasa.getOsebaId());
        dto.setDatum(porabaCasa.getDatum());
        dto.setSteviloUr(porabaCasa.getSteviloUr());

        return dto;
    }

    /**
     * Kreiraj PorabaCasaDTO iz PorabaCasaPosodobiZahteva.
     *
     * @param porabaCasaPosodobiZahteva instanca tipa PorabaCasaPosodobiZahteva
     * @return instanca tipa NalogaDTO
     */
    public static PorabaCasaDTO kreirajDTO(PorabaCasaPosodobiZahteva porabaCasaPosodobiZahteva) {

        if (porabaCasaPosodobiZahteva == null) {
            return null;
        }

        PorabaCasaDTO dto = new PorabaCasaDTO();
        dto.setId(porabaCasaPosodobiZahteva.getId());
        dto.setDatum(
                StringUtils.isEmpty(porabaCasaPosodobiZahteva.getDatum()) ?
                        null : NalogaUtils.kreirajLocalDate(porabaCasaPosodobiZahteva.getDatum()));
        dto.setSteviloUr(BigDecimal.valueOf(porabaCasaPosodobiZahteva.getSteviloUr()));

        return dto;
    }

    /*
     * Tipa, ki se uporabljata za določanje validacijske skupne (validation group)
     * pri validiranju PorabaCasaDTO v metodah razdreda PorabaCasaServiceImpl.
     */
    public interface ObDodajanju {
    }

    public interface ObPosodabljanju {
    }

}
