package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class PorabaCasa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "zadolzeni_za_nalogo_naloga_id")
    private Integer nalogaId;

    @Column(name = "zadolzeni_za_nalogo_oseba_id")
    private Integer osebaId;

    @Column(name = "datum")
    private LocalDate datum;

    @Column(name = "stevilo_ur")
    private BigDecimal steviloUr;

}
