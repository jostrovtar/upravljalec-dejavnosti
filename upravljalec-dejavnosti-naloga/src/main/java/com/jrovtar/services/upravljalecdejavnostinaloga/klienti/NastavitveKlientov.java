package com.jrovtar.services.upravljalecdejavnostinaloga.klienti;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Configuration
@ConfigurationProperties(prefix = "klienti")
@Validated
public class NastavitveKlientov {

    @NotBlank
    private String osebaServiceUrl;

    @NotNull
    private Integer osebaServicePort;
}
