package com.jrovtar.services.upravljalecdejavnostinaloga.napake;

public class NalogaNeObstajaException extends Exception {

    public NalogaNeObstajaException(String message) {
        super(message);
    }
}
