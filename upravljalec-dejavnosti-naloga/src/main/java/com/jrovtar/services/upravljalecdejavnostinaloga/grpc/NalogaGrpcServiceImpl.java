package com.jrovtar.services.upravljalecdejavnostinaloga.grpc;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import javax.validation.ConstraintViolationException;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@GRpcService
public class NalogaGrpcServiceImpl extends NalogaServiceGrpc.NalogaServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju naloge. ";
    public static final String VRNI_NAPAKA_SPOROCILO = "Napaka pri vračanju naloge. ";
    public static final String POSODOBI_NAPAKA_SPOROCILO = "Napaka pri posodabljanju naloge. ";
    public static final String BRISI_NAPAKA_SPOROCILO = "Napaka pri brisanju naloge. ";
    public static final String FILTRITAJ_NAPAKA_SPOROCILO = "Napaka pri filtriranju nalog. ";

    private NalogaService nalogaService;

    public NalogaGrpcServiceImpl(NalogaService nalogaService) {
        this.nalogaService = nalogaService;
    }


    @Override
    public void dodaj(NalogaDodajZahteva request, StreamObserver<NalogaOdgovor> responseObserver) {

        NalogaOdgovor nalogaOdgovor;

        try {
            Naloga n = nalogaService.dodaj(NalogaDTO.kreirajDTO(request));
            nalogaOdgovor = NalogaOdgovor.newBuilder()
                    .setId(n.getId())
                    .setNaziv(n.getNaziv())
                    .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                    .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaOdgovor = NalogaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void vrni(NalogaVrniZahteva request, StreamObserver<NalogaVrniOdgovor> responseObserver) {

        NalogaVrniOdgovor nalogaOdgovor;

        try {
            Naloga n = nalogaService.vrni(request.getId(), true);
            nalogaOdgovor = NalogaVrniOdgovor.newBuilder()
                    .setId(n.getId())
                    .setNaziv(n.getNaziv())
                    .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                    .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))

                    // dodaj zadolzene osebe
                    .addAllOsebe(
                            n.getZadolzeniZaNalogoList().stream().map(
                                    zzn -> NalogaVrniOseba.newBuilder()
                                            .setOsebaId(zzn.getZadolzeniZaNalogoPk().getOsebaId())
                                            .setDodeljeneUre(zzn.getDodeljeneUre().doubleValue())

                                            // dodaj porabljen cas zadolzenih oseb
                                            .addAllPorabljenCas(
                                                    zzn.getPorabaCasaList().stream().map(
                                                            pc -> NalogaOsebaCas.newBuilder()
                                                                    .setId(pc.getId())
                                                                    .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                                                                    .setSteviloUr(pc.getSteviloUr().doubleValue())
                                                                    .build()
                                                    ).collect(Collectors.toList())
                                            )
                                            .build()
                            ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof NalogaNeObstajaException) {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaOdgovor = NalogaVrniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void izbrisi(NalogaIzbrisiZahteva request, StreamObserver<NalogaOdgovor> responseObserver) {

        NalogaOdgovor nalogaOdgovor;

        try {
            Naloga n = nalogaService.izbrisi(request.getId());
            nalogaOdgovor = NalogaOdgovor.newBuilder()
                    .setId(n.getId())
                    .setNaziv(n.getNaziv())
                    .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                    .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof NalogaNeObstajaException) {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaOdgovor = NalogaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void posodobi(NalogaPosodobiZahteva request, StreamObserver<NalogaOdgovor> responseObserver) {

        NalogaOdgovor nalogaOdgovor;

        try {
            Naloga n = nalogaService.posodobi(NalogaDTO.kreirajDTO(request));
            nalogaOdgovor = NalogaOdgovor.newBuilder()
                    .setId(n.getId())
                    .setNaziv(n.getNaziv())
                    .setDatumZacetka(NalogaUtils.formatiraj(n.getDatumZacetka()))
                    .setDatumZakljucka(NalogaUtils.formatiraj(n.getDatumZakljucka()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof NalogaNeObstajaException ||
                    e instanceof ConstraintViolationException) {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaOdgovor = NalogaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void filtriraj(NalogaFiltrirajZahteva request, StreamObserver<NalogaFiltrirajOdgovor> responseObserver) {

        NalogaFiltrirajOdgovor odgovor;

        try {
            List<FiltrirajNalogeDTO> list = nalogaService.filtriraj(
                    request.getFilterNaziv(),
                    request.getFilterZadolzenaOseba(),
                    request.getMinDodeljeneUre(),
                    request.getMaxDodeljeneUre(),
                    request.getMinOpravljeneUre(),
                    request.getMaxOpravljeneUre()
            );
            odgovor = NalogaFiltrirajOdgovor.newBuilder()
                    .addAllItems(
                            list.stream().map(
                                    e -> NalogaFiltrirajItem.newBuilder()
                                            .setNaziv(e.getNaziv())
                                            .setDodeljeneUre(e.getDodeljeneUre().doubleValue())
                                            .setOpravljeneUre(e.getOpravljeneUre().doubleValue())
                                            .build()
                            ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {
            log.error(FILTRITAJ_NAPAKA_SPOROCILO + e.getMessage(), e);

            odgovor = NalogaFiltrirajOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }
}
