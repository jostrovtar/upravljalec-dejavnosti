package com.jrovtar.services.upravljalecdejavnostinaloga.napake;

public class PorabaCasaNeObstajaException extends Exception {

    public PorabaCasaNeObstajaException(String message) {
        super(message);
    }
}
