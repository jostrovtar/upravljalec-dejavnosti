package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.PorabaCasaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogo;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoPk;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoRepository;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.math.RoundingMode;
import java.util.Optional;

@Slf4j
@Service
@Validated
public class PorabaCasaServiceImpl implements PorabaCasaService {

    public static final String MESSAGE_ID_PORABE_CASA_NE_SME_BITI_NULL = "Id porabe časa ne sme biti null.";
    public static final String MESSAGE_PORABA_CASA_NE_OBSTAJA = "Poraba časa z id '%d' ne obstaja.";
    public static final String MESSAGE_PORABA_CASA_NE_SME_BITI_NULL = "Poraba casa ne sme biti null.";

    private PorabaCasaRepository porabaCasaRepository;
    private ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository;

    public PorabaCasaServiceImpl(
            PorabaCasaRepository porabaCasaRepository,
            ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository) {
        this.porabaCasaRepository = porabaCasaRepository;
        this.zadolzeniZaNalogoRepository = zadolzeniZaNalogoRepository;
    }

    /**
     * Dodaj porabo časa.
     *
     * @param dto poraba časa
     * @return dodana poraba casa
     * @throws ZadolzeniZaNalogoNeObstajaException če oseba ni zadolžena za nalogo
     */
    @Validated(PorabaCasaDTO.ObDodajanju.class)
    public PorabaCasa dodaj(@Valid PorabaCasaDTO dto) throws ZadolzeniZaNalogoNeObstajaException {

        Assert.notNull(dto, MESSAGE_PORABA_CASA_NE_SME_BITI_NULL);

        log.debug("Dodajanje porabe casa: {}", dto);

        // preverjanje zadolzenega za nalogo
        ZadolzeniZaNalogoPk zadolzeniZaNalogoPk = new ZadolzeniZaNalogoPk();
        zadolzeniZaNalogoPk.setNalogaId(dto.getNalogaId());
        zadolzeniZaNalogoPk.setOsebaId(dto.getOsebaId());

        Optional<ZadolzeniZaNalogo> zznOpt = zadolzeniZaNalogoRepository.findById(zadolzeniZaNalogoPk);

        if (!zznOpt.isPresent()) {
            throw new ZadolzeniZaNalogoNeObstajaException(
                    String.format(
                            ZadolzeniZaNalogoServiceImpl.MESSAGE_ZADOLZENA_OSEBA_ZA_NALOGO_NE_OBSTAJA,
                            dto.getOsebaId(), dto.getNalogaId()));
        }

        // dodaj porabo časa
        PorabaCasa porabaCasa = new PorabaCasa();
        porabaCasa.setNalogaId(dto.getNalogaId());
        porabaCasa.setOsebaId(dto.getOsebaId());
        porabaCasa.setDatum(dto.getDatum());
        porabaCasa.setSteviloUr(dto.getSteviloUr().setScale(2, RoundingMode.HALF_UP));

        porabaCasa = porabaCasaRepository.save(porabaCasa);

        return porabaCasa;
    }

    /**
     * Vrni porabo časa.
     *
     * @param id id porabe časa
     * @return poraba časa osebe
     * @throws PorabaCasaNeObstajaException če poraba časa s podanim id ne obstaja.
     */
    public PorabaCasa vrni(Integer id) throws PorabaCasaNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_PORABE_CASA_NE_SME_BITI_NULL);

        Optional<PorabaCasa> pcOptional = porabaCasaRepository.findById(id);

        if (!pcOptional.isPresent()) {
            throw new PorabaCasaNeObstajaException(
                    String.format(MESSAGE_PORABA_CASA_NE_OBSTAJA, id));
        }

        return pcOptional.get();
    }

    /**
     * Posodobi porabo časa osebe za nalgo. Omogočeno je urejanje datuma in število ur.
     *
     * @param dto poraba časa dto
     * @return posodobljena poraba časa
     * @throws PorabaCasaNeObstajaException če poraba časa s podanim id ne obstaja.
     */
    @Validated(PorabaCasaDTO.ObPosodabljanju.class)
    public PorabaCasa posodobi(@Valid PorabaCasaDTO dto) throws PorabaCasaNeObstajaException {

        Assert.notNull(dto, MESSAGE_PORABA_CASA_NE_SME_BITI_NULL);

        log.debug("Posodabljanje porabe časa: {}", dto);

        PorabaCasa pc = vrni(dto.getId());
        pc.setDatum(dto.getDatum());
        pc.setSteviloUr(dto.getSteviloUr().setScale(2, RoundingMode.HALF_UP));

        // osebe in naloge ni mogoče spreminjati

        pc = porabaCasaRepository.save(pc);

        return pc;
    }

    /**
     * Izbriši porabo časa.
     *
     * @param id id porabe časa
     * @return izbrisana naloga
     * @throws PorabaCasaNeObstajaException če poraba časa s podanim id ne obstaja.
     */
    @Transactional
    public PorabaCasa izbrisi(Integer id) throws PorabaCasaNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_PORABE_CASA_NE_SME_BITI_NULL);

        log.debug("Brisanje porabe časa z id: {}", id);

        // vrni porabo casa
        PorabaCasa pc = vrni(id);

        // izbrisi porabo casa
        porabaCasaRepository.deleteById(id);

        return pc;
    }
}
