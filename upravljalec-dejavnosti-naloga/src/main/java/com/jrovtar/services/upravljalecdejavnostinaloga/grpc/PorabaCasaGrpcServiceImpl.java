package com.jrovtar.services.upravljalecdejavnostinaloga.grpc;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaUtils;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.PorabaCasaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaDTO;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import javax.validation.ConstraintViolationException;
import java.time.format.DateTimeParseException;

@Slf4j
@GRpcService
public class PorabaCasaGrpcServiceImpl extends PorabaCasaServiceGrpc.PorabaCasaServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju porabe časa. ";
    public static final String VRNI_NAPAKA_SPOROCILO = "Napaka pri vračanju porabe časa. ";
    public static final String POSODOBI_NAPAKA_SPOROCILO = "Napaka pri posodabljanju porabe časa. ";
    public static final String BRISI_NAPAKA_SPOROCILO = "Napaka pri brisanju porabe časa. ";

    private PorabaCasaService porabaCasaService;

    public PorabaCasaGrpcServiceImpl(PorabaCasaService porabaCasaService) {
        this.porabaCasaService = porabaCasaService;
    }


    @Override
    public void dodaj(PorabaCasaDodajZahteva request, StreamObserver<PorabaCasaOdgovor> responseObserver) {

        PorabaCasaOdgovor odgovor;

        try {
            PorabaCasa pc = porabaCasaService.dodaj(PorabaCasaDTO.kreirajDTO(request));
            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setId(pc.getId())
                    .setNalogaId(pc.getNalogaId())
                    .setOsebaId(pc.getOsebaId())
                    .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                    .setSteviloUr(pc.getSteviloUr().doubleValue())
                    .build();

        } catch (Exception e) {

            if (e instanceof ZadolzeniZaNalogoNeObstajaException ||
                    e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void vrni(PorabaCasaVrniZahteva request, StreamObserver<PorabaCasaOdgovor> responseObserver) {

        PorabaCasaOdgovor odgovor;

        try {
            PorabaCasa pc = porabaCasaService.vrni(request.getId());
            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setOsebaId(pc.getOsebaId())
                    .setNalogaId(pc.getNalogaId())
                    .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                    .setSteviloUr(pc.getSteviloUr().doubleValue())
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException ||
                    e instanceof PorabaCasaNeObstajaException) {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void izbrisi(PorabaCasaIzbrisiZahteva request, StreamObserver<PorabaCasaOdgovor> responseObserver) {

        PorabaCasaOdgovor odgovor;

        try {
            PorabaCasa pc = porabaCasaService.izbrisi(request.getId());
            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setOsebaId(pc.getOsebaId())
                    .setNalogaId(pc.getNalogaId())
                    .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                    .setSteviloUr(pc.getSteviloUr().doubleValue())
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException ||
                    e instanceof PorabaCasaNeObstajaException) {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void posodobi(PorabaCasaPosodobiZahteva request, StreamObserver<PorabaCasaOdgovor> responseObserver) {

        PorabaCasaOdgovor odgovor;

        try {
            PorabaCasa pc = porabaCasaService.posodobi(PorabaCasaDTO.kreirajDTO(request));
            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setOsebaId(pc.getOsebaId())
                    .setNalogaId(pc.getNalogaId())
                    .setDatum(NalogaUtils.formatiraj(pc.getDatum()))
                    .setSteviloUr(pc.getSteviloUr().doubleValue())
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof PorabaCasaNeObstajaException ||
                    e instanceof ConstraintViolationException) {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            odgovor = PorabaCasaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }
}
