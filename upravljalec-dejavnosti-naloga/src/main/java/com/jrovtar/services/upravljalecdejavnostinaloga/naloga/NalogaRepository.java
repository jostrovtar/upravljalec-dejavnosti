package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface NalogaRepository extends CrudRepository<Naloga, Integer> {


    @Query(
            value = "SELECT n1.naziv, n1.dodeljene_ure, n1.opravljene_ure\n" +
                    "FROM (\n" +
                    "         SELECT n.naziv,\n" +
                    "               zzn.dodeljene_ure as dodeljene_ure,\n" +
                    "                pc.stevilo_ur as opravljene_ure\n" +
                    "         FROM (\n" +
                    // pridobi naloge in filtriraj po nazivu
                    "                  SELECT n.id, n.naziv\n" +
                    "                  FROM naloga n\n" +
                    "                  WHERE :nazivFilter IS NULL\n" +
                    "                  OR n.naziv like %:nazivFilter%\n" +
                    "              ) AS n\n" +
                    // filtriraj zadolžene po osebi in seštej dodeljene ure
                    "                  LEFT OUTER JOIN (\n" +
                    "                     SELECT zzn.naloga_id,\n" +
                    "                            SUM(zzn.dodeljene_ure) dodeljene_ure\n" +
                    "                     FROM zadolzeni_za_nalogo zzn\n" +
                    "                     WHERE :osebaFilter IS NULL\n" +
                    "                        OR zzn.oseba_id = :osebaFilter\n" +
                    "                     GROUP BY zzn.naloga_id\n" +
                    "                 ) as zzn\n" +
                    "                  ON n.id = zzn.naloga_id\n" +
                    // filtriraj porabo časa po osebi in seštej opravaljene ure
                    "                  LEFT OUTER JOIN (\n" +
                    "                     SELECT pc.zadolzeni_za_nalogo_naloga_id as naloga_id,\n" +
                    "                            SUM(pc.stevilo_ur)               as stevilo_ur\n" +
                    "                     FROM poraba_casa pc\n" +
                    "                     WHERE :osebaFilter IS NULL\n" +
                    "                        OR pc.zadolzeni_za_nalogo_oseba_id = :osebaFilter\n" +
                    "                     GROUP BY pc.zadolzeni_za_nalogo_naloga_id\n" +
                    "                 ) AS pc\n" +
                    "                  ON n.id = pc.naloga_id\n" +
                    "     ) n1\n" +
                    // filtriraj po dodeljenih in opravljenih urah
                    "WHERE n1.dodeljene_ure IS NOT NULL\n" +
                    "AND n1.opravljene_ure IS NOT NULL " +
                    "AND (:minDodeljeneUre IS NULL OR n1.dodeljene_ure >= :minDodeljeneUre)\n" +
                    "AND (:maxDodeljeneUre IS NULL OR n1.dodeljene_ure <= :maxDodeljeneUre)\n" +
                    "AND (:minOpravljeneUre IS NULL OR n1.opravljene_ure >= :minOpravljeneUre)\n" +
                    "AND (:maxOpravljeneUre IS NULL OR n1.opravljene_ure <= :maxOpravljeneUre)",
            nativeQuery = true)
    List<Object[]> filtriraj(
            @Param(value = "nazivFilter") String nazivFilter,
            @Param(value = "osebaFilter") Integer osebaFilter,
            @Param(value = "minDodeljeneUre") Double minDodeljeneUre,
            @Param(value = "maxDodeljeneUre") Double maxDodeljeneUre,
            @Param(value = "minOpravljeneUre") Double minOpravljeneUre,
            @Param(value = "maxOpravljeneUre") Double maxOpravljeneUre
    );
}
