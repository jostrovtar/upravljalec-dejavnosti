package com.jrovtar.services.upravljalecdejavnostinaloga.grpc;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.*;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogo;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoDTO;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

@Slf4j
@GRpcService
public class NalogaZadolzeniGrpcServiceImpl extends NalogaZadolzeniServiceGrpc.NalogaZadolzeniServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju osebe na nalogo. ";
    public static final String BRISI_NAPAKA_SPOROCILO = "Napaka pri brisanju osebe iz naloge. ";

    private ZadolzeniZaNalogoService zadolzeniZaNalogoService;

    public NalogaZadolzeniGrpcServiceImpl(ZadolzeniZaNalogoService zadolzeniZaNalogoService) {
        this.zadolzeniZaNalogoService = zadolzeniZaNalogoService;
    }

    @Override
    public void dodaj(NalogaZadolzeniDodajZahteva request, StreamObserver<NalogaZadolzeniOdgovor> responseObserver) {

        NalogaZadolzeniOdgovor nalogaZadolzeniOdgovor;

        try {
            ZadolzeniZaNalogo zadolzeniZaNalogo =
                    zadolzeniZaNalogoService.dodaj(ZadolzeniZaNalogoDTO.kreirajDTO(request));

            nalogaZadolzeniOdgovor = NalogaZadolzeniOdgovor.newBuilder()
                    .setNalogaId(zadolzeniZaNalogo.getZadolzeniZaNalogoPk().getNalogaId())
                    .setOsebaId(zadolzeniZaNalogo.getZadolzeniZaNalogoPk().getOsebaId())
                    .setDodeljeneUre(zadolzeniZaNalogo.getDodeljeneUre().doubleValue())
                    .build();


        } catch (Exception e) {

            if (e instanceof NalogaNeObstajaException ||
                    e instanceof OsebaNeObstajaException ||
                    e instanceof OsebaServiceException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaZadolzeniOdgovor = NalogaZadolzeniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaZadolzeniOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void izbrisi(NalogaZadolzeniIzbrisiZahteva request, StreamObserver<NalogaZadolzeniOdgovor> responseObserver) {

        NalogaZadolzeniOdgovor nalogaZadolzeniOdgovor;

        try {
            ZadolzeniZaNalogo zadolzeniZaNalogo =
                    zadolzeniZaNalogoService.izbrisi(ZadolzeniZaNalogoDTO.kreirajDTO(request));

            nalogaZadolzeniOdgovor = NalogaZadolzeniOdgovor.newBuilder()
                    .setNalogaId(zadolzeniZaNalogo.getZadolzeniZaNalogoPk().getNalogaId())
                    .setOsebaId(zadolzeniZaNalogo.getZadolzeniZaNalogoPk().getOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof ZadolzeniZaNalogoNeObstajaException) {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaZadolzeniOdgovor = NalogaZadolzeniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaZadolzeniOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void posodobi(NalogaZadolzeniPosodobiZahteva request, StreamObserver<NalogaZadolzeniOdgovor> responseObserver) {

        NalogaZadolzeniOdgovor nalogaZadolzeniOdgovor;

        try {
            ZadolzeniZaNalogo zadolzeniZaNalogo =
                    zadolzeniZaNalogoService.posodobi(ZadolzeniZaNalogoDTO.kreirajDTO(request));

            nalogaZadolzeniOdgovor = NalogaZadolzeniOdgovor.newBuilder()
                    .setNalogaId(zadolzeniZaNalogo.getZadolzeniZaNalogoPk().getNalogaId())
                    .setOsebaId(zadolzeniZaNalogo.getZadolzeniZaNalogoPk().getOsebaId())
                    .setDodeljeneUre(zadolzeniZaNalogo.getDodeljeneUre().doubleValue())
                    .build();

        } catch (Exception e) {

            if (e instanceof ZadolzeniZaNalogoNeObstajaException) {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            nalogaZadolzeniOdgovor = NalogaZadolzeniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(nalogaZadolzeniOdgovor);
        responseObserver.onCompleted();
    }
}
