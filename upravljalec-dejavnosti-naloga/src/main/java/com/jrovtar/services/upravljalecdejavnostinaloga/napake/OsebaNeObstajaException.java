package com.jrovtar.services.upravljalecdejavnostinaloga.napake;

public class OsebaNeObstajaException extends Exception {

    public OsebaNeObstajaException(String message) {
        super(message);
    }
}
