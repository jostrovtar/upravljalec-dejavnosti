package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaDodajZahteva;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaPosodobiZahteva;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.StringUtils;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class NalogaDTO {

    @NotNull(
            groups = {ObPosodabljanju.class},
            message = "Id je obvezen."
    )
    private Integer id;

    @NotBlank(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Naziv je obvezen.")
    @Length(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            max = 255,
            message = "Največja dovoljena dolžina naziva je {max} znakov.")
    private String naziv;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Datum zacetka je obvezen.")
    private LocalDate datumZacetka;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Datum zakljucka je obvezen.")
    private LocalDate datumZakljucka;

    /**
     * Kreiraj NalogaDTO iz NalogaDodajZahteva.
     *
     * @param nalogaDodajZahteva instanca tipa NalogaDodajZahteva
     * @return instanca tipa NalogaDTO
     */
    public static NalogaDTO kreirajDTO(NalogaDodajZahteva nalogaDodajZahteva) {

        if (nalogaDodajZahteva == null) {
            return null;
        }

        NalogaDTO dto = new NalogaDTO();
        dto.setNaziv(nalogaDodajZahteva.getNaziv());
        dto.setDatumZacetka(
                StringUtils.isEmpty(nalogaDodajZahteva.getDatumZacetka()) ?
                        null : NalogaUtils.kreirajLocalDate(nalogaDodajZahteva.getDatumZacetka()));
        dto.setDatumZakljucka(
                StringUtils.isEmpty(nalogaDodajZahteva.getDatumZakljucka()) ?
                        null : NalogaUtils.kreirajLocalDate(nalogaDodajZahteva.getDatumZakljucka()));

        return dto;
    }

    /**
     * Kreiraj NalogaDTO iz Naloga.
     *
     * @param naloga instanca tipa Naloga
     * @return instanca tipa NalogaDTO
     */
    public static NalogaDTO kreirajDTO(Naloga naloga) {

        if (naloga == null) {
            return null;
        }

        NalogaDTO dto = new NalogaDTO();
        dto.setId(naloga.getId());
        dto.setNaziv(naloga.getNaziv());
        dto.setDatumZacetka(naloga.getDatumZacetka());
        dto.setDatumZakljucka(naloga.getDatumZakljucka());

        return dto;
    }

    /**
     * Kreiraj NalogaDTO iz NalogaPosodobiZahteva.
     *
     * @param nalogaPosodobiZahteva instanca tipa NalogaPosodobiZahteva
     * @return instanca tipa NalogaDTO
     */
    public static NalogaDTO kreirajDTO(NalogaPosodobiZahteva nalogaPosodobiZahteva) {

        if (nalogaPosodobiZahteva == null) {
            return null;
        }

        NalogaDTO dto = new NalogaDTO();
        dto.setId(nalogaPosodobiZahteva.getId());
        dto.setNaziv(nalogaPosodobiZahteva.getNaziv());
        dto.setDatumZacetka(
                StringUtils.isEmpty(nalogaPosodobiZahteva.getDatumZacetka()) ?
                        null : NalogaUtils.kreirajLocalDate(nalogaPosodobiZahteva.getDatumZacetka()));
        dto.setDatumZakljucka(
                StringUtils.isEmpty(nalogaPosodobiZahteva.getDatumZakljucka()) ?
                        null : NalogaUtils.kreirajLocalDate(nalogaPosodobiZahteva.getDatumZakljucka()));

        return dto;
    }

    @AssertTrue(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Datum začetka ne sme biti za datumom zaključka.")
    public boolean isDatumZacetkaPredDatumomZakljucka() {

        if (datumZacetka != null && datumZakljucka != null &&
                datumZacetka.isAfter(datumZakljucka)) {
            return false;
        }

        return true;
    }

    /*
     * Tipa, ki se uporabljata za določanje validacijske skupne (validation group)
     * pri validiranju NalogaDTO v metodah razdreda NalogaServiceImpl.
     */
    public interface ObDodajanju {
    }

    public interface ObPosodabljanju {
    }
}
