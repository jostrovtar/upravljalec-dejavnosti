package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PorabaCasaRepository extends CrudRepository<PorabaCasa, Integer> {

    List<PorabaCasa> findByNalogaIdAndOsebaId(Integer nalogaId, Integer osebaId);
}
