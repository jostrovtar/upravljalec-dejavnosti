package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class NalogaUtils {

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private NalogaUtils() {
    }


    /**
     * Kreiraj LocalDate iz stringa. Zahtevan format dd.MM.yyyy .
     *
     * @param strDatum formatiran datum
     * @return LocalDate
     * @throws IllegalArgumentException če je strDatum null ali prazen string
     * @throws DateTimeParseException   če strDatum ni pravilno formatiran
     */
    public static LocalDate kreirajLocalDate(String strDatum) {

        Assert.hasText(strDatum, "Podan string je null ali prazen.");

        return LocalDate.parse(strDatum, dateFormatter);
    }

    /**
     * Kreiraj String iz LocalDate. Format: dd.MM.yyyy .
     *
     * @param datum datum
     * @return formatiran datum
     */
    public static String formatiraj(LocalDate datum) {

        if (datum == null) {
            return "";
        }

        return datum.format(dateFormatter);
    }
}
