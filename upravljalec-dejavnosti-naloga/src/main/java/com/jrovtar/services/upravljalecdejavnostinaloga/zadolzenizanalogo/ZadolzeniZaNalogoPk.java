package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ZadolzeniZaNalogoPk implements Serializable {

    @Column(name = "naloga_id")
    private Integer nalogaId;

    @Column(name = "oseba_id")
    private Integer osebaId;
}
