package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaZadolzeniDodajZahteva;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaZadolzeniIzbrisiZahteva;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaZadolzeniPosodobiZahteva;
import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ZadolzeniZaNalogoDTO {

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class, ObBrisanju.class},
            message = "Id naloge je obvezen."
    )
    private Integer nalogaId;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class, ObBrisanju.class},
            message = "Id osebe je obvezen."
    )
    private Integer osebaId;

    @DecimalMin(
            value = "0.0",
            inclusive = false,
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Število dodeljenih ur mora biti večje od {value}.")
    @DecimalMax(
            value = "9999.9",
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Število ur ne sme biti večje od {value}.")
    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Število ur je obvezno."
    )
    private BigDecimal dodeljeneUre;


    /**
     * Kreiraj ZadolzeniZaNalogoDTO iz NalogaZadolzeniDodajZahteva.
     *
     * @param nalogaZadolzeniDodajZahteva instanca tipa NalogaZadolzeniDodajZahteva
     * @return instanca tipa ZadolzeniZaNalogoDTO
     */
    public static ZadolzeniZaNalogoDTO kreirajDTO(NalogaZadolzeniDodajZahteva nalogaZadolzeniDodajZahteva) {

        if (nalogaZadolzeniDodajZahteva == null) {
            return null;
        }

        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setNalogaId(nalogaZadolzeniDodajZahteva.getNalogaId());
        dto.setOsebaId(nalogaZadolzeniDodajZahteva.getOsebaId());
        dto.setDodeljeneUre(BigDecimal.valueOf(nalogaZadolzeniDodajZahteva.getDodeljeneUre()));

        return dto;
    }

    /**
     * Kreiraj ZadolzeniZaNalogoDTO iz NalogaZadolzeniPosodobiZahteva.
     *
     * @param nalogaZadolzeniPosodobiZahteva instanca tipa NalogaZadolzeniPosodobiZahteva
     * @return instanca tipa ZadolzeniZaNalogoDTO
     */
    public static ZadolzeniZaNalogoDTO kreirajDTO(NalogaZadolzeniPosodobiZahteva nalogaZadolzeniPosodobiZahteva) {

        if (nalogaZadolzeniPosodobiZahteva == null) {
            return null;
        }

        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setNalogaId(nalogaZadolzeniPosodobiZahteva.getNalogaId());
        dto.setOsebaId(nalogaZadolzeniPosodobiZahteva.getOsebaId());
        dto.setDodeljeneUre(BigDecimal.valueOf(nalogaZadolzeniPosodobiZahteva.getDodeljeneUre()));

        return dto;
    }

    /**
     * Kreiraj ZadolzeniZaNalogoDTO iz NalogaZadolzeniIzbrisiZahteva.
     *
     * @param nalogaZadolzeniIzbrisiZahteva instanca tipa NalogaZadolzeniIzbrisiZahteva
     * @return instanca tipa ZadolzeniZaNalogoDTO
     */
    public static ZadolzeniZaNalogoDTO kreirajDTO(NalogaZadolzeniIzbrisiZahteva nalogaZadolzeniIzbrisiZahteva) {

        if (nalogaZadolzeniIzbrisiZahteva == null) {
            return null;
        }

        ZadolzeniZaNalogoDTO dto = new ZadolzeniZaNalogoDTO();
        dto.setNalogaId(nalogaZadolzeniIzbrisiZahteva.getNalogaId());
        dto.setOsebaId(nalogaZadolzeniIzbrisiZahteva.getOsebaId());

        return dto;
    }

    public interface ObDodajanju {
    }

    public interface ObPosodabljanju {
    }

    public interface ObBrisanju {
    }

}