package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class FiltrirajNalogeDTO {

    private String naziv;
    private BigDecimal dodeljeneUre;
    private BigDecimal opravljeneUre;

}
