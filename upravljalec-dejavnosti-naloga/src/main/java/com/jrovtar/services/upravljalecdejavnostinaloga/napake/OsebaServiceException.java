package com.jrovtar.services.upravljalecdejavnostinaloga.napake;

public class OsebaServiceException extends Exception {

    public OsebaServiceException(String message) {
        super(message);
    }

    public OsebaServiceException(String message, Throwable t) {
        super(message, t);
    }
}
