package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaRepository;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogo;
import com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo.ZadolzeniZaNalogoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
public class NalogaServiceImpl implements NalogaService {

    public static final String MESSAGE_NALOGA_NE_OBSTAJA = "Naloga z id '%d' ne obstaja.";
    public static final String MESSAGE_NALOGA_NE_SME_BITI_NULL = "Naloga ne sme biti null.";
    public static final String MESSAGE_ID_NALOGE_NI_PODAN = "Id naloge mora biti podan.";

    private NalogaRepository nalogaRepository;
    private ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository;
    private PorabaCasaRepository porabaCasaRepository;

    public NalogaServiceImpl(
            NalogaRepository nalogaRepository,
            ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository,
            PorabaCasaRepository porabaCasaRepository) {
        this.nalogaRepository = nalogaRepository;
        this.zadolzeniZaNalogoRepository = zadolzeniZaNalogoRepository;
        this.porabaCasaRepository = porabaCasaRepository;
    }

    /**
     * Dodaj nalogo.
     *
     * @param dto naloga
     * @return dodana naloga
     */
    @Validated(NalogaDTO.ObDodajanju.class)
    public Naloga dodaj(@Valid NalogaDTO dto) {

        Assert.notNull(dto, MESSAGE_NALOGA_NE_SME_BITI_NULL);

        log.debug("Dodajanje naloge: {}", dto);

        Naloga n = new Naloga();
        n.setNaziv(dto.getNaziv());
        n.setDatumZacetka(dto.getDatumZacetka());
        n.setDatumZakljucka(dto.getDatumZakljucka());

        n = nalogaRepository.save(n);

        return n;
    }

    /**
     * Vrni nalogo s podanim id.
     *
     * @param id            id naloge
     * @param vrniZadolzene vrni tudi zadolzene za nalogo
     * @return naloga in zadolžene osebe
     * @throws NalogaNeObstajaException če naloga s podanim id-jem ne obstaja.
     */
    public Naloga vrni(Integer id, boolean vrniZadolzene) throws NalogaNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_NALOGE_NI_PODAN);

        Optional<Naloga> nalogaOptional = nalogaRepository.findById(id);

        if (!nalogaOptional.isPresent()) {
            throw new NalogaNeObstajaException(
                    String.format(MESSAGE_NALOGA_NE_OBSTAJA, id));
        }

        Naloga n = nalogaOptional.get();

        if (vrniZadolzene) {
            n.setZadolzeniZaNalogoList(
                    zadolzeniZaNalogoRepository.findByNalogaId(n.getId()));

            for (ZadolzeniZaNalogo zzn : n.getZadolzeniZaNalogoList()) {
                zzn.setPorabaCasaList(
                        porabaCasaRepository.findByNalogaIdAndOsebaId(zzn.getNalogaId(), zzn.getOsebaId())
                );
            }
        }

        return n;
    }

    /**
     * Posodobi nalogo.
     *
     * @param dto naloga
     * @return posodobljena naloga
     * @throws NalogaNeObstajaException če naloga s podanim id-jem ne obstaja.
     */
    @Validated(NalogaDTO.ObPosodabljanju.class)
    public Naloga posodobi(@Valid NalogaDTO dto) throws NalogaNeObstajaException {

        Assert.notNull(dto, MESSAGE_NALOGA_NE_SME_BITI_NULL);

        log.debug("Posodabljanje naloge: {}", dto);

        Naloga s = vrni(dto.getId(), false);
        s.setNaziv(dto.getNaziv());
        s.setDatumZacetka(dto.getDatumZacetka());
        s.setDatumZakljucka(dto.getDatumZakljucka());

        s = nalogaRepository.save(s);

        return s;
    }

    /**
     * Izbriši nalogo.
     *
     * @param id id naloge
     * @return izbrisana naloga
     * @throws NalogaNeObstajaException če naloga s podanim id-jem ne obstaja.
     */
    @Transactional
    public Naloga izbrisi(Integer id) throws NalogaNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_NALOGE_NI_PODAN);

        // vrni nalogo
        Naloga n = vrni(id, true);

        // pobrisi zadolzene za nalogo
        for (ZadolzeniZaNalogo zzn : n.getZadolzeniZaNalogoList()) {

            log.debug("Brisanje porabe časa za zadolženo osebo: {}", zzn.getOsebaId());

            // izbrisi porabo casa
            for (PorabaCasa pc : zzn.getPorabaCasaList()) {
                log.debug("Brisanje porarabe časa z id: {}", pc.getId());
                porabaCasaRepository.deleteById(pc.getId());
            }

            // izbrisi zadolzeni za nalogo
            zadolzeniZaNalogoRepository.deleteById(zzn.getZadolzeniZaNalogoPk());
        }

        // izbrisi nalogo
        nalogaRepository.delete(n);

        return n;
    }

    /**
     * Filtriraj naloge po nazivu, zadolženi osebu, dodeljenih in opravljenih urah. Filter se ne upošteva, če je
     * vrednost null ali negativna (prazen string v primeru naziva).
     * <p>
     * Dodeljene ure se seštejejo po tabeli zadolzeni_za_nalogo, opravljene ure se seštejejo po tabelo poraba_casa.
     *
     * @param nazivFilter            filter po nazivu naloge
     * @param zadolzenaOsebaIdFilter filter po zadolzeni osebi.
     * @param minDodeljeneUre        min število dodeljenih ur, ne sme biti večje kot maxDodeljeneUre
     * @param maxDodeljeneUre        max število dodeljenih ur
     * @param minOpravljeneUre       min število opravljenih ur, ne sme biti večje kot maxOpravljeneUre
     * @param maxOpravljeneUre       max število opravljenih ur
     * @return seznam filtriranih nalog
     */
    public List<FiltrirajNalogeDTO> filtriraj(
            String nazivFilter, Integer zadolzenaOsebaIdFilter, Double minDodeljeneUre,
            Double maxDodeljeneUre, Double minOpravljeneUre, Double maxOpravljeneUre) {

        if ("".equals(nazivFilter)) {
            nazivFilter = null;
        }

        if (zadolzenaOsebaIdFilter != null && zadolzenaOsebaIdFilter < 0) {
            zadolzenaOsebaIdFilter = null;
        }

        if (minDodeljeneUre != null && minDodeljeneUre < 0.0) {
            minDodeljeneUre = null;
        }

        if (maxDodeljeneUre != null && maxDodeljeneUre < 0.0) {
            maxDodeljeneUre = null;
        }

        if (minOpravljeneUre != null && minOpravljeneUre < 0) {
            minOpravljeneUre = null;
        }

        if (maxOpravljeneUre != null && maxOpravljeneUre < 0) {
            maxOpravljeneUre = null;
        }

        if (minDodeljeneUre != null && maxDodeljeneUre != null &&
                minDodeljeneUre > maxDodeljeneUre) {
            throw new IllegalArgumentException(
                    "Minimalno število dodeljenih ur ne sme biti večje od maximalnega števila dodeljenih ur.");
        }

        if (minOpravljeneUre != null && maxOpravljeneUre != null &&
                minOpravljeneUre > maxOpravljeneUre) {
            throw new IllegalArgumentException(
                    "Minimalno število opravljenih ur ne sme biti večje od maximalnega števila opravljenih ur.");
        }

        List<Object[]> list =
                nalogaRepository.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        return list.stream().map(o ->
                new FiltrirajNalogeDTO((String) o[0], (BigDecimal) o[1], (BigDecimal) o[2]))
                .collect(Collectors.toList());
    }
}
