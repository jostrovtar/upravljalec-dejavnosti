package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ZadolzeniZaNalogoRepository extends CrudRepository<ZadolzeniZaNalogo, ZadolzeniZaNalogoPk> {

    List<ZadolzeniZaNalogo> findByNalogaId(Integer nalogaId);
}
