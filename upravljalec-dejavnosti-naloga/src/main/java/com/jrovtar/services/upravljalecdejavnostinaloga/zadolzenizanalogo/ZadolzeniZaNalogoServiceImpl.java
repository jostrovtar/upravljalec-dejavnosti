package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.klienti.OsebaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.Naloga;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaRepository;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.NalogaServiceImpl;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasaRepository;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Validated
public class ZadolzeniZaNalogoServiceImpl implements ZadolzeniZaNalogoService {

    public static final String MESSAGE_ZADOLZENA_OSEBA_ZA_NALOGO_NE_OBSTAJA = "Oseba z id '%d' ni bila zadolžena za nalogo z id '%d'.";
    public static final String MESSAGE_ZADOLZENI_ZA_NALOGO_NE_SME_BITI_NULL = "Zadolzeni za nalogo ne sme biti null.";

    private NalogaRepository nalogaRepository;
    private ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository;
    private OsebaServiceKlient osebaServiceKlient;
    private PorabaCasaRepository porabaCasaRepository;

    public ZadolzeniZaNalogoServiceImpl(
            NalogaRepository nalogaRepository,
            ZadolzeniZaNalogoRepository zadolzeniZaNalogoRepository,
            OsebaServiceKlient osebaServiceKlient,
            PorabaCasaRepository porabaCasaRepository) {
        this.nalogaRepository = nalogaRepository;
        this.zadolzeniZaNalogoRepository = zadolzeniZaNalogoRepository;
        this.osebaServiceKlient = osebaServiceKlient;
        this.porabaCasaRepository = porabaCasaRepository;
    }

    /**
     * Zadolži osebo za nalogo.
     *
     * @param dto zadolzeni za nalogo
     * @return dodan zadolzeni za nalogo
     * @throws NalogaNeObstajaException če naloga s podanim id ne obstaja
     * @throws OsebaNeObstajaException  če oseba s podanim id ne obstaja.
     * @throws OsebaServiceException    če preverjanje osebe ni bilo uspešno.
     */
    @Validated({ZadolzeniZaNalogoDTO.ObDodajanju.class})
    public ZadolzeniZaNalogo dodaj(@Valid ZadolzeniZaNalogoDTO dto)
            throws NalogaNeObstajaException, OsebaNeObstajaException, OsebaServiceException {

        Assert.notNull(dto, MESSAGE_ZADOLZENI_ZA_NALOGO_NE_SME_BITI_NULL);

        // preverjanje naloge
        Optional<Naloga> nalogaOptional = nalogaRepository.findById(dto.getNalogaId());

        if (!nalogaOptional.isPresent()) {
            throw new NalogaNeObstajaException(
                    String.format(NalogaServiceImpl.MESSAGE_NALOGA_NE_OBSTAJA, dto.getNalogaId()));
        }

        // prevejanje osebe
        preveriObstojOsebe(dto.getOsebaId());

        log.debug("Dodajanje zadolženega za nalogo: {}", dto);

        // kreiraj primarni ključ
        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setOsebaId(dto.getOsebaId());
        pk.setNalogaId(dto.getNalogaId());

        // kreiraj entiteto
        ZadolzeniZaNalogo zzn = new ZadolzeniZaNalogo();
        zzn.setZadolzeniZaNalogoPk(pk);
        zzn.setDodeljeneUre(dto.getDodeljeneUre());

        // shrani v bazo
        zzn = zadolzeniZaNalogoRepository.save(zzn);

        return zzn;
    }

    /**
     * Posodobi dodeljene ure za osebo zadolženo za nalogo.
     *
     * @param dto zadolzeži za nalogo
     * @return posodobljen zadolzeni za nalogo
     * @throws ZadolzeniZaNalogoNeObstajaException če zadolženi za osebo ne obstaja
     */
    @Validated({ZadolzeniZaNalogoDTO.ObPosodabljanju.class})
    public ZadolzeniZaNalogo posodobi(@Valid ZadolzeniZaNalogoDTO dto)
            throws ZadolzeniZaNalogoNeObstajaException {

        Assert.notNull(dto, MESSAGE_ZADOLZENI_ZA_NALOGO_NE_SME_BITI_NULL);

        log.debug("Posodabljanje zadolženega za nalogo: {}", dto);

        // kreiraj primarni ključ
        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setOsebaId(dto.getOsebaId());
        pk.setNalogaId(dto.getNalogaId());

        // preveri obstoj zapisa
        Optional<ZadolzeniZaNalogo> zznOpt = zadolzeniZaNalogoRepository.findById(pk);

        if(!zznOpt.isPresent()){
            throw new ZadolzeniZaNalogoNeObstajaException(
                    String.format(MESSAGE_ZADOLZENA_OSEBA_ZA_NALOGO_NE_OBSTAJA, dto.getOsebaId(), dto.getNalogaId()));
        }

        // posodobi zapis
        ZadolzeniZaNalogo zzn = zznOpt.get();
        zzn.setDodeljeneUre(dto.getDodeljeneUre());
        zzn = zadolzeniZaNalogoRepository.save(zzn);

        return zzn;
    }

    /**
     * Izbrisi osebo iz naloge.
     *
     * @param dto zadolženi za nalogo
     * @return odstranjena zadolzena oseba iz naloge
     * @throws ZadolzeniZaNalogoNeObstajaException če oseba ni bila zadolžena za nalogo.
     */
    @Transactional
    @Validated({ZadolzeniZaNalogoDTO.ObBrisanju.class})
    public ZadolzeniZaNalogo izbrisi(@Valid ZadolzeniZaNalogoDTO dto)
            throws ZadolzeniZaNalogoNeObstajaException {

        Assert.notNull(dto, MESSAGE_ZADOLZENI_ZA_NALOGO_NE_SME_BITI_NULL);

        log.debug(String.format(
                "Brisanje osebe z id '%d' na iz naloge z id '%d'", dto.getOsebaId(), dto.getNalogaId()));

        // kreiraj primarni ključ
        ZadolzeniZaNalogoPk pk = new ZadolzeniZaNalogoPk();
        pk.setOsebaId(dto.getOsebaId());
        pk.setNalogaId(dto.getNalogaId());

        // preverjanje ali zapis obstaja
        Optional<ZadolzeniZaNalogo> optional = zadolzeniZaNalogoRepository.findById(pk);

        if (!optional.isPresent()) {
            throw new ZadolzeniZaNalogoNeObstajaException(
                    String.format(MESSAGE_ZADOLZENA_OSEBA_ZA_NALOGO_NE_OBSTAJA, dto.getOsebaId(), dto.getNalogaId()));
        }

        ZadolzeniZaNalogo zzn = optional.get();

        // izbrisi porabo casa
        List<PorabaCasa> pcList =
                porabaCasaRepository.findByNalogaIdAndOsebaId(zzn.getNalogaId(), zzn.getOsebaId());

        for (PorabaCasa pc : pcList) {
            log.debug("Brisanje porarabe časa z id: {}", pc.getId());
            porabaCasaRepository.deleteById(pc.getId());
        }

        // izbriši zadolženega za nalogo
        zadolzeniZaNalogoRepository.delete(zzn);

        return zzn;
    }

    /**
     * Preveri, ali oseba s podanim id obstaja.
     *
     * @param osebaId id osebe
     * @throws OsebaServiceException   če je prišlo do napake pri klicu
     * @throws OsebaNeObstajaException če oseba ne obstaja
     */
    public void preveriObstojOsebe(Integer osebaId) throws OsebaServiceException, OsebaNeObstajaException {

        OsebaOdgovor osebaOdgovor = osebaServiceKlient.vrni(osebaId);

        if (!osebaOdgovor.getNapaka().equals("")) {

            if (osebaOdgovor.getNapaka().equals("OsebaNeObstajaException")) {
                throw new OsebaNeObstajaException(osebaOdgovor.getNapakaSporocilo());
            } else {
                throw new OsebaServiceException(
                        String.format("Napaka pri vračanju osebe. Tip napake: %s. Sporočilo: %s",
                                osebaOdgovor.getNapaka(), osebaOdgovor.getNapakaSporocilo()));
            }
        }
    }
}
