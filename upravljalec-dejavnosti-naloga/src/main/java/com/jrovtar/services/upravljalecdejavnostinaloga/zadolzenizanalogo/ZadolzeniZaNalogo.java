package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa.PorabaCasa;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
public class ZadolzeniZaNalogo {

    @EmbeddedId
    private ZadolzeniZaNalogoPk zadolzeniZaNalogoPk;

    @Column(name = "naloga_id", insertable = false, updatable = false)
    private Integer nalogaId;

    @Column(name = "oseba_id", insertable = false, updatable = false)
    private Integer osebaId;

    @Column(name = "dodeljene_ure")
    private BigDecimal dodeljeneUre;

    @Transient
    private List<PorabaCasa> porabaCasaList;

}
