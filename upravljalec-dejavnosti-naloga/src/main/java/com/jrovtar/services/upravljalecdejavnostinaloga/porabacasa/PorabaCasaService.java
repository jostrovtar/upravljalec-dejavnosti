package com.jrovtar.services.upravljalecdejavnostinaloga.porabacasa;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.PorabaCasaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

public interface PorabaCasaService {

    /**
     * Dodaj porabo časa.
     *
     * @param dto poraba časa
     * @return dodana poraba casa
     * @throws ZadolzeniZaNalogoNeObstajaException če oseba ni zadolžena za nalogo
     */
    @Validated(PorabaCasaDTO.ObDodajanju.class)
    PorabaCasa dodaj(@Valid PorabaCasaDTO dto) throws ZadolzeniZaNalogoNeObstajaException;

    /**
     * Vrni porabo časa.
     *
     * @param id id porabe časa
     * @return poraba časa osebe
     * @throws PorabaCasaNeObstajaException če poraba časa s podanim id ne obstaja.
     */
    PorabaCasa vrni(Integer id) throws PorabaCasaNeObstajaException;

    /**
     * Posodobi porabo časa osebe za nalgo. Omogočeno je urejanje datuma in število ur.
     *
     * @param dto poraba časa dto
     * @return posodobljena poraba časa
     * @throws PorabaCasaNeObstajaException če poraba časa s podanim id ne obstaja.
     */
    @Validated(PorabaCasaDTO.ObPosodabljanju.class)
    PorabaCasa posodobi(@Valid PorabaCasaDTO dto) throws PorabaCasaNeObstajaException;

    /**
     * Izbriši porabo časa.
     *
     * @param id id porabe časa
     * @return izbrisana naloga
     * @throws PorabaCasaNeObstajaException če poraba časa s podanim id ne obstaja.
     */
    @Transactional
    PorabaCasa izbrisi(Integer id) throws PorabaCasaNeObstajaException;
}
