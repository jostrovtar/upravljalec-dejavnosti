package com.jrovtar.services.upravljalecdejavnostinaloga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpravljalecDejavnostiNalogaApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpravljalecDejavnostiNalogaApplication.class, args);
	}

}
