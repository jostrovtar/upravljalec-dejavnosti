package com.jrovtar.services.upravljalecdejavnostinaloga.zadolzenizanalogo;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostinaloga.napake.ZadolzeniZaNalogoNeObstajaException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

public interface ZadolzeniZaNalogoService {

    /**
     * Zadolži osebo za nalogo.
     *
     * @param dto zadolzeni za nalogo
     * @return dodan zadolzeni za nalogo
     * @throws NalogaNeObstajaException če naloga s podanim id ne obstaja
     * @throws OsebaNeObstajaException  če oseba s podanim id ne obstaja.
     * @throws OsebaServiceException    če preverjanje osebe ni bilo uspešno.
     */
    @Validated({ZadolzeniZaNalogoDTO.ObDodajanju.class})
    ZadolzeniZaNalogo dodaj(@Valid ZadolzeniZaNalogoDTO dto)
            throws NalogaNeObstajaException, OsebaNeObstajaException, OsebaServiceException;

    /**
     * Posodobi dodeljene ure za osebo zadolženo za nalogo.
     *
     * @param dto zadolzeži za nalogo
     * @return posodobljen zadolzeni za nalogo
     * @throws ZadolzeniZaNalogoNeObstajaException če zadolženi za osebo ne obstaja
     */
    @Validated({ZadolzeniZaNalogoDTO.ObPosodabljanju.class})
    ZadolzeniZaNalogo posodobi(@Valid ZadolzeniZaNalogoDTO dto)
            throws ZadolzeniZaNalogoNeObstajaException;

    /**
     * Izbrisi osebo iz naloge.
     *
     * @param dto zadolženi za nalogo
     * @return odstranjena zadolzena oseba iz naloge
     * @throws ZadolzeniZaNalogoNeObstajaException če oseba ni bila zadolžena za nalogo.
     */
    @Transactional
    @Validated({ZadolzeniZaNalogoDTO.ObBrisanju.class})
    ZadolzeniZaNalogo izbrisi(@Valid ZadolzeniZaNalogoDTO dto)
            throws ZadolzeniZaNalogoNeObstajaException;

    /**
     * Preveri, ali oseba s podanim id obstaja.
     *
     * @param osebaId id osebe
     * @throws OsebaServiceException   če je prišlo do napake pri klicu
     * @throws OsebaNeObstajaException če oseba ne obstaja
     */
    void preveriObstojOsebe(Integer osebaId) throws OsebaServiceException, OsebaNeObstajaException;

}
