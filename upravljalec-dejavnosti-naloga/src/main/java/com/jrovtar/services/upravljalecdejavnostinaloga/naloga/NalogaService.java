package com.jrovtar.services.upravljalecdejavnostinaloga.naloga;

import com.jrovtar.services.upravljalecdejavnostinaloga.napake.NalogaNeObstajaException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

public interface NalogaService {

    /**
     * Dodaj nalogo.
     *
     * @param dto naloga
     * @return dodana naloga
     */
    @Validated(NalogaDTO.ObDodajanju.class)
    Naloga dodaj(@Valid NalogaDTO dto);

    /**
     * Vrni nalogo s podanim id.
     *
     * @param id            id naloge
     * @param vrniZadolzene vrni tudi zadolzene za nalogo
     * @return naloga in zadolžene osebe
     * @throws NalogaNeObstajaException če naloga s podanim id-jem ne obstaja.
     */
    Naloga vrni(Integer id, boolean vrniZadolzene) throws NalogaNeObstajaException;

    /**
     * Posodobi nalogo.
     *
     * @param dto naloga
     * @return posodobljena naloga
     * @throws NalogaNeObstajaException če naloga s podanim id-jem ne obstaja.
     */
    @Validated(NalogaDTO.ObPosodabljanju.class)
    Naloga posodobi(@Valid NalogaDTO dto) throws NalogaNeObstajaException;

    /**
     * Izbriši nalogo.
     *
     * @param id id naloge
     * @return izbrisana naloga
     * @throws NalogaNeObstajaException če naloga s podanim id-jem ne obstaja.
     */
    @Transactional
    Naloga izbrisi(Integer id) throws NalogaNeObstajaException;

    /**
     * Filtriraj naloge po nazivu, zadolženi osebu, dodeljenih in opravljenih urah. Filter se ne upošteva, če je
     * vrednost null ali negativna (prazen string v primeru naziva).
     * <p>
     * Dodeljene ure se seštejejo po tabeli zadolzeni_za_nalogo, opravljene ure se seštejejo po tabelo poraba_casa.
     *
     * @param nazivFilter            filter po nazivu naloge
     * @param zadolzenaOsebaIdFilter filter po zadolzeni osebi.
     * @param minDodeljeneUre        min število dodeljenih ur, ne sme biti večje kot maxDodeljeneUre
     * @param maxDodeljeneUre        max število dodeljenih ur
     * @param minOpravljeneUre       min število opravljenih ur, ne sme biti večje kot maxOpravljeneUre
     * @param maxOpravljeneUre       max število opravljenih ur
     * @return seznam filtriranih nalog
     */
    List<FiltrirajNalogeDTO> filtriraj(
            String nazivFilter, Integer zadolzenaOsebaIdFilter, Double minDodeljeneUre,
            Double maxDodeljeneUre, Double minOpravljeneUre, Double maxOpravljeneUre);
}
