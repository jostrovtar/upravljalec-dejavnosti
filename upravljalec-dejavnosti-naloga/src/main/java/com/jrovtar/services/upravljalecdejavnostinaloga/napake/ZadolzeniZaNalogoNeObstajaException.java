package com.jrovtar.services.upravljalecdejavnostinaloga.napake;

public class ZadolzeniZaNalogoNeObstajaException extends Exception {

    public ZadolzeniZaNalogoNeObstajaException(String message) {
        super(message);
    }
}
