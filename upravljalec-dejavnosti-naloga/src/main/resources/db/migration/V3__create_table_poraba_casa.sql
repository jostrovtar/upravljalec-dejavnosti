CREATE TABLE IF NOT EXISTS poraba_casa
(
    id                            INT           NOT NULL AUTO_INCREMENT,
    zadolzeni_za_nalogo_naloga_id INT           NOT NULL,
    zadolzeni_za_nalogo_oseba_id  INT           NOT NULL,
    datum                         DATE          NOT NULL,
    stevilo_ur                    DECIMAL(4, 2) NOT NULL,
    INDEX fk_poraba_casa_zadolzeni_za_nalogo1_idx (
                                                   zadolzeni_za_nalogo_naloga_id ASC,
                                                   zadolzeni_za_nalogo_oseba_id ASC),
    PRIMARY KEY (id),
    CONSTRAINT fk_poraba_casa_zadolzeni_za_nalogo1
        FOREIGN KEY (zadolzeni_za_nalogo_naloga_id, zadolzeni_za_nalogo_oseba_id)
            REFERENCES zadolzeni_za_nalogo (naloga_id, oseba_id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB