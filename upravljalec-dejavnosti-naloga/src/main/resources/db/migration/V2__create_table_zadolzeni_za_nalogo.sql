CREATE TABLE IF NOT EXISTS zadolzeni_za_nalogo (
  naloga_id INT NOT NULL,
  oseba_id INT NOT NULL,
  dodeljene_ure DECIMAL(6,2) NOT NULL,
  PRIMARY KEY (naloga_id, oseba_id),
  CONSTRAINT fk_zadolzeni_za_nalogo_naloga
    FOREIGN KEY (naloga_id)
    REFERENCES naloga (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB