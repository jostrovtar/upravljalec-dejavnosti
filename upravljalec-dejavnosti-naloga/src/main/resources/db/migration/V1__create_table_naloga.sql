CREATE TABLE IF NOT EXISTS `naloga` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `naziv` VARCHAR(255) NOT NULL,
  `datum_zacetka` DATE NOT NULL,
  `datum_zakljucka` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB