
# Build project
mvn clean package

# Build docker image
docker build . -t upravljalec-dejavnosti-filter:1.0

# Create container and run it (change client urls and ports)
docker run --name upravljalec-dejavnosti-filter-1-0 -e KLIENTI_MEJNIK_SERVICE_URL=172.17.0.6 -e KLIENTI_MEJNIK_SERVICE_PORT=6561 -e KLIENTI_SESTANEK_SERVICE_URL=172.17.0.5 -e KLIENTI_SESTANEK_SERVICE_PORT=6562 -e KLIENTI_NALOGA_SERVICE_URL=172.17.0.4 -e KLIENTI_NALOGA_SERVICE_PORT=6563 -e GRPC_PORT=6564 -d -p 6564:6564 upravljalec-dejavnosti-filter:1.0
