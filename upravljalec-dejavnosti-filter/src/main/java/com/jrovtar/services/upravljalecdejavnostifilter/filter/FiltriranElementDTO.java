package com.jrovtar.services.upravljalecdejavnostifilter.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class FiltriranElementDTO {

    private String naziv;
    private BigDecimal dodeljeneUre;
    private BigDecimal opravljeneUre;

}
