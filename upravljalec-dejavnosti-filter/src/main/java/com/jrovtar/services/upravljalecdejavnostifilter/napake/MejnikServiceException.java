package com.jrovtar.services.upravljalecdejavnostifilter.napake;

public class MejnikServiceException extends Exception {

    public MejnikServiceException(String message) {
        super(message);
    }

    public MejnikServiceException(String message, Throwable t) {
        super(message, t);
    }
}
