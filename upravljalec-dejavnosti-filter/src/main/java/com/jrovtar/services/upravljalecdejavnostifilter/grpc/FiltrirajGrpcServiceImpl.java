package com.jrovtar.services.upravljalecdejavnostifilter.grpc;

import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajItem;
import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajServiceGrpc;
import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajZahteva;
import com.jrovtar.services.upravljalecdejavnostifilter.filter.FilterService;
import com.jrovtar.services.upravljalecdejavnostifilter.filter.FiltriranElementDTO;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@GRpcService
public class FiltrirajGrpcServiceImpl extends FiltrirajServiceGrpc.FiltrirajServiceImplBase {

    public static final String FILTRITAJ_NAPAKA_SPOROCILO = "Napaka pri filtriranju nalog. ";

    private FilterService filterService;

    public FiltrirajGrpcServiceImpl(FilterService filterService) {
        this.filterService = filterService;
    }


    @Override
    public void filtriraj(FiltrirajZahteva request, StreamObserver<FiltrirajOdgovor> responseObserver) {

        FiltrirajOdgovor odgovor;

        try {
            List<FiltriranElementDTO> list = filterService.filtriraj(
                    request.getFilterNaziv(),
                    request.getFilterZadolzenaOseba(),
                    request.getMinDodeljeneUre(),
                    request.getMaxDodeljeneUre(),
                    request.getMinOpravljeneUre(),
                    request.getMaxOpravljeneUre(),
                    request.getSortirniAtribut(),
                    request.getNarascajoceSortiranje()
            );
            odgovor = FiltrirajOdgovor.newBuilder()
                    .addAllItems(
                            list.stream().map(
                                    e -> FiltrirajItem.newBuilder()
                                            .setNaziv(e.getNaziv())
                                            .setDodeljeneUre(e.getDodeljeneUre().doubleValue())
                                            .setOpravljeneUre(e.getOpravljeneUre().doubleValue())
                                            .build()
                            ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException) {
                log.error(FILTRITAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(FILTRITAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            odgovor = FiltrirajOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }
}
