package com.jrovtar.services.upravljalecdejavnostifilter.napake;

public class NalogaServiceException extends Exception {

    public NalogaServiceException(String message) {
        super(message);
    }

    public NalogaServiceException(String message, Throwable t) {
        super(message, t);
    }
}
