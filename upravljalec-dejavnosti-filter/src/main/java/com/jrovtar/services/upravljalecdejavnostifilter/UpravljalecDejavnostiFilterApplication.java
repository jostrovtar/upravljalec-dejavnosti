package com.jrovtar.services.upravljalecdejavnostifilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpravljalecDejavnostiFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpravljalecDejavnostiFilterApplication.class, args);
	}

}
