package com.jrovtar.services.upravljalecdejavnostifilter.filter;

import java.util.List;
import java.util.stream.Collectors;

public enum SortirniAtribut {

    NAZIV,
    DODELJENE_URE,
    OPRAVLJENE_URE;

    public static SortirniAtribut getEnum(String value) {

        if (value == null) {
            return null;
        }

        SortirniAtribut sortirniAtribut;

        try {
            sortirniAtribut = valueOf(value.toUpperCase());

        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(
                    "Vrednost sortirnega atributa ni pravilna. Pravilne vrednosti: " +
                            List.of(SortirniAtribut.class.getEnumConstants()).stream().map(SortirniAtribut::name).collect(Collectors.joining(", ")));
        }

        return sortirniAtribut;
    }
}
