package com.jrovtar.services.upravljalecdejavnostifilter.filter;

import com.jrovtar.services.upravljalecdejavnostifilter.napake.MejnikServiceException;
import com.jrovtar.services.upravljalecdejavnostifilter.napake.NalogaServiceException;
import com.jrovtar.services.upravljalecdejavnostifilter.napake.SestanekServiceException;

import java.util.List;

public interface FilterService {

    /**
     * Filtriraj naloge, mejnike in sestanke. Če je posamezen parameter za filtriranje negativen (oz. prazen string) se
     * le ta ne upošteva.
     *
     * @param nazivFilter            filter po naziv
     * @param zadolzenaOsebaIdFilter filter po zadolženi osebi
     * @param minDodeljeneUre        minimalno število dodeljenih ur
     * @param maxDodeljeneUre        maksimalno število dodeljenih ur
     * @param minOpravljeneUre       minimalno število opravljenih ur
     * @param maxOpravljeneUre       maksimalno število opravljenih ur
     * @param sortirniAtribut        sortirni atribut (za veljavne vrednosti poglej enum {@code SortirniAtribut})
     * @param narascajoceSortiranje  če je true se sortira naraščajoče, če false pa padajoče
     * @return seznam filtriranih in sortiranih nalog, mejnikov in sestankov
     * @throws NalogaServiceException   če klic naloga storitve vrne napako
     * @throws SestanekServiceException če klic sestanek storitve vrne napako
     * @throws MejnikServiceException   če klic mejnik storitve ven napako
     */
    List<FiltriranElementDTO> filtriraj(
            String nazivFilter,
            int zadolzenaOsebaIdFilter,
            double minDodeljeneUre,
            double maxDodeljeneUre,
            double minOpravljeneUre,
            double maxOpravljeneUre,
            String sortirniAtribut,
            boolean narascajoceSortiranje
    ) throws NalogaServiceException, SestanekServiceException, MejnikServiceException;
}
