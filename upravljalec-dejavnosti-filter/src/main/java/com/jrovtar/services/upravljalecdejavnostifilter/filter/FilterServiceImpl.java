package com.jrovtar.services.upravljalecdejavnostifilter.filter;

import com.jrovtar.services.upravljalecdejavnostifilter.klienti.MejnikServiceKlient;
import com.jrovtar.services.upravljalecdejavnostifilter.klienti.NalogaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostifilter.klienti.SestanekServiceKlient;
import com.jrovtar.services.upravljalecdejavnostifilter.napake.MejnikServiceException;
import com.jrovtar.services.upravljalecdejavnostifilter.napake.NalogaServiceException;
import com.jrovtar.services.upravljalecdejavnostifilter.napake.SestanekServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FilterServiceImpl implements FilterService {

    private MejnikServiceKlient mejnikServiceKlient;
    private SestanekServiceKlient sestanekServiceKlient;
    private NalogaServiceKlient nalogaServiceKlient;

    public FilterServiceImpl(
            MejnikServiceKlient mejnikServiceKlient,
            SestanekServiceKlient sestanekServiceKlient,
            NalogaServiceKlient nalogaServiceKlient) {
        this.mejnikServiceKlient = mejnikServiceKlient;
        this.sestanekServiceKlient = sestanekServiceKlient;
        this.nalogaServiceKlient = nalogaServiceKlient;
    }

    /**
     * Filtriraj naloge, mejnike in sestanke. Če je posamezen parameter za filtriranje negativen (oz. prazen string) se
     * le ta ne upošteva.
     *
     * @param nazivFilter            filter po naziv
     * @param zadolzenaOsebaIdFilter filter po zadolženi osebi
     * @param minDodeljeneUre        minimalno število dodeljenih ur
     * @param maxDodeljeneUre        maksimalno število dodeljenih ur
     * @param minOpravljeneUre       minimalno število opravljenih ur
     * @param maxOpravljeneUre       maksimalno število opravljenih ur
     * @param sortirniAtribut        sortirni atribut (za veljavne vrednosti poglej enum {@code SortirniAtribut})
     * @param narascajoceSortiranje  če je true se sortira naraščajoče, če false pa padajoče
     * @return seznam filtriranih in sortiranih nalog, mejnikov in sestankov
     * @throws NalogaServiceException   če klic naloga storitve vrne napako
     * @throws SestanekServiceException če klic sestanek storitve vrne napako
     * @throws MejnikServiceException   če klic mejnik storitve ven napako
     */
    public List<FiltriranElementDTO> filtriraj(
            String nazivFilter,
            int zadolzenaOsebaIdFilter,
            double minDodeljeneUre,
            double maxDodeljeneUre,
            double minOpravljeneUre,
            double maxOpravljeneUre,
            String sortirniAtribut,
            boolean narascajoceSortiranje
    ) throws NalogaServiceException, SestanekServiceException, MejnikServiceException {

        log.debug("Filtriranje entitet s parametri: " +
                        "nazivFilter={}, zadolzenaOsebaIdFilter={}, minDodeljeneUre={}, maxDodeljeneUre={}, " +
                        "minOpravljeneUre={}, maxOpravljeneUre={}, sortirniAtribut={}, narascajoceSortiranje={}",
                nazivFilter, zadolzenaOsebaIdFilter, minDodeljeneUre, maxDodeljeneUre,
                minOpravljeneUre, maxOpravljeneUre, sortirniAtribut, narascajoceSortiranje);

        // preveri parametre
        Assert.notNull(nazivFilter, "Filter naziva ne sme biti null.");

        // pripravi sortirni atribut
        SortirniAtribut sortirniAtributEnum = null;

        if (!StringUtils.isEmpty(sortirniAtribut)) {
            sortirniAtributEnum = SortirniAtribut.getEnum(sortirniAtribut);
        }

        if (minDodeljeneUre >= 0 && maxDodeljeneUre >= 0 &&
                minDodeljeneUre > maxDodeljeneUre) {
            throw new IllegalArgumentException(
                    "Minimalno število dodeljenih ur ne sme biti večje od maximalnega števila dodeljenih ur.");
        }

        if (minOpravljeneUre >= 0 && maxOpravljeneUre >= 0 &&
                minOpravljeneUre > maxOpravljeneUre) {
            throw new IllegalArgumentException(
                    "Minimalno število opravljenih ur ne sme biti večje od maximalnega števila opravljenih ur.");
        }

        // kreiraj seznam filtriranih elementov
        List<FiltriranElementDTO> filtriranSeznam =

                // filtriraj naloge
                nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ).getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        filtriranSeznam.addAll(

                // filtriraj sestanke
                sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ).getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        // če ja nastavljen filter min dodeljene ali opravljene ure potem ne filtriraj
        // po mejnikih, ker imajo 0 dodeljenih in opravljenih ur.
        if (minDodeljeneUre < 0.0 && minOpravljeneUre < 0.0) {
            filtriranSeznam.addAll(

                    // filtriraj mejnike
                    mejnikServiceKlient.filtriraj(
                            nazivFilter,
                            zadolzenaOsebaIdFilter
                    ).getItemsList().stream()
                            .map(e ->
                                    new FiltriranElementDTO(
                                            e.getNaziv(),
                                            BigDecimal.ZERO,
                                            BigDecimal.ZERO)
                            )
                            .collect(Collectors.toList())
            );
        }

        // sortiraj filtrirane rezultate
        if (sortirniAtributEnum != null) {

            Comparator comparator = null;

            if (SortirniAtribut.NAZIV.equals(sortirniAtributEnum)) {
                comparator = Comparator.comparing(FiltriranElementDTO::getNaziv);

            } else if (SortirniAtribut.DODELJENE_URE.equals(sortirniAtributEnum)) {
                comparator = Comparator.comparing(FiltriranElementDTO::getDodeljeneUre);

            } else if (SortirniAtribut.OPRAVLJENE_URE.equals(sortirniAtributEnum)) {
                comparator = Comparator.comparing(FiltriranElementDTO::getOpravljeneUre);

            } else {
                log.warn("Za sortirni atribut {} ni definiran Comparator. Izvedeno bo filtriranje po nazivu");
                comparator = Comparator.comparing(FiltriranElementDTO::getNaziv);
            }

            if (!narascajoceSortiranje) {
                comparator = comparator.reversed();
            }

            filtriranSeznam.sort(comparator);
        }

        return filtriranSeznam;
    }

}
