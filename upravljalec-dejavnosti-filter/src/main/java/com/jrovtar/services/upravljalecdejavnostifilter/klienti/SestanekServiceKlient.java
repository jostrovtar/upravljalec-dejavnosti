package com.jrovtar.services.upravljalecdejavnostifilter.klienti;

import com.jrovtar.services.upravljalecdejavnostifilter.napake.SestanekServiceException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekFiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekFiltrirajZahteva;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;

@Component
@Slf4j
public class SestanekServiceKlient {

    private NastavitveKlientov nastavitveKlientov;
    private ManagedChannel channel;
    private SestanekServiceGrpc.SestanekServiceFutureStub sestanekServiceFutureStub = null;

    public SestanekServiceKlient(NastavitveKlientov nastavitveKlientov) {
        this.nastavitveKlientov = nastavitveKlientov;
    }

    @PostConstruct
    public void init() {
        channel = ManagedChannelBuilder.forAddress(
                nastavitveKlientov.getSestanekServiceUrl(),
                nastavitveKlientov.getSestanekServicePort())
                .usePlaintext()
                .build();

        sestanekServiceFutureStub = SestanekServiceGrpc.newFutureStub(channel);
    }

    @PreDestroy
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    /**
     * Filtriraj sestanke.
     *
     * @param nazivFilter
     * @param zadolzenaOsebaIdFilter
     * @param minDodeljeneUre
     * @param maxDodeljeneUre
     * @param minOpravljeneUre
     * @param maxOpravljeneUre
     * @return seznam filtriranih sestankov
     * @throws SestanekServiceException če klic metode za filtriranje sestankov ni bil uspešen
     */
    public SestanekFiltrirajOdgovor filtriraj(
            String nazivFilter, int zadolzenaOsebaIdFilter, double minDodeljeneUre,
            double maxDodeljeneUre, double minOpravljeneUre, double maxOpravljeneUre) throws SestanekServiceException {

        Assert.notNull(nazivFilter, "Filter naziva ne sme biti null.");

        SestanekFiltrirajZahteva zahteva = SestanekFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxOpravljeneUre)
                .build();

        SestanekFiltrirajOdgovor odgovor;
        try {
            odgovor = sestanekServiceFutureStub.filtriraj(zahteva).get();

        } catch (Exception e) {
            throw new SestanekServiceException("Napaka pri klicu metode za filtriranje sestankov. " + e.getMessage(), e);
        }

        return odgovor;
    }

}
