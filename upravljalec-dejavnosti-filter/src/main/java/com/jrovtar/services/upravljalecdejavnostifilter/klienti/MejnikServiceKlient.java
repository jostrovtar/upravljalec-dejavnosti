package com.jrovtar.services.upravljalecdejavnostifilter.klienti;

import com.jrovtar.services.upravljalecdejavnostifilter.napake.MejnikServiceException;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikFiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikFiltrirajZahteva;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;

@Component
@Slf4j
public class MejnikServiceKlient {

    private NastavitveKlientov nastavitveKlientov;
    private ManagedChannel channel;
    private MejnikServiceGrpc.MejnikServiceFutureStub mejnikServiceFutureStub = null;

    public MejnikServiceKlient(NastavitveKlientov nastavitveKlientov) {
        this.nastavitveKlientov = nastavitveKlientov;
    }

    @PostConstruct
    public void init() {
        channel = ManagedChannelBuilder.forAddress(
                nastavitveKlientov.getMejnikServiceUrl(),
                nastavitveKlientov.getMejnikServicePort())
                .usePlaintext()
                .build();

        mejnikServiceFutureStub = MejnikServiceGrpc.newFutureStub(channel);
    }

    @PreDestroy
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    /**
     * Filtriraj mejnike.
     *
     * @param nazivFilter
     * @param zadolzenaOsebaIdFilter
     * @return seznam filtriranih mejnikov
     * @throws MejnikServiceException če klic metode za filtriranje mejnikov ni bil uspešen
     */
    public MejnikFiltrirajOdgovor filtriraj(
            String nazivFilter, int zadolzenaOsebaIdFilter) throws MejnikServiceException {

        Assert.notNull(nazivFilter, "Filter naziva ne sme biti null.");

        MejnikFiltrirajZahteva zahteva = MejnikFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .build();

        MejnikFiltrirajOdgovor odgovor;
        try {
            odgovor = mejnikServiceFutureStub.filtriraj(zahteva).get();

        } catch (Exception e) {
            throw new MejnikServiceException("Napaka pri klicu metode za filtriranje mejnikov. " + e.getMessage(), e);
        }

        return odgovor;
    }

}
