package com.jrovtar.services.upravljalecdejavnostifilter.napake;

public class SestanekServiceException extends Exception {

    public SestanekServiceException(String message) {
        super(message);
    }

    public SestanekServiceException(String message, Throwable t) {
        super(message, t);
    }
}
