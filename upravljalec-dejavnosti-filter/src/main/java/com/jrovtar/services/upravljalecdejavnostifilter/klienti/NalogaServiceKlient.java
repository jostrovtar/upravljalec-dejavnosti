package com.jrovtar.services.upravljalecdejavnostifilter.klienti;

import com.jrovtar.services.upravljalecdejavnostifilter.napake.NalogaServiceException;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaFiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaFiltrirajZahteva;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;

@Component
@Slf4j
public class NalogaServiceKlient {

    private NastavitveKlientov nastavitveKlientov;
    private ManagedChannel channel;
    private NalogaServiceGrpc.NalogaServiceFutureStub nalogaServiceFutureStub = null;

    public NalogaServiceKlient(NastavitveKlientov nastavitveKlientov) {
        this.nastavitveKlientov = nastavitveKlientov;
    }

    @PostConstruct
    public void init() {
        channel = ManagedChannelBuilder.forAddress(
                nastavitveKlientov.getNalogaServiceUrl(),
                nastavitveKlientov.getNalogaServicePort())
                .usePlaintext()
                .build();

        nalogaServiceFutureStub = NalogaServiceGrpc.newFutureStub(channel);
    }

    @PreDestroy
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    /**
     * Filtriraj naloge.
     *
     * @param nazivFilter
     * @param zadolzenaOsebaIdFilter
     * @param minDodeljeneUre
     * @param maxDodeljeneUre
     * @param minOpravljeneUre
     * @param maxOpravljeneUre
     * @return seznam filtriranih nalog
     * @throws NalogaServiceException če klic metode za filtriranje nalog ni bil uspešen
     */
    public NalogaFiltrirajOdgovor filtriraj(
            String nazivFilter, int zadolzenaOsebaIdFilter, double minDodeljeneUre,
            double maxDodeljeneUre, double minOpravljeneUre, double maxOpravljeneUre) throws NalogaServiceException {

        Assert.notNull(nazivFilter, "Filter naziva ne sme biti null.");

        NalogaFiltrirajZahteva zahteva = NalogaFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxOpravljeneUre)
                .build();

        NalogaFiltrirajOdgovor odgovor;
        try {
            odgovor = nalogaServiceFutureStub.filtriraj(zahteva).get();

        } catch (Exception e) {
            throw new NalogaServiceException("Napaka pri klicu metode za filtriranje nalog. " + e.getMessage(), e);
        }

        return odgovor;
    }

}
