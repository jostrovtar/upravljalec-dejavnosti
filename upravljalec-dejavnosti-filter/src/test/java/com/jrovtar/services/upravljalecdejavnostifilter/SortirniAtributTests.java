package com.jrovtar.services.upravljalecdejavnostifilter;

import com.jrovtar.services.upravljalecdejavnostifilter.filter.SortirniAtribut;
import org.junit.Assert;
import org.junit.Test;

public class SortirniAtributTests {

    @Test(expected = IllegalArgumentException.class)
    public void neObstaja() {

        String value = "val";
        SortirniAtribut.getEnum(value);
    }

    @Test
    public void ok_naziv() {

        String value = "naziv";
        SortirniAtribut sa = SortirniAtribut.getEnum(value);
        Assert.assertEquals(SortirniAtribut.NAZIV, sa);
    }

    @Test
    public void ok_dodeljeneUre() {

        String value = "dodeljene_ure";
        SortirniAtribut sa = SortirniAtribut.getEnum(value);
        Assert.assertEquals(SortirniAtribut.DODELJENE_URE, sa);
    }

    @Test
    public void ok_opravljeneUre() {

        String value = "opravljene_ure";
        SortirniAtribut sa = SortirniAtribut.getEnum(value);
        Assert.assertEquals(SortirniAtribut.OPRAVLJENE_URE, sa);
    }
}
