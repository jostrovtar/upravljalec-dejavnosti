package com.jrovtar.services.upravljalecdejavnostifilter;

import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajServiceGrpc;
import com.jrovtar.services.upravljalecdejavnostfiltritaj.filtriraj.grpc.FiltrirajZahteva;
import com.jrovtar.services.upravljalecdejavnostifilter.filter.FilterService;
import com.jrovtar.services.upravljalecdejavnostifilter.filter.FiltriranElementDTO;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "klienti.nalogaServicePort=6565",
        "klienti.nalogaServiceUrl=localhost",
        "klienti.sestanekServicePort=6565",
        "klienti.sestanekServiceUrl=localhost",
        "klienti.mejnikServicePort=6565",
        "klienti.mejnikServiceUrl=localhost",
        "grpc.port=0",
        "grpc.shutdownGrace=-1"
})
public class FilterGrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private FilterService filterService;

    private FiltrirajServiceGrpc.FiltrirajServiceFutureStub filtrirajServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        filtrirajServiceFutureStub = FiltrirajServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void filtriraj_ok() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1.0;
        double maxDodeljeneUre = -1.0;
        double minOpravljeneUre = -1.0;
        double maxOpravljeneUre = -1.0;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        List<FiltriranElementDTO> testList =
                List.of(
                        new FiltriranElementDTO("Naziv 1", BigDecimal.valueOf(1), BigDecimal.valueOf(2)),
                        new FiltriranElementDTO("Naziv 2", BigDecimal.valueOf(1.5), BigDecimal.valueOf(2.5))
                );

        // mock metod
        Mockito
                .when(filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                ))
                .thenAnswer((Answer<List<FiltriranElementDTO>>) invocation -> testList);

        // klic testirane metode
        FiltrirajZahteva zahteva = FiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxDodeljeneUre)
                .setSortirniAtribut(sortirniAtribut)
                .setNarascajoceSortiranje(narascajoceSortiranje)
                .build();
        FiltrirajOdgovor odgovor = filtrirajServiceFutureStub.filtriraj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(testList.size(), odgovor.getItemsCount());
        Assert.assertEquals("", odgovor.getNapaka());
        Assert.assertEquals("", odgovor.getNapakaSporocilo());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), odgovor.getItems(i).getNaziv());
            Assert.assertEquals(0,
                    testList.get(i).getDodeljeneUre().compareTo(
                            BigDecimal.valueOf(odgovor.getItems(i).getDodeljeneUre())));
            Assert.assertEquals(0,
                    testList.get(i).getOpravljeneUre().compareTo(
                            BigDecimal.valueOf(odgovor.getItems(i).getOpravljeneUre())));
        }
    }

    @SneakyThrows
    @Test
    final public void filtriraj_napaka() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1.0;
        double maxDodeljeneUre = -1.0;
        double minOpravljeneUre = -1.0;
        double maxOpravljeneUre = -1.0;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                ))
                .thenThrow(exception);

        // klic testirane metode
        FiltrirajZahteva zahteva = FiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxDodeljeneUre)
                .setSortirniAtribut(sortirniAtribut)
                .setNarascajoceSortiranje(narascajoceSortiranje)
                .build();
        FiltrirajOdgovor odgovor = filtrirajServiceFutureStub.filtriraj(zahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, odgovor.getItemsCount());
        Assert.assertEquals(exception.getClass().getSimpleName(), odgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), odgovor.getNapakaSporocilo());
    }
}
