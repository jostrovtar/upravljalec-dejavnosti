package com.jrovtar.services.upravljalecdejavnostifilter;

import com.jrovtar.services.upravljalecdejavnostifilter.filter.FilterService;
import com.jrovtar.services.upravljalecdejavnostifilter.filter.FilterServiceImpl;
import com.jrovtar.services.upravljalecdejavnostifilter.filter.FiltriranElementDTO;
import com.jrovtar.services.upravljalecdejavnostifilter.klienti.MejnikServiceKlient;
import com.jrovtar.services.upravljalecdejavnostifilter.klienti.NalogaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostifilter.klienti.SestanekServiceKlient;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikFiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikNazivOdgovor;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaFiltrirajItem;
import com.jrovtar.services.upravljalecdejavnostinaloga.naloga.grpc.NalogaFiltrirajOdgovor;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekFiltrirajItem;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekFiltrirajOdgovor;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
public class FilterServiceImplTests {

    @MockBean
    private MejnikServiceKlient mejnikServiceKlient;

    @MockBean
    private SestanekServiceKlient sestanekServiceKlient;

    @MockBean
    private NalogaServiceKlient nalogaServiceKlient;

    private FilterService filterService;

    @Before
    public void init() {
        filterService = new FilterServiceImpl(
                mejnikServiceKlient,
                sestanekServiceKlient,
                nalogaServiceKlient);
    }

    @SneakyThrows
    @Test
    public void filtriraj_ok() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // mejnik filtriraj odgovor
        MejnikFiltrirajOdgovor mejnikFiltrirajOdgovor = MejnikFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 1").build(),
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 2").build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        testList.addAll(
                // mejniki
                mejnikFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.ZERO,
                                        BigDecimal.ZERO)
                        )
                        .collect(Collectors.toList())
        );

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        Mockito
                .when(mejnikServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter
                ))
                .thenReturn(mejnikFiltrirajOdgovor);


        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_nazivNull() {

        // priprava testnih podatkov
        String nazivFilter = null;
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        // klic testirane metode
        filterService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre,
                sortirniAtribut,
                narascajoceSortiranje
        );
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_napakaDodeljeneUre() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = 5;
        double maxDodeljeneUre = 2;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        // klic testirane metode
        filterService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre,
                sortirniAtribut,
                narascajoceSortiranje
        );
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_napakaOpravljeneUre() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = 10;
        double maxOpravljeneUre = 1;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        // klic testirane metode
        filterService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre,
                sortirniAtribut,
                narascajoceSortiranje
        );
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_napakaSortirniAtribut() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "Test";
        boolean narascajoceSortiranje = true;

        // klic testirane metode
        filterService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre,
                sortirniAtribut,
                narascajoceSortiranje
        );
    }

    @SneakyThrows
    @Test
    public void filtriraj_neFiltrirajMejnikovMinDodeljeneUre() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = 1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());
        Mockito.verify(mejnikServiceKlient, Mockito.times(0)).filtriraj(nazivFilter, zadolzenaOsebaIdFilter);

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }

    @SneakyThrows
    @Test
    public void filtriraj_neFiltrirajMejnikovMinOpravljeneUre() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = 1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "";
        boolean narascajoceSortiranje = true;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());
        Mockito.verify(mejnikServiceKlient, Mockito.times(0)).filtriraj(nazivFilter, zadolzenaOsebaIdFilter);

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }

    @SneakyThrows
    @Test
    public void filtriraj_urediNaziv() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "naziv";
        boolean narascajoceSortiranje = true;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // mejnik filtriraj odgovor
        MejnikFiltrirajOdgovor mejnikFiltrirajOdgovor = MejnikFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 1").build(),
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 2").build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        testList.addAll(
                // mejniki
                mejnikFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.ZERO,
                                        BigDecimal.ZERO)
                        )
                        .collect(Collectors.toList())
        );
        testList.sort(Comparator.comparing(FiltriranElementDTO::getNaziv));

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        Mockito
                .when(mejnikServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter
                ))
                .thenReturn(mejnikFiltrirajOdgovor);


        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }

    @SneakyThrows
    @Test
    public void filtriraj_urediNazivDesc() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "naziv";
        boolean narascajoceSortiranje = false;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // mejnik filtriraj odgovor
        MejnikFiltrirajOdgovor mejnikFiltrirajOdgovor = MejnikFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 1").build(),
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 2").build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        testList.addAll(
                // mejniki
                mejnikFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.ZERO,
                                        BigDecimal.ZERO)
                        )
                        .collect(Collectors.toList())
        );
        testList.sort(Comparator.comparing(FiltriranElementDTO::getNaziv).reversed());

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        Mockito
                .when(mejnikServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter
                ))
                .thenReturn(mejnikFiltrirajOdgovor);


        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }

    @SneakyThrows
    @Test
    public void filtriraj_urediDodeljeneUre() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "dodeljene_ure";
        boolean narascajoceSortiranje = true;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // mejnik filtriraj odgovor
        MejnikFiltrirajOdgovor mejnikFiltrirajOdgovor = MejnikFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 1").build(),
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 2").build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        testList.addAll(
                // mejniki
                mejnikFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.ZERO,
                                        BigDecimal.ZERO)
                        )
                        .collect(Collectors.toList())
        );
        testList.sort(Comparator.comparing(FiltriranElementDTO::getDodeljeneUre));

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        Mockito
                .when(mejnikServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter
                ))
                .thenReturn(mejnikFiltrirajOdgovor);


        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }

    @SneakyThrows
    @Test
    public void filtriraj_urediOpravljeneUre() {

        // priprava testnih podatkov
        String nazivFilter = "";
        int zadolzenaOsebaIdFilter = -1;
        double minDodeljeneUre = -1;
        double maxDodeljeneUre = -1;
        double minOpravljeneUre = -1;
        double maxOpravljeneUre = -1;
        String sortirniAtribut = "opravljene_ure";
        boolean narascajoceSortiranje = true;

        // naloga filtriraj odgovor
        NalogaFiltrirajOdgovor nalogaFiltrirajOdgovor = NalogaFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 1").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build(),
                                NalogaFiltrirajItem.newBuilder().setNaziv("Naloga 2").setDodeljeneUre(10.5).setOpravljeneUre(5.5).build()
                        )
                )
                .build();

        // sestanek filtriraj odgovor
        SestanekFiltrirajOdgovor sestanekFiltrirajOdgovor = SestanekFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 1").setDodeljeneUre(3.5).setOpravljeneUre(5.5).build(),
                                SestanekFiltrirajItem.newBuilder().setNaziv("Sestanek 2").setDodeljeneUre(4.5).setOpravljeneUre(6.5).build()
                        )
                )
                .build();

        // mejnik filtriraj odgovor
        MejnikFiltrirajOdgovor mejnikFiltrirajOdgovor = MejnikFiltrirajOdgovor.newBuilder()
                .addAllItems(
                        List.of(
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 1").build(),
                                MejnikNazivOdgovor.newBuilder().setNaziv("Mejnik 2").build()
                        )
                )
                .build();

        // skupna lista
        List<FiltriranElementDTO> testList =
                // naloge
                nalogaFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList());

        testList.addAll(
                // sestanki
                sestanekFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.valueOf(e.getDodeljeneUre()),
                                        BigDecimal.valueOf(e.getOpravljeneUre()))
                        )
                        .collect(Collectors.toList())
        );

        testList.addAll(
                // mejniki
                mejnikFiltrirajOdgovor.getItemsList().stream()
                        .map(e ->
                                new FiltriranElementDTO(
                                        e.getNaziv(),
                                        BigDecimal.ZERO,
                                        BigDecimal.ZERO)
                        )
                        .collect(Collectors.toList())
        );
        testList.sort(Comparator.comparing(FiltriranElementDTO::getOpravljeneUre));

        // mock metod
        Mockito
                .when(nalogaServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(nalogaFiltrirajOdgovor);

        Mockito
                .when(sestanekServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre
                ))
                .thenReturn(sestanekFiltrirajOdgovor);

        Mockito
                .when(mejnikServiceKlient.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter
                ))
                .thenReturn(mejnikFiltrirajOdgovor);


        // klic testirane metode
        List<FiltriranElementDTO> list =
                filterService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre,
                        sortirniAtribut,
                        narascajoceSortiranje
                );

        Assert.assertEquals(testList.size(), list.size());

        for (int i = 0; i < testList.size(); i++) {
            Assert.assertEquals(testList.get(i).getNaziv(), list.get(i).getNaziv());
            Assert.assertEquals(testList.get(i).getDodeljeneUre(), list.get(i).getDodeljeneUre());
            Assert.assertEquals(testList.get(i).getOpravljeneUre(), list.get(i).getOpravljeneUre());
        }
    }
}
