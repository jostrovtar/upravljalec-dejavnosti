
# Create database
CREATE DATABASE upravljalec_dejavnosti_sestanek CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

# Build project
mvn clean package

# Create table in db
updateDb.bat

# Build docker image
docker build . -t upravljalec-dejavnosti-sestanek:1.0

# Create container and run it (change datasource url credentials and ports)
docker run --name upravljalec-dejavnosti-sestanek-1-0 -e SPRING_DATASOURCE_URL='jdbc:mysql://172.17.0.2:3306/upravljalec_dejavnosti_sestanek?useUnicode=true&characterEncoding=utf8&serverTimezone=UTC' -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=password1! -e KLIENTI_OSEBA_SERVICE_URL=172.17.0.3 -e KLIENTI_OSEBA_SERVICE_PORT=6560 -e GRPC_PORT=6562 -d -p 6562:6562 upravljalec-dejavnosti-sestanek:1.0
