package com.jrovtar.services.upravljalecdejavnostisestanek;

import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import com.jrovtar.services.upravljalecdejavnostisestanek.klienti.OsebaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.ZadolzeniZaSestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.Sestanek;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.SestanekRepository;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.*;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
public class ZadolzeniZaSestanekServiceImplTests {

    @MockBean
    private SestanekRepository sestanekRepository;

    @MockBean
    private ZadolzeniZaSestanekRepository zadolzeniZaSestanekRepository;

    @MockBean
    private OsebaServiceKlient osebaServiceKlient;

    private ZadolzeniZaSestanekService zadolzeniZaSestanekService;

    @Before
    public void init() {
        zadolzeniZaSestanekService = new ZadolzeniZaSestanekServiceImpl(
                sestanekRepository,
                zadolzeniZaSestanekRepository,
                osebaServiceKlient);
    }

    @SneakyThrows
    @Test
    public void dodaj_ok() {

        // priprava testnih podatkov
        Integer sestanekId = 1;
        Integer osebaId = 2;

        Sestanek s = new Sestanek();
        s.setId(sestanekId);

        // mock metod
        Mockito.when(sestanekRepository.findById(s.getId()))
                .thenReturn(java.util.Optional.of(s));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        Mockito
                .when(zadolzeniZaSestanekRepository.save(ArgumentMatchers.any(ZadolzeniZaSestanek.class)))
                .thenAnswer((Answer<ZadolzeniZaSestanek>) invocation -> invocation.getArgument(0));

        // klic testirane metode
        ZadolzeniZaSestanek zadolzeniZaSestanek = zadolzeniZaSestanekService.dodaj(sestanekId, osebaId);

        // preverjanje rezultatov
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).save(ArgumentMatchers.any(ZadolzeniZaSestanek.class));
        Assert.assertNotNull(zadolzeniZaSestanek.getZadolzeniZaSestanekPk());
        Assert.assertEquals(sestanekId, zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getSestanekId());
        Assert.assertEquals(osebaId, zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getOsebaId());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_sestanekIdNull() {
        zadolzeniZaSestanekService.dodaj(null, 1);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_osebaIdNull() {
        zadolzeniZaSestanekService.dodaj(1, null);
    }

    @SneakyThrows
    @Test(expected = SestanekNeObstajaException.class)
    public void dodaj_sestanekNeObstaja() {
        zadolzeniZaSestanekService.dodaj(1, 1);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void dodaj_osebaNeObstaja() {

        // priprava testnih podatkov
        Integer sestanekId = 1;
        Integer osebaId = 2;

        Sestanek s = new Sestanek();
        s.setId(sestanekId);

        // mock metod
        Mockito.when(sestanekRepository.findById(s.getId()))
                .thenReturn(java.util.Optional.of(s));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        zadolzeniZaSestanekService.dodaj(sestanekId, osebaId);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void dodaj_osebaServiceException() {

        // priprava testnih podatkov
        Integer sestanekId = 1;
        Integer osebaId = 2;

        Sestanek s = new Sestanek();
        s.setId(sestanekId);

        // mock metod
        Mockito.when(sestanekRepository.findById(s.getId()))
                .thenReturn(java.util.Optional.of(s));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("ExecutionException").build()
                );

        // klic testirane metode
        zadolzeniZaSestanekService.dodaj(sestanekId, osebaId);
    }

    @SneakyThrows
    @Test
    public void odstrani_ok() {

        // priprava testnih podatkov
        Integer sestanekId = 1;
        Integer osebaId = 2;

        Sestanek s = new Sestanek();
        s.setId(sestanekId);

        // mock metod
        Mockito
                .when(zadolzeniZaSestanekRepository.findById(ArgumentMatchers.any(ZadolzeniZaSestanekPk.class)))
                .thenAnswer((Answer<Optional<ZadolzeniZaSestanek>>) invocation -> {
                    ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
                    zzs.setZadolzeniZaSestanekPk(invocation.getArgument(0));
                    return Optional.of(zzs);
                });

        // klic testirane metode
        ZadolzeniZaSestanek zadolzeniZaSestanek = zadolzeniZaSestanekService.odstrani(sestanekId, osebaId);

        // preverjanje rezultatov
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).delete(ArgumentMatchers.any(ZadolzeniZaSestanek.class));
        Assert.assertNotNull(zadolzeniZaSestanek.getZadolzeniZaSestanekPk());
        Assert.assertEquals(sestanekId, zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getSestanekId());
        Assert.assertEquals(osebaId, zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getOsebaId());
    }

    @SneakyThrows
    @Test(expected = ZadolzeniZaSestanekNeObstajaException.class)
    public void odstrani_neObstaja() {

        zadolzeniZaSestanekService.odstrani(1, 1);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void odstrani_sestanekIdNull() {

        zadolzeniZaSestanekService.odstrani(null, 1);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void odstrani_osebaIdNull() {

        zadolzeniZaSestanekService.odstrani(1, null);
    }

    @SneakyThrows
    @Test
    public void preveriObstojOsebe_ok() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        // klic testirane metode
        zadolzeniZaSestanekService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void preveriObstojOsebe_osebaNeObstaja() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        zadolzeniZaSestanekService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void preveriObstojOsebe_osebaServiceNapaka() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("Exception").build()
                );

        // klic testirane metode
        zadolzeniZaSestanekService.preveriObstojOsebe(1);
    }
}
