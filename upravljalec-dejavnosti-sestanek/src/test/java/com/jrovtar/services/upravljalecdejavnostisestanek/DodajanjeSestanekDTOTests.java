package com.jrovtar.services.upravljalecdejavnostisestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.SestanekDTO;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class DodajanjeSestanekDTOTests {

    private static Validator validator;
    private static Class[] group;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        group = new Class[]{SestanekDTO.ObDodajanju.class};
    }

    @Test
    public void nazivNull() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv(null);
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.MINUTES));
        sestanek.setUraZakljucka(LocalTime.now());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void nazivPrazen() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv("");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.MINUTES));
        sestanek.setUraZakljucka(LocalTime.now());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void nazivPredolg() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv(StringUtils.repeat("*", 256));
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.MINUTES));
        sestanek.setUraZakljucka(LocalTime.now());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void datumNull() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv("naziv");
        sestanek.setDatum(null);
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.MINUTES));
        sestanek.setUraZakljucka(LocalTime.now());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("datum");
    }

    @Test
    public void datumUraZacetkaNull() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv("naziv");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(null);
        sestanek.setUraZakljucka(LocalTime.now());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("uraZacetka");
    }

    @Test
    public void datumUraZakljuckaNull() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv("naziv");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(null);

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("uraZakljucka");
    }

    @Test
    public void uraZacetkaNiPredUroZakljucka() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv("naziv");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(sestanek.getUraZacetka());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("uraZacetkaPredUroZakljucka");
    }

    @Test
    public void ok() {

        SestanekDTO sestanek = new SestanekDTO();
        sestanek.setNaziv("naziv");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.MINUTES));
        sestanek.setUraZakljucka(LocalTime.now());

        Set<ConstraintViolation<SestanekDTO>> violations =
                validator.validate(sestanek, group);

        assertThat(violations.size()).isEqualTo(0);
    }
}
