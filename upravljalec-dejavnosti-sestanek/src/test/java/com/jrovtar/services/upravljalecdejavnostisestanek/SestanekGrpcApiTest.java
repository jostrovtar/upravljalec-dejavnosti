package com.jrovtar.services.upravljalecdejavnostisestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.ZadolzeniZaSestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.*;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.*;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanek;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekPk;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekService;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "klienti.osebaServicePort=6565",
        "klienti.osebaServiceUrl=localhost",
        "grpc.port=0",
        "grpc.shutdownGrace=-1"
})
public class SestanekGrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private SestanekService sestanekService;

    private SestanekServiceGrpc.SestanekServiceFutureStub sestanekServiceFutureStub = null;

    @MockBean
    private ZadolzeniZaSestanekService zadolzeniZaSestanekService;

    private SestanekZadolzeniServiceGrpc.SestanekZadolzeniServiceFutureStub zadolzeniServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        sestanekServiceFutureStub = SestanekServiceGrpc.newFutureStub(channel);
        zadolzeniServiceFutureStub = SestanekZadolzeniServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void vrni_ok() {

        // priprava testnih podatkov
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(2);

        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek 1");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.HOURS));
        sestanek.setZadolzeniZaSestanekList(List.of(zzs));

        // mock metod
        Mockito
                .when(sestanekService.vrni(sestanek.getId(), true))
                .thenAnswer((Answer<Sestanek>) invocation -> sestanek);

        // klic testirane metode
        SestanekVrniZahteva sestanekVrniZahteva = SestanekVrniZahteva.newBuilder().setId(sestanek.getId()).build();
        SestanekVrniOdgovor sestanekOdgovor = sestanekServiceFutureStub.vrni(sestanekVrniZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(sestanek.getId(), Integer.valueOf(sestanekOdgovor.getId()));
        Assert.assertEquals(sestanek.getNaziv(), sestanekOdgovor.getNaziv());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getDatum()), sestanekOdgovor.getDatum());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZacetka()), sestanekOdgovor.getUraZacetka());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZakljucka()), sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());
        Assert.assertEquals(1, sestanekOdgovor.getOsebeCount());
        Assert.assertEquals(
                zzs.getZadolzeniZaSestanekPk().getOsebaId(),
                Integer.valueOf(sestanekOdgovor.getOsebeList().get(0).getOsebaId()));
    }

    @SneakyThrows
    @Test
    final public void vrni_napaka() {

        // priprava testnih podatkov
        Integer id = 1;
        boolean vrniZadolzene = true;

        Exception exception = new SestanekNeObstajaException(
                String.format(SestanekServiceImpl.MESSAGE_SESTANEK_NE_OBSTAJA, id));

        // mock metod
        Mockito
                .when(sestanekService.vrni(id, vrniZadolzene))
                .thenThrow(exception);

        // klic testirane metode
        SestanekVrniZahteva sestanekVrniZahteva = SestanekVrniZahteva.newBuilder().setId(id).build();
        SestanekVrniOdgovor sestanekOdgovor = sestanekServiceFutureStub.vrni(sestanekVrniZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getId());
        Assert.assertEquals("", sestanekOdgovor.getNaziv());
        Assert.assertEquals("", sestanekOdgovor.getDatum());
        Assert.assertEquals("", sestanekOdgovor.getUraZacetka());
        Assert.assertEquals("", sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
        Assert.assertEquals(0, sestanekOdgovor.getOsebeCount());
    }

    @SneakyThrows
    @Test
    final public void dodaj_ok() {

        // priprava testnih podatkov
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(2);

        Sestanek sestanek = new Sestanek();
        sestanek.setNaziv("Sestanek 1");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.HOURS));
        sestanek.setZadolzeniZaSestanekList(List.of(zzs));

        // mock metod
        Mockito
                .when(sestanekService.dodaj(ArgumentMatchers.any()))
                .thenAnswer((Answer<Sestanek>) invocation -> {
                    SestanekDTO sInv = invocation.getArgument(0);
                    Sestanek s = new Sestanek();
                    s.setId(new Random().nextInt(10) + 1);
                    s.setNaziv(sInv.getNaziv());
                    s.setDatum(sInv.getDatum());
                    s.setUraZacetka(sInv.getUraZacetka());
                    s.setUraZakljucka(sInv.getUraZakljucka());
                    return s;
                });

        // klic testirane metode
        SestanekDodajZahteva sestanekDodajZahteva = SestanekDodajZahteva.newBuilder()
                .setNaziv(sestanek.getNaziv())
                .setDatum(SestanekUtils.formatiraj(sestanek.getDatum()))
                .setUraZacetka(SestanekUtils.formatiraj(sestanek.getUraZacetka()))
                .setUraZakljucka(SestanekUtils.formatiraj(sestanek.getUraZakljucka()))
                .build();
        SestanekOdgovor sestanekOdgovor = sestanekServiceFutureStub.dodaj(sestanekDodajZahteva).get();

        // preverjanje rezultatov
        Assert.assertTrue(sestanekOdgovor.getId() != 0);
        Assert.assertEquals(sestanek.getNaziv(), sestanekOdgovor.getNaziv());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getDatum()), sestanekOdgovor.getDatum());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZacetka()), sestanekOdgovor.getUraZacetka());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZakljucka()), sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_napaka() {

        // priprava testnih podatkov
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(2);

        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek 1");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.HOURS));
        sestanek.setZadolzeniZaSestanekList(List.of(zzs));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(sestanekService.dodaj(ArgumentMatchers.any(SestanekDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        SestanekDodajZahteva sestanekDodajZahteva = SestanekDodajZahteva.newBuilder()
                .setNaziv(sestanek.getNaziv())
                .setDatum(SestanekUtils.formatiraj(sestanek.getDatum()))
                .setUraZacetka(SestanekUtils.formatiraj(sestanek.getUraZacetka()))
                .setUraZakljucka(SestanekUtils.formatiraj(sestanek.getUraZakljucka()))
                .build();
        SestanekOdgovor sestanekOdgovor = sestanekServiceFutureStub.dodaj(sestanekDodajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getId());
        Assert.assertEquals("", sestanekOdgovor.getNaziv());
        Assert.assertEquals("", sestanekOdgovor.getDatum());
        Assert.assertEquals("", sestanekOdgovor.getUraZacetka());
        Assert.assertEquals("", sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_ok() {

        // priprava testnih podatkov
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(2);

        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek 1");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.HOURS));
        sestanek.setZadolzeniZaSestanekList(List.of(zzs));

        // mock metod
        Mockito
                .when(sestanekService.posodobi(ArgumentMatchers.any(SestanekDTO.class)))
                .thenAnswer((Answer<Sestanek>) invocation -> sestanek);

        // klic testirane metode
        SestanekPosodobiZahteva sestanekPosodobiZahteva = SestanekPosodobiZahteva.newBuilder()
                .setId(sestanek.getId())
                .setNaziv(sestanek.getNaziv())
                .setDatum(SestanekUtils.formatiraj(sestanek.getDatum()))
                .setUraZacetka(SestanekUtils.formatiraj(sestanek.getUraZacetka()))
                .setUraZakljucka(SestanekUtils.formatiraj(sestanek.getUraZakljucka()))
                .build();
        SestanekOdgovor sestanekOdgovor = sestanekServiceFutureStub.posodobi(sestanekPosodobiZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(sestanek.getId(), Integer.valueOf(sestanekOdgovor.getId()));
        Assert.assertEquals(sestanek.getNaziv(), sestanekOdgovor.getNaziv());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getDatum()), sestanekOdgovor.getDatum());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZacetka()), sestanekOdgovor.getUraZacetka());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZakljucka()), sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_napaka() {

        // priprava testnih podatkov
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(2);

        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek 1");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.HOURS));
        sestanek.setZadolzeniZaSestanekList(List.of(zzs));

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(sestanekService.posodobi(ArgumentMatchers.any(SestanekDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        SestanekPosodobiZahteva sestanekPosodobiZahteva = SestanekPosodobiZahteva.newBuilder()
                .setId(sestanek.getId())
                .setNaziv(sestanek.getNaziv())
                .setDatum(SestanekUtils.formatiraj(sestanek.getDatum()))
                .setUraZacetka(SestanekUtils.formatiraj(sestanek.getUraZacetka()))
                .setUraZakljucka(SestanekUtils.formatiraj(sestanek.getUraZakljucka()))
                .build();
        SestanekOdgovor sestanekOdgovor = sestanekServiceFutureStub.posodobi(sestanekPosodobiZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getId());
        Assert.assertEquals("", sestanekOdgovor.getNaziv());
        Assert.assertEquals("", sestanekOdgovor.getDatum());
        Assert.assertEquals("", sestanekOdgovor.getUraZacetka());
        Assert.assertEquals("", sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_ok() {

        // priprava testnih podatkov
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(2);

        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek 1");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now());
        sestanek.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.HOURS));
        sestanek.setZadolzeniZaSestanekList(List.of(zzs));

        // mock metod
        Mockito
                .when(sestanekService.izbrisi(ArgumentMatchers.any()))
                .thenAnswer((Answer<Sestanek>) invocation -> sestanek);

        // klic testirane metode
        SestanekIzbrisiZahteva sestanekDodajZahteva = SestanekIzbrisiZahteva.newBuilder()
                .setId(sestanek.getId())
                .build();
        SestanekOdgovor sestanekOdgovor = sestanekServiceFutureStub.izbrisi(sestanekDodajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(sestanek.getId(), Integer.valueOf(sestanekOdgovor.getId()));
        Assert.assertEquals(sestanek.getNaziv(), sestanekOdgovor.getNaziv());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getDatum()), sestanekOdgovor.getDatum());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZacetka()), sestanekOdgovor.getUraZacetka());
        Assert.assertEquals(SestanekUtils.formatiraj(sestanek.getUraZakljucka()), sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_napaka() {

        // priprava testnih podatkov
        Integer id = 1;
        Exception exception = new SestanekNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(sestanekService.izbrisi(id))
                .thenThrow(exception);

        // klic testirane metode
        SestanekIzbrisiZahteva sestanekDodajZahteva = SestanekIzbrisiZahteva.newBuilder()
                .setId(id)
                .build();
        SestanekOdgovor sestanekOdgovor = sestanekServiceFutureStub.izbrisi(sestanekDodajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getId());
        Assert.assertEquals("", sestanekOdgovor.getNaziv());
        Assert.assertEquals("", sestanekOdgovor.getDatum());
        Assert.assertEquals("", sestanekOdgovor.getUraZacetka());
        Assert.assertEquals("", sestanekOdgovor.getUraZakljucka());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void filtriraj_ok() {

        // priprava testnih podatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = -1.0;
        Double maxDodeljeneUre = -1.0;
        Double minOpravljeneUre = -1.0;
        Double maxOpravljeneUre = -1.0;

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv("Sestanek 1");
        fs.setDodeljeneUre(5.0);
        fs.setOpravljeneUre(5.0);

        FiltrirajSestanekDTO fs2 = new FiltrirajSestanekDTO();
        fs2.setNaziv("Sestanek 2");
        fs2.setDodeljeneUre(6.0);
        fs2.setOpravljeneUre(6.0);

        List<FiltrirajSestanekDTO> list = List.of(fs, fs2);

        // mock metod
        Mockito
                .when(sestanekService.filtriraj(
                        nazivFilter, zadolzenaOsebaIdFilter, minDodeljeneUre, maxDodeljeneUre,
                        minDodeljeneUre, maxOpravljeneUre))
                .thenAnswer((Answer<List<FiltrirajSestanekDTO>>) invocation -> list);

        // klic testirane metode
        SestanekFiltrirajZahteva sestanekFiltrirajZahteva = SestanekFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxOpravljeneUre)
                .build();
        SestanekFiltrirajOdgovor sestanekOdgovor =
                sestanekServiceFutureStub.filtriraj(sestanekFiltrirajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(list.size(), sestanekOdgovor.getItemsCount());
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());

        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(
                    list.get(i).getNaziv(),
                    sestanekOdgovor.getItems(i).getNaziv());
            Assert.assertEquals(
                    Double.valueOf(list.get(i).getDodeljeneUre()),
                    Double.valueOf(sestanekOdgovor.getItems(i).getDodeljeneUre()));
            Assert.assertEquals(
                    Double.valueOf(list.get(i).getOpravljeneUre()),
                    Double.valueOf(sestanekOdgovor.getItems(i).getOpravljeneUre()));
        }
    }

    @SneakyThrows
    @Test
    final public void filtriraj_napaka() {

        // priprava testnih podatkov
        String nazivFilter = "";
        Integer zadolzenaOsebaIdFilter = -1;
        Double minDodeljeneUre = -1.0;
        Double maxDodeljeneUre = -1.0;
        Double minOpravljeneUre = -1.0;
        Double maxOpravljeneUre = -1.0;

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metod
        Mockito
                .when(sestanekService.filtriraj(
                        nazivFilter, zadolzenaOsebaIdFilter, minDodeljeneUre, maxDodeljeneUre,
                        minDodeljeneUre, maxOpravljeneUre))
                .thenThrow(exception);

        // klic testirane metode
        SestanekFiltrirajZahteva sestanekFiltrirajZahteva = SestanekFiltrirajZahteva.newBuilder()
                .setFilterNaziv(nazivFilter)
                .setFilterZadolzenaOseba(zadolzenaOsebaIdFilter)
                .setMinDodeljeneUre(minDodeljeneUre)
                .setMaxDodeljeneUre(maxDodeljeneUre)
                .setMinOpravljeneUre(minOpravljeneUre)
                .setMaxOpravljeneUre(maxOpravljeneUre)
                .build();
        SestanekFiltrirajOdgovor sestanekOdgovor =
                sestanekServiceFutureStub.filtriraj(sestanekFiltrirajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getItemsCount());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodajZadolzenega_ok() {

        // priprava testnih poatkov
        Integer sestanekId = 5;
        Integer osebaId = 10;

        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(osebaId);
        zzs.getZadolzeniZaSestanekPk().setSestanekId(sestanekId);

        // mock metod
        Mockito
                .when(zadolzeniZaSestanekService.dodaj(sestanekId, osebaId))
                .thenAnswer((Answer<ZadolzeniZaSestanek>) invocation -> zzs);

        // klic testirane metode
        SestanekZadolzeniZahteva sestanekZadolzeniZahteva = SestanekZadolzeniZahteva.newBuilder()
                .setSestanekId(sestanekId)
                .setOsebaId(osebaId)
                .build();
        SestanekZadolzeniOdgovor sestanekOdgovor = zadolzeniServiceFutureStub.dodajZadolzenega(sestanekZadolzeniZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(sestanekId, Integer.valueOf(sestanekOdgovor.getSestanekId()));
        Assert.assertEquals(osebaId, Integer.valueOf(sestanekOdgovor.getOsebaId()));
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodajZadolzenega_napaka() {

        // priprava testnih poatkov
        Integer sestanekId = 5;
        Integer osebaId = 10;

        Exception exception = new SestanekNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(zadolzeniZaSestanekService.dodaj(sestanekId, osebaId))
                .thenThrow(exception);

        // klic testirane metode
        SestanekZadolzeniZahteva sestanekZadolzeniZahteva = SestanekZadolzeniZahteva.newBuilder()
                .setSestanekId(sestanekId)
                .setOsebaId(osebaId)
                .build();
        SestanekZadolzeniOdgovor sestanekOdgovor =
                zadolzeniServiceFutureStub.dodajZadolzenega(sestanekZadolzeniZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getSestanekId());
        Assert.assertEquals(0, sestanekOdgovor.getOsebaId());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void odstraniZadolzenega_ok() {

        // priprava testnih poatkov
        Integer sestanekId = 5;
        Integer osebaId = 10;

        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(osebaId);
        zzs.getZadolzeniZaSestanekPk().setSestanekId(sestanekId);

        // mock metod
        Mockito
                .when(zadolzeniZaSestanekService.odstrani(sestanekId, osebaId))
                .thenAnswer((Answer<ZadolzeniZaSestanek>) invocation -> zzs);

        // klic testirane metode
        SestanekZadolzeniZahteva sestanekZadolzeniZahteva = SestanekZadolzeniZahteva.newBuilder()
                .setSestanekId(sestanekId)
                .setOsebaId(osebaId)
                .build();
        SestanekZadolzeniOdgovor sestanekOdgovor = zadolzeniServiceFutureStub.odstraniZadolzenega(sestanekZadolzeniZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(sestanekId, Integer.valueOf(sestanekOdgovor.getSestanekId()));
        Assert.assertEquals(osebaId, Integer.valueOf(sestanekOdgovor.getOsebaId()));
        Assert.assertEquals("", sestanekOdgovor.getNapaka());
        Assert.assertEquals("", sestanekOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void odstraniZadolzenega_napaka() {

        // priprava testnih poatkov
        Integer sestanekId = 5;
        Integer osebaId = 10;

        Exception exception = new ZadolzeniZaSestanekNeObstajaException("Napaka");

        // mock metod
        Mockito
                .when(zadolzeniZaSestanekService.odstrani(sestanekId, osebaId))
                .thenThrow(exception);

        // klic testirane metode
        SestanekZadolzeniZahteva sestanekZadolzeniZahteva = SestanekZadolzeniZahteva.newBuilder()
                .setSestanekId(sestanekId)
                .setOsebaId(osebaId)
                .build();
        SestanekZadolzeniOdgovor sestanekOdgovor =
                zadolzeniServiceFutureStub.odstraniZadolzenega(sestanekZadolzeniZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, sestanekOdgovor.getSestanekId());
        Assert.assertEquals(0, sestanekOdgovor.getOsebaId());
        Assert.assertEquals(exception.getClass().getSimpleName(), sestanekOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), sestanekOdgovor.getNapakaSporocilo());
    }
}
