package com.jrovtar.services.upravljalecdejavnostisestanek;


import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.SestanekUtils;
import org.junit.Assert;
import org.junit.Test;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

public class SestanekUtilsTest {

    @Test
    public void kreirajLocalDate_ok() {

        LocalDate ld1 = LocalDate.of(2020, 1, 1);
        LocalDate ld2 = SestanekUtils.kreirajLocalDate(SestanekUtils.formatiraj(ld1));

        Assert.assertEquals(ld1, ld2);
    }

    @Test
    public void formatiraj_null_date() {

        String str = SestanekUtils.formatiraj((LocalDate) null);
        Assert.assertEquals("", str);
    }

    @Test
    public void formatiraj_null_time() {

        String str = SestanekUtils.formatiraj((LocalTime) null);
        Assert.assertEquals("", str);
    }

    @Test
    public void formatiraj_ok() {

        String str1 = "01.01.2020";
        String str2 = SestanekUtils.formatiraj(SestanekUtils.kreirajLocalDate(str1));

        Assert.assertEquals(str1, str2);
    }

    @Test(expected = DateTimeException.class)
    public void kreirajLocalDate_nepravilenFormat() {

        SestanekUtils.kreirajLocalDate("01012020");
    }

    @Test(expected = IllegalArgumentException.class)
    public void kreirajLocalDate_prazenString() {

        SestanekUtils.kreirajLocalDate("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void kreirajLocalDate_null() {

        SestanekUtils.kreirajLocalDate(null);
    }
}
