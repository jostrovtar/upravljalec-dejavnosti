package com.jrovtar.services.upravljalecdejavnostisestanek;

import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.*;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanek;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekPk;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekRepository;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@RunWith(SpringRunner.class)
public class SestanekServiceImplTests {

    @MockBean
    private SestanekRepository sestanekRepository;

    @MockBean
    private ZadolzeniZaSestanekRepository zadolzeniZaSestanekRepository;

    private SestanekService sestanekService;

    @Before
    public void init() {
        sestanekService = new SestanekServiceImpl(
                sestanekRepository,
                zadolzeniZaSestanekRepository
        );
    }

    @SneakyThrows
    @Test
    public void dodaj_ok() {

        // priprava testnih podatkov
        SestanekDTO dto = new SestanekDTO();
        dto.setNaziv("Naziv");
        dto.setDatum(LocalDate.now());
        dto.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.HOURS));
        dto.setUraZakljucka(LocalTime.now());

        // mock metod
        Mockito
                .when(sestanekRepository.save(ArgumentMatchers.any(Sestanek.class)))
                .thenAnswer((Answer<Sestanek>) invocation -> {
                            Sestanek s = invocation.getArgument(0);
                            s.setId(new Random().nextInt(10));
                            return s;
                        }
                );

        // klic testirane metode
        Sestanek sestanek = sestanekService.dodaj(dto);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).save(ArgumentMatchers.any(Sestanek.class));
        Assert.assertNotNull(sestanek.getId());
        Assert.assertEquals(dto.getNaziv(), sestanek.getNaziv());
        Assert.assertEquals(dto.getDatum(), sestanek.getDatum());
        Assert.assertEquals(dto.getUraZacetka(), sestanek.getUraZacetka());
        Assert.assertEquals(dto.getUraZakljucka(), sestanek.getUraZakljucka());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_null() {

        sestanekService.dodaj(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_brezAnotacijeValidated() {

        Method method = SestanekServiceImpl.class.getDeclaredMethod("dodaj", SestanekDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(SestanekDTO.ObDodajanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void vrni_ok_vrniZadolzene() {

        // priprava testnih podatkov
        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.HOURS));
        sestanek.setUraZakljucka(LocalTime.now());

        ZadolzeniZaSestanek zzs1 = new ZadolzeniZaSestanek();
        zzs1.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs1.getZadolzeniZaSestanekPk().setSestanekId(sestanek.getId());
        zzs1.getZadolzeniZaSestanekPk().setOsebaId(2);

        ZadolzeniZaSestanek zzs2 = new ZadolzeniZaSestanek();
        zzs2.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs2.getZadolzeniZaSestanekPk().setSestanekId(sestanek.getId());
        zzs2.getZadolzeniZaSestanekPk().setOsebaId(1);

        List<ZadolzeniZaSestanek> zzsList = List.of(zzs1, zzs2);

        // mock metod
        Mockito.when(sestanekRepository.findById(sestanek.getId()))
                .thenReturn(java.util.Optional.of(sestanek));

        Mockito.when(zadolzeniZaSestanekRepository.findBySestanekId(sestanek.getId()))
                .thenReturn(zzsList);

        // klic testirane metode
        Sestanek s = sestanekService.vrni(sestanek.getId(), true);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).findById(ArgumentMatchers.anyInt());
        Assert.assertEquals(sestanek.getId(), s.getId());
        Assert.assertEquals(sestanek.getNaziv(), s.getNaziv());
        Assert.assertEquals(sestanek.getDatum(), s.getDatum());
        Assert.assertEquals(sestanek.getUraZacetka(), s.getUraZacetka());
        Assert.assertEquals(sestanek.getUraZakljucka(), s.getUraZakljucka());
        Assert.assertNotNull(s.getZadolzeniZaSestanekList());
        Assert.assertEquals(zzsList.size(), s.getZadolzeniZaSestanekList().size());

        for(int i = 0; i < zzsList.size(); i++){
            Assert.assertEquals(
                    zzsList.get(i).getZadolzeniZaSestanekPk().getOsebaId(),
                    s.getZadolzeniZaSestanekList().get(i).getZadolzeniZaSestanekPk().getOsebaId());

            Assert.assertEquals(
                    zzsList.get(i).getZadolzeniZaSestanekPk().getSestanekId(),
                    s.getZadolzeniZaSestanekList().get(i).getZadolzeniZaSestanekPk().getSestanekId());
        }
    }

    @SneakyThrows
    @Test
    public void vrni_ok_neVrniZadolzene() {

        // priprava testnih podatkov
        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Sestanek");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.HOURS));
        sestanek.setUraZakljucka(LocalTime.now());

        ZadolzeniZaSestanek zzs1 = new ZadolzeniZaSestanek();
        zzs1.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs1.getZadolzeniZaSestanekPk().setSestanekId(sestanek.getId());
        zzs1.getZadolzeniZaSestanekPk().setOsebaId(2);

        List<ZadolzeniZaSestanek> zzsList = List.of(zzs1);

        // mock metod
        Mockito.when(sestanekRepository.findById(sestanek.getId()))
                .thenReturn(java.util.Optional.of(sestanek));

        Mockito.when(zadolzeniZaSestanekRepository.findBySestanekId(sestanek.getId()))
                .thenReturn(zzsList);

        // klic testirane metode
        Sestanek s = sestanekService.vrni(sestanek.getId(), false);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).findById(ArgumentMatchers.anyInt());
        Assert.assertEquals(sestanek.getId(), s.getId());
        Assert.assertEquals(sestanek.getNaziv(), s.getNaziv());
        Assert.assertEquals(sestanek.getDatum(), s.getDatum());
        Assert.assertEquals(sestanek.getUraZacetka(), s.getUraZacetka());
        Assert.assertEquals(sestanek.getUraZakljucka(), s.getUraZakljucka());
        Assert.assertNull(s.getZadolzeniZaSestanekList());
    }

    @SneakyThrows
    @Test(expected = SestanekNeObstajaException.class)
    public void vrni_neObstaja() {

        sestanekService.vrni(0, false);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void vrni_idNull() {

        sestanekService.vrni(null, false);
    }

    @SneakyThrows
    @Test
    public void posodobi_ok() {

        // priprava testnih podatkov
        SestanekDTO dto = new SestanekDTO();
        dto.setId(1);
        dto.setNaziv("Naziv");
        dto.setDatum(LocalDate.now());
        dto.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.HOURS));
        dto.setUraZakljucka(LocalTime.now());

        // mock metod
        Mockito
                .when(sestanekRepository.save(ArgumentMatchers.any(Sestanek.class)))
                .thenAnswer((Answer<Sestanek>) invocation -> invocation.getArgument(0));

        Mockito
                .when(sestanekRepository.findById(dto.getId()))
                .thenAnswer((Answer<Optional<Sestanek>>) invocation -> {
                    Sestanek s = new Sestanek();
                    s.setId(invocation.getArgument(0));
                    return Optional.of(s);
                });

        // klic testirane metode
        Sestanek s = sestanekService.posodobi(dto);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).save(ArgumentMatchers.any(Sestanek.class));
        Assert.assertEquals(dto.getId(), s.getId());
        Assert.assertEquals(dto.getNaziv(), s.getNaziv());
        Assert.assertEquals(dto.getDatum(), s.getDatum());
        Assert.assertEquals(dto.getUraZacetka(), s.getUraZacetka());
        Assert.assertEquals(dto.getUraZakljucka(), s.getUraZakljucka());
    }

    @SneakyThrows
    @Test(expected = SestanekNeObstajaException.class)
    public void posodobi_sestanekNeObstaja() {

        // priprava testnih podatkov
        SestanekDTO dto = new SestanekDTO();
        dto.setId(1);
        dto.setNaziv("Naziv");
        dto.setDatum(LocalDate.now());
        dto.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.HOURS));
        dto.setUraZakljucka(LocalTime.now());

        // klic testirane metode
        sestanekService.posodobi(dto);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void posodobi_sestanekNull() {

        sestanekService.posodobi(null);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeValidated() {

        Method method = SestanekServiceImpl.class.getDeclaredMethod("posodobi", SestanekDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(SestanekDTO.ObPosodabljanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void izbrisi_ok() {

        // priprava testnih podatkov
        Sestanek sestanek = new Sestanek();
        sestanek.setId(1);
        sestanek.setNaziv("Naziv");
        sestanek.setDatum(LocalDate.now());
        sestanek.setUraZacetka(LocalTime.now().minus(1, ChronoUnit.HOURS));
        sestanek.setUraZakljucka(LocalTime.now());

        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(new ZadolzeniZaSestanekPk());
        zzs.getZadolzeniZaSestanekPk().setSestanekId(sestanek.getId());
        zzs.getZadolzeniZaSestanekPk().setOsebaId(1);

        List<ZadolzeniZaSestanek> zzsList = List.of(zzs);

        // mock metod
        Mockito
                .when(sestanekRepository.findById(sestanek.getId()))
                .thenAnswer((Answer<Optional<Sestanek>>) invocation -> Optional.of(sestanek));

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(sestanek.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzsList);

        // klic testirane metode
        Sestanek s = sestanekService.izbrisi(sestanek.getId());

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).delete (ArgumentMatchers.any(Sestanek.class));
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(zzsList.size())).deleteById(ArgumentMatchers.any(ZadolzeniZaSestanekPk.class));
        Assert.assertEquals(sestanek.getId(), s.getId());
        Assert.assertEquals(sestanek.getNaziv(), s.getNaziv());
        Assert.assertEquals(sestanek.getDatum(), s.getDatum());
        Assert.assertEquals(sestanek.getUraZacetka(), s.getUraZacetka());
        Assert.assertEquals(sestanek.getUraZakljucka(), s.getUraZakljucka());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void izbrisi_null() {

        sestanekService.izbrisi(null);
    }

    @SneakyThrows
    @Test(expected = SestanekNeObstajaException.class)
    public void izbrisi_sestanekNeObstaja() {

        sestanekService.izbrisi(1);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeTransactional() {

        Method method = SestanekServiceImpl.class.getDeclaredMethod("izbrisi", Integer.class);
        Transactional annotation = method.getAnnotation(Transactional.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(Propagation.REQUIRED,  annotation.propagation());
    }

    @Test
    public void filtriraj_ok_filtriNull_preteklost(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().minus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        List<ZadolzeniZaSestanek> zzs = List.of(new ZadolzeniZaSestanek(), new ZadolzeniZaSestanek());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0 * zzs.size());
        fs.setOpravljeneUre(fs.getDodeljeneUre());

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(s.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzs);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(s.getId());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(ArgumentMatchers.anyInt());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findById(ArgumentMatchers.any());
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(fs.getNaziv(), list.get(0).getNaziv());
        Assert.assertEquals(fs.getDodeljeneUre(), list.get(0).getDodeljeneUre());
        Assert.assertEquals(fs.getOpravljeneUre(), list.get(0).getOpravljeneUre());
    }

    @Test
    public void filtriraj_ok_filtriNull_prihodnost(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        List<ZadolzeniZaSestanek> zzs = List.of(new ZadolzeniZaSestanek(), new ZadolzeniZaSestanek());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0 * zzs.size());
        fs.setOpravljeneUre(0.0);

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(s.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzs);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(s.getId());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(ArgumentMatchers.anyInt());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findById(ArgumentMatchers.any());
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(fs.getNaziv(), list.get(0).getNaziv());
        Assert.assertEquals(fs.getDodeljeneUre(), list.get(0).getDodeljeneUre());
        Assert.assertEquals(fs.getOpravljeneUre(), list.get(0).getOpravljeneUre());
    }

    @Test
    public void filtriraj_ok_filterOsebaNotNull_osebaObstaja(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = 1;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        ZadolzeniZaSestanekPk zzsPk = new ZadolzeniZaSestanekPk();
        zzsPk.setOsebaId(zadolzenaOsebaIdFilter);
        zzsPk.setSestanekId(s.getId());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0);
        fs.setOpravljeneUre(0.0);

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findById(zzsPk))
                .thenAnswer((Answer<Optional<ZadolzeniZaSestanek>>) invocation -> Optional.of(new ZadolzeniZaSestanek()));

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findById(zzsPk);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findById(ArgumentMatchers.any());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findBySestanekId(ArgumentMatchers.any());
        Assert.assertEquals(1, list.size());
        Assert.assertEquals(fs.getNaziv(), list.get(0).getNaziv());
        Assert.assertEquals(fs.getDodeljeneUre(), list.get(0).getDodeljeneUre());
        Assert.assertEquals(fs.getOpravljeneUre(), list.get(0).getOpravljeneUre());
    }

    @Test
    public void filtriraj_ok_filterOsebaNotNull_osebaNeObstaja(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = 1;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().plus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        ZadolzeniZaSestanekPk zzsPk = new ZadolzeniZaSestanekPk();
        zzsPk.setOsebaId(zadolzenaOsebaIdFilter);
        zzsPk.setSestanekId(s.getId());

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findById(zzsPk);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findById(ArgumentMatchers.any());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findBySestanekId(ArgumentMatchers.any());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void filtriraj_ok_filterMinDodeljeneUre(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = 3.5;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().minus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        List<ZadolzeniZaSestanek> zzs = List.of(new ZadolzeniZaSestanek(), new ZadolzeniZaSestanek());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0 * zzs.size());
        fs.setOpravljeneUre(fs.getDodeljeneUre());

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(s.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzs);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(s.getId());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(ArgumentMatchers.anyInt());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findById(ArgumentMatchers.any());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void filtriraj_ok_filterMaxDodeljeneUre(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = 2.5;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().minus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        List<ZadolzeniZaSestanek> zzs = List.of(new ZadolzeniZaSestanek(), new ZadolzeniZaSestanek());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0 * zzs.size());
        fs.setOpravljeneUre(fs.getDodeljeneUre());

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(s.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzs);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(s.getId());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(ArgumentMatchers.anyInt());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findById(ArgumentMatchers.any());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void filtriraj_ok_filterMinOpravljeneUre(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = 3.5;
        Double maxOpravljeneUre = null;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().minus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        List<ZadolzeniZaSestanek> zzs = List.of(new ZadolzeniZaSestanek(), new ZadolzeniZaSestanek());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0 * zzs.size());
        fs.setOpravljeneUre(fs.getDodeljeneUre());

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(s.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzs);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(s.getId());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(ArgumentMatchers.anyInt());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findById(ArgumentMatchers.any());
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void filtriraj_ok_filterMaxOpravljeneUre(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = 2.5;

        // pripravi testne podatke
        Sestanek s = new Sestanek();
        s.setId(1);
        s.setNaziv("Sestanek 1");
        s.setDatum(LocalDate.now());
        s.setUraZakljucka(LocalTime.now().minus(1, ChronoUnit.MINUTES)); // trajanje 1.5h
        s.setUraZacetka(s.getUraZakljucka().minus(90, ChronoUnit.MINUTES));

        List<Sestanek> sestanki = List.of(s);

        List<ZadolzeniZaSestanek> zzs = List.of(new ZadolzeniZaSestanek(), new ZadolzeniZaSestanek());

        FiltrirajSestanekDTO fs = new FiltrirajSestanekDTO();
        fs.setNaziv(s.getNaziv());
        fs.setDodeljeneUre(
                Duration.between(s.getUraZacetka(), s.getUraZakljucka()).getSeconds() / 3600.0 * zzs.size());
        fs.setOpravljeneUre(fs.getDodeljeneUre());

        // mock metod
        Mockito
                .when(sestanekRepository.filtrirajPoNazivu(nazivFilter))
                .thenAnswer((Answer<List<Sestanek>>) invocation -> sestanki);

        Mockito
                .when(zadolzeniZaSestanekRepository.findBySestanekId(s.getId()))
                .thenAnswer((Answer<List<ZadolzeniZaSestanek>>) invocation -> zzs);

        // klic testirane metode
        List<FiltrirajSestanekDTO> list =
                sestanekService.filtriraj(
                        nazivFilter,
                        zadolzenaOsebaIdFilter,
                        minDodeljeneUre,
                        maxDodeljeneUre,
                        minOpravljeneUre,
                        maxOpravljeneUre);

        // preverjanje rezultatov
        Mockito.verify(sestanekRepository, Mockito.times(1)).filtrirajPoNazivu(nazivFilter);
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(s.getId());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(1)).findBySestanekId(ArgumentMatchers.anyInt());
        Mockito.verify(zadolzeniZaSestanekRepository, Mockito.times(0)).findById(ArgumentMatchers.any());
        Assert.assertEquals(0, list.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_ok_napakaDodeljeneUre(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = 2.0;
        Double maxDodeljeneUre = 1.0;
        Double minOpravljeneUre = null;
        Double maxOpravljeneUre = null;

        // klic testirane metode
        sestanekService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filtriraj_ok_napakaOpravljeneUre(){

        // pripravi filtre
        String nazivFilter = null;
        Integer zadolzenaOsebaIdFilter = null;
        Double minDodeljeneUre = null;
        Double maxDodeljeneUre = null;
        Double minOpravljeneUre = 2.0;
        Double maxOpravljeneUre = 1.0;

        // klic testirane metode
        sestanekService.filtriraj(
                nazivFilter,
                zadolzenaOsebaIdFilter,
                minDodeljeneUre,
                maxDodeljeneUre,
                minOpravljeneUre,
                maxOpravljeneUre);
    }

}
