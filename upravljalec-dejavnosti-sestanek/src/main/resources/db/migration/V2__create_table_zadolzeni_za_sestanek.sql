CREATE TABLE IF NOT EXISTS zadolzeni_za_sestanek (
  sestanek_id INT NOT NULL,
  oseba_id INT NOT NULL,
  PRIMARY KEY (sestanek_id, oseba_id),
  CONSTRAINT fk_zadolzeni_za_sestanek_sestanek1
    FOREIGN KEY (sestanek_id)
    REFERENCES sestanek (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE = InnoDB