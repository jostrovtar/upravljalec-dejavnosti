package com.jrovtar.services.upravljalecdejavnostisestanek.napake;

public class ZadolzeniZaSestanekNeObstajaException extends Exception {

    public ZadolzeniZaSestanekNeObstajaException(String message) {
        super(message);
    }
}
