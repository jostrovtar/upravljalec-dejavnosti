package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class SestanekUtils {

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

    private SestanekUtils() {
    }


    /**
     * Kreiraj LocalDate iz stringa. Zahtevan format dd.MM.yyyy .
     *
     * @param strDatum formatiran datum
     * @return LocalDate
     * @throws IllegalArgumentException če je strDatum null ali prazen string
     * @throws DateTimeParseException   če strDatum ni pravilno formatiran
     */
    public static LocalDate kreirajLocalDate(String strDatum) {

        Assert.hasText(strDatum, "Podan string je null ali prazen.");

        return LocalDate.parse(strDatum, dateFormatter);
    }

    /**
     * Kreiraj String iz LocalDate. Format: dd.MM.yyyy .
     *
     * @param datum datum
     * @return formatiran datum
     */
    public static String formatiraj(LocalDate datum) {

        if (datum == null) {
            return "";
        }

        return datum.format(dateFormatter);
    }

    /**
     * Kreiraj LocalTime iz stringa. Zahtevan format HH:mm.
     *
     * @param strCas formatiran čas
     * @return LocalTime
     * @throws IllegalArgumentException če je strCas null ali prazen string.
     * @throws DateTimeParseException   če strCas ni pravilno formatiran.
     */
    public static LocalTime kreirajLocalTime(String strCas) {

        Assert.hasText(strCas, "Podan string je null ali prazen.");

        return LocalTime.parse(strCas, timeFormatter);
    }

    /**
     * Kreiraj String iz LocalTime. Format: HH:mm .
     *
     * @param ura ura
     * @return formatirana ura
     */
    public static String formatiraj(LocalTime ura) {

        if (ura == null) {
            return "";
        }

        return ura.format(timeFormatter);
    }
}
