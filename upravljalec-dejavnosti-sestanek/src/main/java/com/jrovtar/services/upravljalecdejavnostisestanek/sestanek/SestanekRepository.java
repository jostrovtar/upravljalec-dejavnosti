package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SestanekRepository extends CrudRepository<Sestanek, Integer> {

    @Query(value = "SELECT s FROM Sestanek s WHERE (:filterNaziv IS NULL OR s.naziv LIKE %:filterNaziv%)")
    List<Sestanek> filtrirajPoNazivu(@Param("filterNaziv") String filterNaziv);
}
