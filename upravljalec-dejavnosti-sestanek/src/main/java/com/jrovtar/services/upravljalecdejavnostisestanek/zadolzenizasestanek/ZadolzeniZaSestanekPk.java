package com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class ZadolzeniZaSestanekPk implements Serializable {

    @Column(name = "sestanek_id")
    private Integer sestanekId;

    @Column(name = "oseba_id")
    private Integer osebaId;
}
