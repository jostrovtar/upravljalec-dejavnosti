package com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class ZadolzeniZaSestanek {

    @EmbeddedId
    private ZadolzeniZaSestanekPk zadolzeniZaSestanekPk;

    @Column(name = "sestanek_id", insertable = false, updatable = false)
    private Integer sestanekId;

}
