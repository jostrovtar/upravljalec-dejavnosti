package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanek;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekPk;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Validated
public class SestanekServiceImpl implements SestanekService {

    public static final String MESSAGE_SESTANEK_NE_OBSTAJA = "Sestanek z id '%d' ne obstaja.";
    public static final String MESSAGE_SESTANEK_NE_SME_BITI_NULL = "Sestanek ne sme biti null.";
    public static final String MESSAGE_ID_SESTANKA_NI_PODAN = "Id sestanka mora biti podan.";

    private SestanekRepository sestanekRepository;
    private ZadolzeniZaSestanekRepository zadolzeniZaSestanekRepository;

    public SestanekServiceImpl(
            SestanekRepository sestanekRepository,
            ZadolzeniZaSestanekRepository zadolzeniZaSestanekRepository) {
        this.sestanekRepository = sestanekRepository;
        this.zadolzeniZaSestanekRepository = zadolzeniZaSestanekRepository;
    }

    /**
     * Dodaj sestanek.
     *
     * @param dto sestanek
     * @return dodan sestanek
     */
    @Validated(SestanekDTO.ObDodajanju.class)
    public Sestanek dodaj(@Valid SestanekDTO dto) {

        Assert.notNull(dto, MESSAGE_SESTANEK_NE_SME_BITI_NULL);

        log.debug("Dodajanje sestanka: {}", dto);

        Sestanek s = new Sestanek();
        s.setNaziv(dto.getNaziv());
        s.setDatum(dto.getDatum());
        s.setUraZacetka(dto.getUraZacetka());
        s.setUraZakljucka(dto.getUraZakljucka());

        s = sestanekRepository.save(s);

        return s;
    }

    /**
     * Vrni sestanek s podanim id.
     *
     * @param id            id sestanka
     * @param vrniZadolzene vrni tudi zadolzene za sestanek
     * @return sestanek in zadolžene osebe
     * @throws SestanekNeObstajaException če sestanek s podanim id-jem ne obstaja.
     */
    public Sestanek vrni(Integer id, boolean vrniZadolzene) throws SestanekNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_SESTANKA_NI_PODAN);

        // vrni sestanek
        Optional<Sestanek> sestanekOptional = sestanekRepository.findById(id);

        if (!sestanekOptional.isPresent()) {
            throw new SestanekNeObstajaException(
                    String.format(MESSAGE_SESTANEK_NE_OBSTAJA, id));
        }

        Sestanek s = sestanekOptional.get();

        // vrni zadolzene za sestanek
        if (vrniZadolzene) {
            s.setZadolzeniZaSestanekList(
                    zadolzeniZaSestanekRepository.findBySestanekId(s.getId()));
        }

        return s;
    }

    /**
     * Posodobi sestanek.
     *
     * @param dto sestanek
     * @return posodobljen sestanek
     * @throws SestanekNeObstajaException če sestanek s podanim id-jem ne obstaja.
     */
    @Validated(SestanekDTO.ObPosodabljanju.class)
    public Sestanek posodobi(@Valid SestanekDTO dto) throws SestanekNeObstajaException {

        Assert.notNull(dto, MESSAGE_SESTANEK_NE_SME_BITI_NULL);

        log.debug("Posodabljanje sestanka: {}", dto);

        Sestanek s = vrni(dto.getId(), false);
        s.setNaziv(dto.getNaziv());
        s.setDatum(dto.getDatum());
        s.setUraZacetka(dto.getUraZacetka());
        s.setUraZakljucka(dto.getUraZakljucka());

        s = sestanekRepository.save(s);

        return s;
    }


    /**
     * Izbriši sestanek.
     *
     * @param id id sestanka
     * @return izbrisan sestanek
     * @throws SestanekNeObstajaException če sestanek s podanim id-jem ne obstaja.
     */
    @Transactional
    public Sestanek izbrisi(Integer id) throws SestanekNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_SESTANKA_NI_PODAN);

        // vrni sestanek
        Sestanek s = vrni(id, true);

        // pobrisi zadolzene za sestanek
        for (ZadolzeniZaSestanek zzs : s.getZadolzeniZaSestanekList()) {
            zadolzeniZaSestanekRepository.deleteById(zzs.getZadolzeniZaSestanekPk());
        }

        // izbrisi sestanek
        sestanekRepository.delete(s);

        return s;
    }

    /**
     * Filtriraj sestanke po nazivu, zadolženi osebi, dodeljenih urah in opravljenih urah. Filter se ne upošteva, če je
     * vrednost null ali negativna (prazen string v primeru naziva).
     * <p>
     * Dodeljene ure se izračunajo po formuli: trajanje sestanka v urah * št. zadolženih oseb za sestanek
     * <p>
     * Če je sestanek končan so opravljene ure so enake dodeljenim, oziroma 0 če sestanek ni končan.
     *
     * @param nazivFilter            filter po nazivu mejnika
     * @param zadolzenaOsebaIdFilter filter po zadolzeni osebi.
     * @param minDodeljeneUre        min število dodeljenih ur, ne sme biti večje kot maxDodeljeneUre
     * @param maxDodeljeneUre        max število dodeljenih ur
     * @param minOpravljeneUre       min število opravljenih ur, ne sme biti večje kot maxOpravljeneUre
     * @param maxOpravljeneUre       max število opravljenih ur
     * @return seznam filtriranih sestankov
     */
    public List<FiltrirajSestanekDTO> filtriraj(
            String nazivFilter, Integer zadolzenaOsebaIdFilter, Double minDodeljeneUre,
            Double maxDodeljeneUre, Double minOpravljeneUre, Double maxOpravljeneUre) {

        // prilagodi filtre
        String fNaziv = "".equals(nazivFilter) ? null : nazivFilter;

        if (minDodeljeneUre != null && minDodeljeneUre < 0.0) {
            minDodeljeneUre = null;
        }

        if (maxDodeljeneUre != null && maxDodeljeneUre < 0.0) {
            maxDodeljeneUre = null;
        }

        if (minOpravljeneUre != null && minOpravljeneUre < 0) {
            minOpravljeneUre = null;
        }

        if (maxOpravljeneUre != null && maxOpravljeneUre < 0) {
            maxOpravljeneUre = null;
        }

        if (minDodeljeneUre != null && maxDodeljeneUre != null &&
                minDodeljeneUre > maxDodeljeneUre) {
            throw new IllegalArgumentException(
                    "Minimalno število dodeljenih ur ne sme biti večje od maximalnega števila dodeljenih ur.");
        }

        if (minOpravljeneUre != null && maxOpravljeneUre != null &&
                minOpravljeneUre > maxOpravljeneUre) {
            throw new IllegalArgumentException(
                    "Minimalno število opravljenih ur ne sme biti večje od maximalnega števila opravljenih ur.");
        }

        // vrni sestanke - filtriraj po nazivu
        List<Sestanek> sestanki = sestanekRepository.filtrirajPoNazivu(fNaziv);

        List<ZadolzeniZaSestanek> zadolzeniZaSestanekList;
        List<FiltrirajSestanekDTO> filtriraniSestanki = new ArrayList<>();

        for (Sestanek sestanek : sestanki) {

            // vrni zadolžene za sestanek
            if (zadolzenaOsebaIdFilter == null || zadolzenaOsebaIdFilter < 0) {
                zadolzeniZaSestanekList = zadolzeniZaSestanekRepository.findBySestanekId(sestanek.getId());

            } else {
                // kreiraj primarni ključ
                ZadolzeniZaSestanekPk pk = new ZadolzeniZaSestanekPk();
                pk.setOsebaId(zadolzenaOsebaIdFilter);
                pk.setSestanekId(sestanek.getId());

                Optional<ZadolzeniZaSestanek> opt = zadolzeniZaSestanekRepository.findById(pk);

                // če oseba ni zadolžena za ta sestanek ga izpustimo.
                if (!opt.isPresent()) {
                    continue;
                }

                zadolzeniZaSestanekList = List.of(opt.get());
            }

            // izračun trajana sestanka v urah
            Double trajanjeSestanka =
                    Duration.between(sestanek.getUraZacetka(), sestanek.getUraZakljucka()).getSeconds() / 3600.0;

            // izračun dodeljenih ur
            Double dodeljeneUre = trajanjeSestanka * zadolzeniZaSestanekList.size();

            // preveri filter min dodeljene ure
            if (minDodeljeneUre != null && dodeljeneUre < minDodeljeneUre) {
                continue;
            }

            // preveri filter max dodeljene ure
            if (maxDodeljeneUre != null && dodeljeneUre > maxDodeljeneUre) {
                continue;
            }

            // sestavi uro in datum zaključka sestanka
            LocalDateTime zakljucekSestanka = LocalDateTime.of(sestanek.getDatum(), sestanek.getUraZakljucka());

            // izračun opravljenih ur
            Double opravljeneUre = LocalDateTime.now().isBefore(zakljucekSestanka) ? 0.0 : dodeljeneUre;

            // preveri filter min opravljene ure
            if (minOpravljeneUre != null && opravljeneUre < minOpravljeneUre) {
                continue;
            }

            // preveri filter max opravljene ure
            if (maxOpravljeneUre != null && opravljeneUre > maxOpravljeneUre) {
                continue;
            }

            // sestavi dto
            FiltrirajSestanekDTO filtrirajSestanekDTO = new FiltrirajSestanekDTO();
            filtrirajSestanekDTO.setNaziv(sestanek.getNaziv());
            filtrirajSestanekDTO.setDodeljeneUre(dodeljeneUre);
            filtrirajSestanekDTO.setOpravljeneUre(opravljeneUre);
            filtriraniSestanki.add(filtrirajSestanekDTO);
        }

        return filtriraniSestanki;
    }
}
