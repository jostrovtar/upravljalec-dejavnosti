package com.jrovtar.services.upravljalecdejavnostisestanek.grpc;

import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.*;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import javax.validation.ConstraintViolationException;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@GRpcService
public class SestanekGrpcServiceImpl extends SestanekServiceGrpc.SestanekServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju sestanka. ";
    public static final String VRNI_NAPAKA_SPOROCILO = "Napaka pri vnačanju sestanka. ";
    public static final String POSODOBI_NAPAKA_SPOROCILO = "Napaka pri posodabljanju sestanka. ";
    public static final String BRISI_NAPAKA_SPOROCILO = "Napaka pri brisanju sestanka. ";
    public static final String FILTRITAJ_NAPAKA_SPOROCILO = "Napaka pri filtriranju sestankov. ";

    private SestanekService sestanekService;

    public SestanekGrpcServiceImpl(SestanekService sestanekService) {
        this.sestanekService = sestanekService;
    }


    @Override
    public void dodaj(SestanekDodajZahteva request, StreamObserver<SestanekOdgovor> responseObserver) {

        SestanekOdgovor sestanekOdgovor;

        try {
            Sestanek s = sestanekService.dodaj(SestanekDTO.kreirajDTO(request));
            sestanekOdgovor = SestanekOdgovor.newBuilder()
                    .setId(s.getId())
                    .setNaziv(s.getNaziv())
                    .setDatum(SestanekUtils.formatiraj(s.getDatum()))
                    .setUraZacetka(SestanekUtils.formatiraj(s.getUraZacetka()))
                    .setUraZakljucka(SestanekUtils.formatiraj(s.getUraZakljucka()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            sestanekOdgovor = SestanekOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(sestanekOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void vrni(SestanekVrniZahteva request, StreamObserver<SestanekVrniOdgovor> responseObserver) {

        SestanekVrniOdgovor odgovor;

        try {
            Sestanek s = sestanekService.vrni(request.getId(), true);
            odgovor = SestanekVrniOdgovor.newBuilder()
                    .setId(s.getId())
                    .setNaziv(s.getNaziv())
                    .setDatum(SestanekUtils.formatiraj(s.getDatum()))
                    .setUraZacetka(SestanekUtils.formatiraj(s.getUraZacetka()))
                    .setUraZakljucka(SestanekUtils.formatiraj(s.getUraZakljucka()))
                    .addAllOsebe(
                            s.getZadolzeniZaSestanekList().stream().map(
                                    e -> SestanekVrniOseba.newBuilder()
                                            .setOsebaId(e.getZadolzeniZaSestanekPk().getOsebaId())
                                            .build()
                            ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof SestanekNeObstajaException) {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            odgovor = SestanekVrniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void izbrisi(SestanekIzbrisiZahteva request, StreamObserver<SestanekOdgovor> responseObserver) {

        SestanekOdgovor sestanekOdgovor;

        try {
            Sestanek s = sestanekService.izbrisi(request.getId());
            sestanekOdgovor = SestanekOdgovor.newBuilder()
                    .setId(s.getId())
                    .setNaziv(s.getNaziv())
                    .setDatum(SestanekUtils.formatiraj(s.getDatum()))
                    .setUraZacetka(SestanekUtils.formatiraj(s.getUraZacetka()))
                    .setUraZakljucka(SestanekUtils.formatiraj(s.getUraZakljucka()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof SestanekNeObstajaException) {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(BRISI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            sestanekOdgovor = SestanekOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(sestanekOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void posodobi(SestanekPosodobiZahteva request, StreamObserver<SestanekOdgovor> responseObserver) {

        SestanekOdgovor sestanekOdgovor;

        try {
            Sestanek s = sestanekService.posodobi(SestanekDTO.kreirajDTO(request));
            sestanekOdgovor = SestanekOdgovor.newBuilder()
                    .setId(s.getId())
                    .setNaziv(s.getNaziv())
                    .setDatum(SestanekUtils.formatiraj(s.getDatum()))
                    .setUraZacetka(SestanekUtils.formatiraj(s.getUraZacetka()))
                    .setUraZakljucka(SestanekUtils.formatiraj(s.getUraZakljucka()))
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof SestanekNeObstajaException ||
                    e instanceof ConstraintViolationException) {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            sestanekOdgovor = SestanekOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(sestanekOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void filtriraj(SestanekFiltrirajZahteva request, StreamObserver<SestanekFiltrirajOdgovor> responseObserver) {

        SestanekFiltrirajOdgovor odgovor;

        try {
            List<FiltrirajSestanekDTO> list = sestanekService.filtriraj(
                    request.getFilterNaziv(),
                    request.getFilterZadolzenaOseba(),
                    request.getMinDodeljeneUre(),
                    request.getMaxDodeljeneUre(),
                    request.getMinOpravljeneUre(),
                    request.getMaxOpravljeneUre()
            );
            odgovor = SestanekFiltrirajOdgovor.newBuilder()
                    .addAllItems(
                            list.stream().map(
                                    e -> SestanekFiltrirajItem.newBuilder()
                                            .setNaziv(e.getNaziv())
                                            .setDodeljeneUre(e.getDodeljeneUre())
                                            .setOpravljeneUre(e.getOpravljeneUre())
                                            .build()
                            ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {
            log.error(FILTRITAJ_NAPAKA_SPOROCILO + e.getMessage(), e);

            odgovor = SestanekFiltrirajOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();
    }
}
