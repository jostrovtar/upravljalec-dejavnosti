package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanek;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@Data
public class Sestanek {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String naziv;
    private LocalDate datum;
    private LocalTime uraZacetka;
    private LocalTime uraZakljucka;

    @Transient
    private List<ZadolzeniZaSestanek> zadolzeniZaSestanekList;

}
