package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

public interface SestanekService {

    /**
     * Dodaj sestanek.
     *
     * @param dto sestanek
     * @return dodan sestanek
     */
    @Validated(SestanekDTO.ObDodajanju.class)
    Sestanek dodaj(@Valid SestanekDTO dto);

    /**
     * Vrni sestanek s podanim id. Vrnjeni bodo tudi vsi zadolženi za sestanek.
     *
     * @param id            id sestanka
     * @param vrniZadolzene vrni tudi zadolzene za sestanek
     * @return sestanek in zadolžene osebe
     * @throws SestanekNeObstajaException če sestanek z podanim id-jem ne obstaja.
     */
    Sestanek vrni(Integer id, boolean vrniZadolzene) throws SestanekNeObstajaException;

    /**
     * Posodobi sestanek.
     *
     * @param dto sestanek
     * @return posodobljen sestanek
     * @throws SestanekNeObstajaException če sestanek z podanim id-jem ne obstaja.
     */
    @Validated(SestanekDTO.ObPosodabljanju.class)
    Sestanek posodobi(@Valid SestanekDTO dto) throws SestanekNeObstajaException;


    /**
     * Izbrisi sestanek.
     *
     * @param id id sestanka
     * @return izbrisan sestanek
     * @throws SestanekNeObstajaException če sestanek z podanim id-jem ne obstaja.
     */
    @Transactional
    Sestanek izbrisi(Integer id) throws SestanekNeObstajaException;

    /**
     * Filtriraj sestanke po nazivu, zadolženi osebi, dodeljenih urah in opravljenih urah. Filter se ne upošteva, če je
     * vrednost null ali negativna (prazen string v primeru naziva).
     * <p>
     * Dodeljene ure se izračunajo po formuli: trajanje sestanka v urah * št. zadolženih oseb za sestanek
     * <p>
     * Če je sestanek končan so opravljene ure so enake dodeljenim, oziroma 0 če sestanek ni končan.
     *
     * @param nazivFilter            filter po nazivu mejnika
     * @param zadolzenaOsebaIdFilter filter po zadolzeni osebi.
     * @param minDodeljeneUre        min število dodeljenih ur
     * @param maxDodeljeneUre        max število dodeljenih ur
     * @param minOpravljeneUre       min število opravljenih ur
     * @param maxOpravljeneUre       max število opravljenih ur
     * @return seznam filtriranih sestankov
     */
    List<FiltrirajSestanekDTO> filtriraj(
            String nazivFilter, Integer zadolzenaOsebaIdFilter, Double minDodeljeneUre,
            Double maxDodeljeneUre, Double minOpravljeneUre, Double maxOpravljeneUre);
}
