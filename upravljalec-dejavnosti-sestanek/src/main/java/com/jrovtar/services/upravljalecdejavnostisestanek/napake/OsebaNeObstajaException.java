package com.jrovtar.services.upravljalecdejavnostisestanek.napake;

public class OsebaNeObstajaException extends Exception {

    public OsebaNeObstajaException(String message) {
        super(message);
    }
}
