package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import lombok.Data;

@Data
public class FiltrirajSestanekDTO {

    private String naziv;
    private Double dodeljeneUre;
    private Double opravljeneUre;

}
