package com.jrovtar.services.upravljalecdejavnostisestanek.sestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekDodajZahteva;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekPosodobiZahteva;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.StringUtils;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class SestanekDTO {

    @NotNull(
            groups = {ObPosodabljanju.class},
            message = "Id je obvezen."
    )
    private Integer id;

    @NotBlank(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Naziv je obvezen.")
    @Length(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            max = 255,
            message = "Največja dovoljena dolžina naziva je {max} znakov.")
    private String naziv;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Datum je obvezen.")
    private LocalDate datum;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Ura začetka je obvezna.")
    private LocalTime uraZacetka;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Ura zaključka je obvezna.")
    private LocalTime uraZakljucka;

    /**
     * Kreiraj SestanekDTO iz SestanekDodajZahteva.
     *
     * @param sestanekDodajZahteva instanca tipa SestanekDodajZahteva
     * @return instanca tipa SestanekDTO
     */
    public static SestanekDTO kreirajDTO(SestanekDodajZahteva sestanekDodajZahteva) {

        if (sestanekDodajZahteva == null) {
            return null;
        }

        SestanekDTO dto = new SestanekDTO();
        dto.setNaziv(sestanekDodajZahteva.getNaziv());
        dto.setDatum(
                StringUtils.isEmpty(sestanekDodajZahteva.getDatum()) ?
                        null : SestanekUtils.kreirajLocalDate(sestanekDodajZahteva.getDatum()));
        dto.setUraZacetka(
                StringUtils.isEmpty(sestanekDodajZahteva.getUraZacetka()) ?
                        null : SestanekUtils.kreirajLocalTime(sestanekDodajZahteva.getUraZacetka()));
        dto.setUraZakljucka(
                StringUtils.isEmpty(sestanekDodajZahteva.getUraZakljucka()) ?
                        null : SestanekUtils.kreirajLocalTime(sestanekDodajZahteva.getUraZakljucka()));

        return dto;
    }

    /**
     * Kreiraj SestanekDTO iz Sestanek.
     *
     * @param sestanek instanca tipa Sestanek
     * @return instanca tipa SestanekDTO
     */
    public static SestanekDTO kreirajDTO(Sestanek sestanek) {

        if (sestanek == null) {
            return null;
        }

        SestanekDTO dto = new SestanekDTO();
        dto.setNaziv(sestanek.getNaziv());
        dto.setDatum(sestanek.getDatum());
        dto.setUraZacetka(sestanek.getUraZacetka());
        dto.setUraZakljucka(sestanek.getUraZakljucka());

        return dto;
    }

    /**
     * Kreiraj SestanekDTO iz SestanekPosodobiZahteva.
     *
     * @param sestanekPosodobiZahteva instanca tipa SestanekPosodobiZahteva
     * @return instanca tipa SestanekDTO
     */
    public static SestanekDTO kreirajDTO(SestanekPosodobiZahteva sestanekPosodobiZahteva) {

        if (sestanekPosodobiZahteva == null) {
            return null;
        }

        SestanekDTO dto = new SestanekDTO();
        dto.setId(sestanekPosodobiZahteva.getId());
        dto.setNaziv(sestanekPosodobiZahteva.getNaziv());
        dto.setDatum(
                StringUtils.isEmpty(sestanekPosodobiZahteva.getDatum()) ?
                        null : SestanekUtils.kreirajLocalDate(sestanekPosodobiZahteva.getDatum()));
        dto.setUraZacetka(
                StringUtils.isEmpty(sestanekPosodobiZahteva.getUraZacetka()) ?
                        null : SestanekUtils.kreirajLocalTime(sestanekPosodobiZahteva.getUraZacetka()));
        dto.setUraZakljucka(
                StringUtils.isEmpty(sestanekPosodobiZahteva.getUraZakljucka()) ?
                        null : SestanekUtils.kreirajLocalTime(sestanekPosodobiZahteva.getUraZakljucka()));

        return dto;
    }

    @AssertTrue(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Ura začetka mora biti pred uro zaključka.")
    public boolean isUraZacetkaPredUroZakljucka() {

        if (uraZacetka != null && uraZakljucka != null &&
                !uraZacetka.isBefore(uraZakljucka)) {
            return false;
        }

        return true;
    }


    /*
     * Tipa, ki se uporabljata za določanje validacijske skupne (validation group)
     * pri validiranju SestanekDTO v metodah razdreda SestanekServiceImpl.
     */
    public interface ObDodajanju {
    }

    public interface ObPosodabljanju {
    }
}
