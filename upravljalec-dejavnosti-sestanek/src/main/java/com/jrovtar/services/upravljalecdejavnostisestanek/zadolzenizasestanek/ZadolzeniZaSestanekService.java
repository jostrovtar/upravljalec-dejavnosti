package com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek;

import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.ZadolzeniZaSestanekNeObstajaException;

public interface ZadolzeniZaSestanekService {

    /**
     * Dodaj (zadolži) osebo na sestanek.
     *
     * @param sestanekId id sestanka
     * @param osebaId    id osebe
     * @return oseba zadolžena za sestanek
     * @throws SestanekNeObstajaException če sestanek s podanim id ne obstaja.
     * @throws OsebaNeObstajaException    če oseba s podanim id ne obstaja.
     * @throws OsebaServiceException      če preverjanje osebe ni bilo uspešno.
     */
    ZadolzeniZaSestanek dodaj(Integer sestanekId, Integer osebaId)
            throws SestanekNeObstajaException, OsebaNeObstajaException, OsebaServiceException;

    /**
     * Odstrani osebo iz sestanka.
     *
     * @param sestanekId sestanek id
     * @param osebaId    oseba id
     * @return odstranjeni zadolzeni za sestanek
     * @throws ZadolzeniZaSestanekNeObstajaException če oseba ni bila zadolžena za sestanek.
     */
    ZadolzeniZaSestanek odstrani(Integer sestanekId, Integer osebaId) throws ZadolzeniZaSestanekNeObstajaException;

    /**
     * Preveri, ali oseba s podanim id obstaja.
     *
     * @param osebaId id osebe
     * @throws OsebaServiceException   če je prišlo do napake pri klicu
     * @throws OsebaNeObstajaException če oseba ne obstaja
     */
    void preveriObstojOsebe(Integer osebaId) throws OsebaServiceException, OsebaNeObstajaException;
}
