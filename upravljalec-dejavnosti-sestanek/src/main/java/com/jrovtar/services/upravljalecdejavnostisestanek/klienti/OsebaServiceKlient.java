package com.jrovtar.services.upravljalecdejavnostisestanek.klienti;

import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaServiceGrpc;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaVrniZahteva;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaServiceException;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Optional;

@Component
@Slf4j
public class OsebaServiceKlient {

    private NastavitveKlientov nastavitveKlientov;
    private ManagedChannel channel;
    private OsebaServiceGrpc.OsebaServiceFutureStub osebaServiceFutureStub = null;

    public OsebaServiceKlient(NastavitveKlientov nastavitveKlientov) {
        this.nastavitveKlientov = nastavitveKlientov;
    }

    @PostConstruct
    public void init() {
        channel = ManagedChannelBuilder.forAddress(
                nastavitveKlientov.getOsebaServiceUrl(),
                nastavitveKlientov.getOsebaServicePort())
                .usePlaintext()
                .build();

        osebaServiceFutureStub = OsebaServiceGrpc.newFutureStub(channel);
    }

    @PreDestroy
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    /**
     * Vrni osebo.
     *
     * @param id id osebe
     * @return oseba
     * @throws OsebaServiceException če klic metode osebaServiceFutureStub.vrni() ni bil uspešno izveden.
     */
    public OsebaOdgovor vrni(Integer id) throws OsebaServiceException {

        Assert.notNull(id, "Id osebe ne sme biti null.");

        OsebaVrniZahteva osebaVrniZahteva = OsebaVrniZahteva.newBuilder().setId(id).build();

        OsebaOdgovor osebaOdgovor;
        try {
            osebaOdgovor = osebaServiceFutureStub.vrni(osebaVrniZahteva).get();

        } catch (Exception e) {
            throw new OsebaServiceException("Napaka pri izvedbi metode osebaServiceFutureStub.vrni() " + e.getMessage(), e);
        }

        return osebaOdgovor;
    }

}
