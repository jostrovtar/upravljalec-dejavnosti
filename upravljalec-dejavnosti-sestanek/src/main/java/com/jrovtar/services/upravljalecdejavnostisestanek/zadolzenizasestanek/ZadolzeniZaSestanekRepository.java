package com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ZadolzeniZaSestanekRepository extends CrudRepository<ZadolzeniZaSestanek, ZadolzeniZaSestanekPk> {

    List<ZadolzeniZaSestanek> findBySestanekId(Integer sestanekId);
}
