package com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek;

import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import com.jrovtar.services.upravljalecdejavnostisestanek.klienti.OsebaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.ZadolzeniZaSestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.Sestanek;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.SestanekRepository;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.SestanekServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

@Slf4j
@Service
public class ZadolzeniZaSestanekServiceImpl implements ZadolzeniZaSestanekService {

    public static final String MESSAGE_ID_OSEBE_NI_PODAN = "Id osebe mora biti podan.";
    public static final String MESSAGE_ZADOLZENA_OSEBA_ZA_SESTANEK_NE_OBSTAJA = "Oseba z id '%d' ni bila zadolžena za sestanek z id '%d'.";

    private SestanekRepository sestanekRepository;
    private ZadolzeniZaSestanekRepository zadolzeniZaSestanekRepository;
    private OsebaServiceKlient osebaServiceKlient;

    public ZadolzeniZaSestanekServiceImpl(
            SestanekRepository sestanekRepository,
            ZadolzeniZaSestanekRepository zadolzeniZaSestanekRepository,
            OsebaServiceKlient osebaServiceKlient) {
        this.sestanekRepository = sestanekRepository;
        this.zadolzeniZaSestanekRepository = zadolzeniZaSestanekRepository;
        this.osebaServiceKlient = osebaServiceKlient;
    }

    /**
     * Dodaj (zadolži) osebo na sestanek.
     *
     * @param sestanekId id sestanka
     * @param osebaId    id osebe
     * @return oseba zadolžena za sestanek
     * @throws SestanekNeObstajaException če sestanek s podanim id ne obstaja.
     * @throws OsebaNeObstajaException    če oseba s podanim id ne obstaja.
     * @throws OsebaServiceException      če preverjanje osebe ni bilo uspešno.
     */
    public ZadolzeniZaSestanek dodaj(Integer sestanekId, Integer osebaId)
            throws SestanekNeObstajaException, OsebaNeObstajaException, OsebaServiceException {

        Assert.notNull(sestanekId, SestanekServiceImpl.MESSAGE_ID_SESTANKA_NI_PODAN);
        Assert.notNull(osebaId, MESSAGE_ID_OSEBE_NI_PODAN);

        // preverjanje sestanka
        Optional<Sestanek> sestanekOptional = sestanekRepository.findById(sestanekId);

        if (!sestanekOptional.isPresent()) {
            throw new SestanekNeObstajaException(
                    String.format(SestanekServiceImpl.MESSAGE_SESTANEK_NE_OBSTAJA, sestanekId));
        }

        // prevejanje osebe
        preveriObstojOsebe(osebaId);

        log.debug(String.format("Dodajanje osebe z id '%d' na sestanek z id '%d'", osebaId, sestanekId));

        // kreiraj primarni ključ
        ZadolzeniZaSestanekPk pk = new ZadolzeniZaSestanekPk();
        pk.setOsebaId(osebaId);
        pk.setSestanekId(sestanekId);

        // kreiraj entiteto
        ZadolzeniZaSestanek zzs = new ZadolzeniZaSestanek();
        zzs.setZadolzeniZaSestanekPk(pk);

        // shrani v bazo
        zzs = zadolzeniZaSestanekRepository.save(zzs);

        return zzs;
    }

    /**
     * Odstrani osebo iz sestanka.
     *
     * @param sestanekId sestanek id
     * @param osebaId    oseba id
     * @return odstranjeni zadolzeni za sestanek
     * @throws ZadolzeniZaSestanekNeObstajaException če oseba ni bila zadolžena za sestanek.
     */
    public ZadolzeniZaSestanek odstrani(Integer sestanekId, Integer osebaId)
            throws ZadolzeniZaSestanekNeObstajaException {

        Assert.notNull(sestanekId, SestanekServiceImpl.MESSAGE_ID_SESTANKA_NI_PODAN);
        Assert.notNull(osebaId, MESSAGE_ID_OSEBE_NI_PODAN);

        log.debug(String.format("Brisanje osebe z id '%d' na iz sestanka z id '%d'", osebaId, sestanekId));

        // kreiraj primarni ključ
        ZadolzeniZaSestanekPk pk = new ZadolzeniZaSestanekPk();
        pk.setOsebaId(osebaId);
        pk.setSestanekId(sestanekId);

        // vrni iz baze
        Optional<ZadolzeniZaSestanek> optional = zadolzeniZaSestanekRepository.findById(pk);

        if (!optional.isPresent()) {
            throw new ZadolzeniZaSestanekNeObstajaException(
                    String.format(MESSAGE_ZADOLZENA_OSEBA_ZA_SESTANEK_NE_OBSTAJA, osebaId, sestanekId));
        }

        ZadolzeniZaSestanek zzs = optional.get();

        // izbriši
        zadolzeniZaSestanekRepository.delete(zzs);

        return zzs;
    }

    /**
     * Preveri, ali oseba s podanim id obstaja.
     *
     * @param osebaId id osebe
     * @throws OsebaServiceException   če je prišlo do napake pri klicu
     * @throws OsebaNeObstajaException če oseba ne obstaja
     */
    public void preveriObstojOsebe(Integer osebaId) throws OsebaServiceException, OsebaNeObstajaException {

        OsebaOdgovor osebaOdgovor = osebaServiceKlient.vrni(osebaId);

        if (!osebaOdgovor.getNapaka().equals("")) {

            if (osebaOdgovor.getNapaka().equals("OsebaNeObstajaException")) {
                throw new OsebaNeObstajaException(osebaOdgovor.getNapakaSporocilo());
            } else {
                throw new OsebaServiceException(
                        String.format("Napaka pri vračanju osebe. Tip napake: %s. Sporočilo: %s",
                                osebaOdgovor.getNapaka(), osebaOdgovor.getNapakaSporocilo()));
            }
        }
    }
}
