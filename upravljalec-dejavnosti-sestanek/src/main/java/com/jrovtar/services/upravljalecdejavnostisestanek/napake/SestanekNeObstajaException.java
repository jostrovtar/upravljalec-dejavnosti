package com.jrovtar.services.upravljalecdejavnostisestanek.napake;

public class SestanekNeObstajaException extends Exception {

    public SestanekNeObstajaException(String message) {
        super(message);
    }
}
