package com.jrovtar.services.upravljalecdejavnostisestanek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpravljalecDejavnostiSestanekApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpravljalecDejavnostiSestanekApplication.class, args);
	}

}
