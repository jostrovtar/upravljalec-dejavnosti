package com.jrovtar.services.upravljalecdejavnostisestanek.grpc;

import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.SestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.napake.ZadolzeniZaSestanekNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekZadolzeniOdgovor;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekZadolzeniServiceGrpc;
import com.jrovtar.services.upravljalecdejavnostisestanek.sestanek.grpc.SestanekZadolzeniZahteva;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanek;
import com.jrovtar.services.upravljalecdejavnostisestanek.zadolzenizasestanek.ZadolzeniZaSestanekService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

@Slf4j
@GRpcService
public class SestanekZadolzeniGrpcServiceImpl extends SestanekZadolzeniServiceGrpc.SestanekZadolzeniServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju osebe na sestanek. ";
    public static final String BRISI_NAPAKA_SPOROCILO = "Napaka pri brisanju osebe iz sesanka. ";

    private ZadolzeniZaSestanekService zadolzeniZaSestanekService;

    public SestanekZadolzeniGrpcServiceImpl(ZadolzeniZaSestanekService zadolzeniZaSestanekService) {
        this.zadolzeniZaSestanekService = zadolzeniZaSestanekService;
    }

    @Override
    public void dodajZadolzenega(SestanekZadolzeniZahteva request, StreamObserver<SestanekZadolzeniOdgovor> responseObserver) {

        SestanekZadolzeniOdgovor sestanekZadolzeniOdgovor;

        try {
            ZadolzeniZaSestanek zadolzeniZaSestanek =
                    zadolzeniZaSestanekService.dodaj(request.getSestanekId(), request.getOsebaId());

            sestanekZadolzeniOdgovor = SestanekZadolzeniOdgovor.newBuilder()
                    .setSestanekId(zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getSestanekId())
                    .setOsebaId(zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof SestanekNeObstajaException ||
                    e instanceof OsebaNeObstajaException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            sestanekZadolzeniOdgovor = SestanekZadolzeniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(sestanekZadolzeniOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void odstraniZadolzenega(SestanekZadolzeniZahteva request, StreamObserver<SestanekZadolzeniOdgovor> responseObserver) {

        SestanekZadolzeniOdgovor sestanekZadolzeniOdgovor;

        try {
            ZadolzeniZaSestanek zadolzeniZaSestanek =
                    zadolzeniZaSestanekService.odstrani(request.getSestanekId(), request.getOsebaId());

            sestanekZadolzeniOdgovor = SestanekZadolzeniOdgovor.newBuilder()
                    .setSestanekId(zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getSestanekId())
                    .setOsebaId(zadolzeniZaSestanek.getZadolzeniZaSestanekPk().getOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof ZadolzeniZaSestanekNeObstajaException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            sestanekZadolzeniOdgovor = SestanekZadolzeniOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(sestanekZadolzeniOdgovor);
        responseObserver.onCompleted();
    }
}
