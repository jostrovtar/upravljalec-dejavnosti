package com.jrovtar.services.upravljalecdejavnostiosebe;

import com.jrovtar.services.upravljalecdejavnostiosebe.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.Oseba;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.OsebaDTO;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.OsebaService;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.OsebaServiceImpl;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private OsebaService osebaService;

    private OsebaServiceGrpc.OsebaServiceFutureStub osebaServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        osebaServiceFutureStub = OsebaServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void vrni_ok() {

        // priprava testnih objektov
        Oseba oseba = new Oseba();
        oseba.setId(1);
        oseba.setIme("Ime");
        oseba.setPriimek("Priimek");

        // mock metode
        Mockito
                .when(osebaService.vrni(oseba.getId()))
                .thenAnswer((Answer<Oseba>) invocation -> {
                    Oseba o = new Oseba();
                    o.setId(invocation.getArgument(0));
                    o.setIme(oseba.getIme());
                    o.setPriimek(oseba.getPriimek());
                    return o;
                });

        // klic testirane metode
        OsebaVrniZahteva osebaVrniZahteva = OsebaVrniZahteva.newBuilder().setId(oseba.getId()).build();
        OsebaOdgovor osebaOdgovor = osebaServiceFutureStub.vrni(osebaVrniZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals(oseba.getId(), Integer.valueOf(osebaOdgovor.getId()));
        Assert.assertEquals(oseba.getIme(), osebaOdgovor.getIme());
        Assert.assertEquals(oseba.getPriimek(), osebaOdgovor.getPriimek());
        Assert.assertEquals("", osebaOdgovor.getNapaka());
        Assert.assertEquals("", osebaOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void vrni_napaka() {

        // priprava testnih podatkov
        Integer id = 1;

        Exception exception = new OsebaNeObstajaException(
                String.format(OsebaServiceImpl.MESSAGE_OSEBA_NE_OBSTAJA, id));

        // mock metode
        Mockito
                .when(osebaService.vrni(id))
                .thenThrow(exception);


        // klic testirane metode
        OsebaVrniZahteva osebaZahteva = OsebaVrniZahteva.newBuilder().setId(id).build();
        OsebaOdgovor osebaOdgovor = osebaServiceFutureStub.vrni(osebaZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals(0, osebaOdgovor.getId());
        Assert.assertEquals("", osebaOdgovor.getIme());
        Assert.assertEquals("", osebaOdgovor.getPriimek());
        Assert.assertEquals(exception.getClass().getSimpleName(), osebaOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), osebaOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_ok() {

        // priprava testnih podatkov
        OsebaDTO oseba = new OsebaDTO();
        oseba.setIme("Ime");
        oseba.setPriimek("Priimek");

        // mock metode
        Mockito
                .when(osebaService.dodaj(oseba))
                .thenAnswer((Answer<Oseba>) invocation -> {

                    OsebaDTO dto = invocation.getArgument(0);

                    Oseba o = new Oseba();
                    o.setId(new Random().nextInt(10) + 1);
                    o.setIme(dto.getIme());
                    o.setPriimek(dto.getPriimek());

                    return o;
                });

        // klic testirane metode
        OsebaDodajZahteva osebaZahteva = OsebaDodajZahteva.newBuilder()
                .setIme(oseba.getIme())
                .setPriimek(oseba.getPriimek())
                .build();
        OsebaOdgovor osebaOdgovor = osebaServiceFutureStub.dodaj(osebaZahteva).get();

        // preverjanje odgovora
        Assert.assertTrue(!(osebaOdgovor.getId() == 0));
        Assert.assertEquals(oseba.getIme(), osebaOdgovor.getIme());
        Assert.assertEquals(oseba.getPriimek(), osebaOdgovor.getPriimek());
        Assert.assertEquals("", osebaOdgovor.getNapaka());
        Assert.assertEquals("", osebaOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_napaka() {

        // priprava testnih podatkov
        Exception exception =
                new IllegalArgumentException("napaka");

        // mock metode
        Mockito
                .when(osebaService.dodaj(ArgumentMatchers.any(OsebaDTO.class)))
                .thenThrow(exception);

        // klic testirane metode
        OsebaDodajZahteva osebaZahteva = OsebaDodajZahteva.newBuilder()
                .setIme("")
                .setPriimek("")
                .build();
        OsebaOdgovor osebaOdgovor = osebaServiceFutureStub.dodaj(osebaZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals("", osebaOdgovor.getIme());
        Assert.assertEquals("", osebaOdgovor.getPriimek());
        Assert.assertEquals(exception.getClass().getSimpleName(), osebaOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), osebaOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_ok() {

        // priprava testnih podatkov
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(1);
        oseba.setIme("Ime");
        oseba.setPriimek("Priimek");

        // mock metode
        Mockito
                .when(osebaService.posodobi(oseba))
                .thenAnswer((Answer<Oseba>) invocation -> {

                    OsebaDTO dto = invocation.getArgument(0);

                    Oseba o = new Oseba();
                    o.setId(dto.getId());
                    o.setIme(dto.getIme());
                    o.setPriimek(dto.getPriimek());

                    return o;
                });


        // klic testirane metode
        OsebaPosodobiZahteva osebaZahteva = OsebaPosodobiZahteva.newBuilder()
                .setId(oseba.getId())
                .setIme(oseba.getIme())
                .setPriimek(oseba.getPriimek())
                .build();
        OsebaOdgovor osebaOdgovor = osebaServiceFutureStub.posodobi(osebaZahteva).get();

        // preverjanje odgovora
        Assert.assertTrue(!(osebaOdgovor.getId() == 0));
        Assert.assertEquals(oseba.getIme(), osebaOdgovor.getIme());
        Assert.assertEquals(oseba.getPriimek(), osebaOdgovor.getPriimek());
        Assert.assertEquals("", osebaOdgovor.getNapaka());
        Assert.assertEquals("", osebaOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_napaka() {

        // priprava testnih podatkov
        Integer id = 1;

        Exception exception = new OsebaNeObstajaException(
                String.format(OsebaServiceImpl.MESSAGE_OSEBA_NE_OBSTAJA, id));

        // mock metode
        Mockito
                .when(osebaService.posodobi(ArgumentMatchers.any(OsebaDTO.class)))
                .thenThrow(exception);

        // Klic testirane metode
        OsebaPosodobiZahteva osebaZahteva = OsebaPosodobiZahteva.newBuilder()
                .setId(id)
                .setIme("")
                .setPriimek("")
                .build();
        OsebaOdgovor osebaOdgovor = osebaServiceFutureStub.posodobi(osebaZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals("", osebaOdgovor.getIme());
        Assert.assertEquals("", osebaOdgovor.getPriimek());
        Assert.assertEquals(exception.getClass().getSimpleName(), osebaOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), osebaOdgovor.getNapakaSporocilo());
    }
}