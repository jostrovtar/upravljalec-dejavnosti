package com.jrovtar.services.upravljalecdejavnostiosebe;

import com.jrovtar.services.upravljalecdejavnostiosebe.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.*;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.annotation.Validated;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Random;

@RunWith(SpringRunner.class)
public class OsebaServiceImplTest {

    @MockBean
    private OsebaRepository osebaRepository;

    private OsebaService osebaService;

    @Before
    public void init() {
        osebaService = new OsebaServiceImpl(osebaRepository);
    }

    @SneakyThrows
    @Test
    public void vrni_ok() {

        // Priprava osebe za testiranje.
        Oseba oseba = new Oseba();
        oseba.setId(1);
        oseba.setIme("Janez");
        oseba.setPriimek("Novak");

        // Mock metode osebaRepository.findById().
        Mockito.when(osebaRepository.findById(oseba.getId()))
                .thenReturn(java.util.Optional.of(oseba));

        Oseba o = osebaService.vrni(oseba.getId());

        Mockito.verify(osebaRepository, Mockito.times(1)).findById(ArgumentMatchers.any(Integer.class));
        Assert.assertEquals(oseba.getId(), o.getId());
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void vrni_neObstaja() {

        osebaService.vrni(0);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void vrni_idNull() {

        osebaService.vrni(null);
    }

    @Test
    public void dodaj_ok() {

        // Mock metode osebaRepository.save().
        Mockito
                .when(osebaRepository.save(ArgumentMatchers.any(Oseba.class)))
                .thenAnswer((Answer<Oseba>) invocation -> {
                    Oseba oseba = invocation.getArgument(0);
                    oseba.setId(new Random().nextInt(10));
                    return oseba;
                });

        OsebaDTO dto = new OsebaDTO();
        dto.setIme("Ime");
        dto.setPriimek("Priimek");

        Oseba o = osebaService.dodaj(dto);

        Mockito.verify(osebaRepository, Mockito.times(1)).save(ArgumentMatchers.any(Oseba.class));
        Assert.assertNotNull(o);
        Assert.assertNotNull(o.getId());
        Assert.assertEquals(dto.getIme(), o.getIme());
        Assert.assertEquals(dto.getPriimek(), o.getPriimek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void dodaj_null() {
        osebaService.dodaj(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_brezAnotacijeValidated() {

        Method method = OsebaServiceImpl.class.getDeclaredMethod("dodaj", OsebaDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(OsebaDTO.ObDodajanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void posodobi_ok() {

        // priprava testnih podatkov
        Oseba oseba = new Oseba();
        oseba.setId(1);
        oseba.setIme("Ime");
        oseba.setPriimek("Priimek");

        OsebaDTO dto = OsebaDTO.kreirajDto(oseba);

        // Mock metod
        Mockito.when(osebaRepository.findById(dto.getId()))
                .thenReturn(java.util.Optional.of(oseba));

        Mockito
                .when(osebaRepository.save(ArgumentMatchers.any(Oseba.class)))
                .thenAnswer((Answer<Oseba>) invocation -> invocation.getArgument(0));

        // klic testirane metode
        Oseba o = osebaService.posodobi(dto);

        // preverjane rezultata
        Mockito.verify(osebaRepository, Mockito.times(1)).save(ArgumentMatchers.any(Oseba.class));
        Assert.assertNotNull(o);
        Assert.assertEquals(dto.getId(), o.getId());
        Assert.assertEquals(dto.getIme(), o.getIme());
        Assert.assertEquals(dto.getPriimek(), o.getPriimek());
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void posodobi_null() {
        osebaService.posodobi(null);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void posodobi_neObstaja() {

        // priprava testnih podatkov
        Oseba oseba = new Oseba();
        oseba.setId(1);
        oseba.setIme("Ime");
        oseba.setPriimek("Priimek");

        OsebaDTO dto = OsebaDTO.kreirajDto(oseba);

        // mock metode
        Mockito
                .when(osebaRepository.save(ArgumentMatchers.any(Oseba.class)))
                .thenAnswer((Answer<Oseba>) invocation -> invocation.getArgument(0));

        // klic testirane metode
        osebaService.posodobi(dto);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeValidated() {

        Method method = OsebaServiceImpl.class.getDeclaredMethod("posodobi", OsebaDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(OsebaDTO.ObPosodabljanju.class.getName(), annotation.value()[0].getName());
    }
}
