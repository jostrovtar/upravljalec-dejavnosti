package com.jrovtar.services.upravljalecdejavnostiosebe;

import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.OsebaDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class PosodabljanjeOsebaDTOTests {

    private static Validator validator;
    private static Class[] group;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        group = new Class[]{OsebaDTO.ObPosodabljanju.class};
    }

    @Test
    public void idNull() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(null);
        oseba.setIme("Ime");
        oseba.setPriimek("Priimek");
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("id");
    }

    @Test
    public void imeNull() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme(null);
        oseba.setPriimek("Priimek");
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("ime");
    }

    @Test
    public void imePrazno() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme("");
        oseba.setPriimek("Priimek");
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("ime");
    }

    @Test
    public void imePredolgo() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme("ZeloDolgoImeZeloDolgoImeZeloDolgoImeZeloDolgoI"); // 46 znakov
        oseba.setPriimek("Priimek");
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("ime");
    }

    @Test
    public void priimekNull() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme("Ime");
        oseba.setPriimek(null);
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("priimek");
    }

    @Test
    public void priimekPrazen() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme("Ime");
        oseba.setPriimek(null);
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("priimek");
    }

    @Test
    public void priimekPredolg() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme("Ime");
        oseba.setPriimek("ZeloDolgPriimekZeloDolgPriimekZeloDolgPriimekZ");
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("priimek");
    }

    @Test
    public void ok() {
        OsebaDTO oseba = new OsebaDTO();
        oseba.setId(0);
        oseba.setIme("ZeloDolgoImeZeloDolgoImeZeloDolgoImeZeloDolgo"); // 45 znakov
        oseba.setPriimek("ZeloDolgPriimekZeloDolgPriimekZeloDolgPriimek"); // 45 znakov
        Set<ConstraintViolation<OsebaDTO>> violations =
                validator.validate(oseba, group);

        assertThat(violations.size()).isEqualTo(0);
    }
}
