create table if not exists oseba
(
    id      int auto_increment primary key,
    ime     varchar(45) not null,
    priimek varchar(45) not null
)