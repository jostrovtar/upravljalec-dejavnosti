package com.jrovtar.services.upravljalecdejavnostiosebe.osebe;

import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaDodajZahteva;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaPosodobiZahteva;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class OsebaDTO {

    @NotNull(
            groups = {ObPosodabljanju.class},
            message = "Id je obvezen."
    )
    private Integer id;

    @NotBlank(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Ime je obvezno.")
    @Length(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            max = 45,
            message = "Največja dovoljena dolžina imena je {max} znakov.")
    private String ime;

    @NotBlank(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Priimek je obvezen.")
    @Length(groups = {ObDodajanju.class, ObPosodabljanju.class},
            max = 45,
            message = "Največja dovoljena dolžina priimka je {max} znakov.")
    private String priimek;

    /**
     * Kreiraj OsebaDTO iz Oseba.
     *
     * @param o instanca tipa Oseba
     * @return instanca tipa OsebaDTO
     */
    public static OsebaDTO kreirajDto(Oseba o) {

        if (o == null) {
            return null;
        }

        OsebaDTO dto = new OsebaDTO();
        dto.setId(o.getId());
        dto.setIme(o.getIme());
        dto.setPriimek(o.getPriimek());

        return dto;
    }

    /**
     * Kreiraj OsebaDTO iz OsebaPosodobiZahteva.
     *
     * @param o instanca tipa OsebaPosodobiZahteva
     * @return instanca tipa OsebaDTO
     */
    public static OsebaDTO kreirajDto(OsebaPosodobiZahteva o) {

        if (o == null) {
            return null;
        }

        OsebaDTO dto = new OsebaDTO();
        dto.setId(o.getId());
        dto.setIme(o.getIme());
        dto.setPriimek(o.getPriimek());

        return dto;
    }

    /**
     * Kreiraj OsebaDTO iz OsebaDodajZahteva.
     *
     * @param o instanca tipa OsebaDodajZahteva
     * @return instanca tipa OsebaDTO
     */
    public static OsebaDTO kreirajDto(OsebaDodajZahteva o) {

        if (o == null) {
            return null;
        }

        OsebaDTO dto = new OsebaDTO();
        dto.setIme(o.getIme());
        dto.setPriimek(o.getPriimek());

        return dto;
    }

    /*
     * Tipa, ki se uporabljata za določanje validacijske skupne (validation group)
     * pri validiranju OsebaDTO v metodah razdreda OsebaServiceImpl.
     */

    public interface ObDodajanju {
    }

    public interface ObPosodabljanju {
    }
}


