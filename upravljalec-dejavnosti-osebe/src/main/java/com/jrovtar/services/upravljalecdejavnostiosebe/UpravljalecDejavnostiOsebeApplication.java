package com.jrovtar.services.upravljalecdejavnostiosebe;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class UpravljalecDejavnostiOsebeApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpravljalecDejavnostiOsebeApplication.class, args);
	}

}
