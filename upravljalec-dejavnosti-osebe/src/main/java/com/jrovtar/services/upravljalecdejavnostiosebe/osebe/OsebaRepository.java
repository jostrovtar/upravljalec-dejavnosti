package com.jrovtar.services.upravljalecdejavnostiosebe.osebe;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface OsebaRepository extends CrudRepository<Oseba, Integer> {
}
