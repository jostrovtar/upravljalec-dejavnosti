package com.jrovtar.services.upravljalecdejavnostiosebe.osebe;

import com.jrovtar.services.upravljalecdejavnostiosebe.napake.OsebaNeObstajaException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Optional;

@Slf4j
@Service
@Validated
public class OsebaServiceImpl implements OsebaService {

    public static final String MESSAGE_OSEBA_NE_OBSTAJA = "Oseba z id '%d' ne obstaja.";
    public static final String MESSAGE_OSEBA_NE_SME_BITI_NULL = "Oseba ne sme biti null.";
    public static final String MESSAGE_ID_OSEBE_NI_PODAN = "Id osebe mora biti podan.";

    private OsebaRepository osebaRepository;

    public OsebaServiceImpl(OsebaRepository osebaRepository) {
        this.osebaRepository = osebaRepository;
    }

    /**
     * Dodaj osebo.
     *
     * @param dto oseba
     * @return dodana oseba
     */
    @Validated(OsebaDTO.ObDodajanju.class)
    public Oseba dodaj(@Valid OsebaDTO dto) {

        Assert.notNull(dto, MESSAGE_OSEBA_NE_SME_BITI_NULL);

        log.debug("Dodajanje osebe: {}", dto);

        Oseba o = new Oseba();
        o.setIme(dto.getIme());
        o.setPriimek(dto.getPriimek());

        o = osebaRepository.save(o);

        return o;
    }

    /**
     * Vrni osebo.
     *
     * @param id id osebe
     * @return oseba
     * @throws OsebaNeObstajaException če oseba z podanim id-jem ne obstaja.
     */
    public Oseba vrni(Integer id) throws OsebaNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_OSEBE_NI_PODAN);

        Optional<Oseba> osebaOptional = osebaRepository.findById(id);

        if (!osebaOptional.isPresent()) {
            throw new OsebaNeObstajaException(
                    String.format(MESSAGE_OSEBA_NE_OBSTAJA, id));
        }

        return osebaOptional.get();
    }


    /**
     * Posodobi osebo.
     *
     * @param dto oseba
     * @return posodobljena oseba
     * @throws OsebaNeObstajaException če oseba z podanim id-jem ne obstaja.
     */
    @Validated(OsebaDTO.ObPosodabljanju.class)
    public Oseba posodobi(@Valid OsebaDTO dto) throws OsebaNeObstajaException {

        Assert.notNull(dto, MESSAGE_OSEBA_NE_SME_BITI_NULL);

        log.debug("Posodabljanje osebe: {}", dto);

        Oseba o = vrni(dto.getId());
        o.setIme(dto.getIme());
        o.setPriimek(dto.getPriimek());
        o = osebaRepository.save(o);

        return o;
    }

}
