package com.jrovtar.services.upravljalecdejavnostiosebe.napake;

public class OsebaNeObstajaException extends Exception {

    public OsebaNeObstajaException(String message) {
        super(message);
    }
}
