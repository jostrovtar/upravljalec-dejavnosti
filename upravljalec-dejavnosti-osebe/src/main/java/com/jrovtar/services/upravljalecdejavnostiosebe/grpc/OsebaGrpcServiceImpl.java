package com.jrovtar.services.upravljalecdejavnostiosebe.grpc;

import com.jrovtar.services.upravljalecdejavnostiosebe.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.Oseba;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.OsebaDTO;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.OsebaService;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.*;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import javax.validation.ConstraintViolationException;

@Slf4j
@GRpcService
public class OsebaGrpcServiceImpl extends OsebaServiceGrpc.OsebaServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju osebe. ";
    public static final String VRNI_NAPAKA_SPOROCILO = "Napaka pri vnačanju osebe. ";
    public static final String POSODOBI_NAPAKA_SPOROCILO = "Napaka pri posodabljanju osebe. ";

    private OsebaService osebaService;

    public OsebaGrpcServiceImpl(OsebaService osebaService) {
        this.osebaService = osebaService;
    }

    @Override
    public void dodaj(OsebaDodajZahteva request, StreamObserver<OsebaOdgovor> responseObserver) {

        OsebaOdgovor osebaOdgovor;

        try {
            Oseba o = osebaService.dodaj(OsebaDTO.kreirajDto(request));
            osebaOdgovor = OsebaOdgovor.newBuilder()
                    .setId(o.getId())
                    .setIme(o.getIme())
                    .setPriimek(o.getPriimek())
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof ConstraintViolationException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            osebaOdgovor = OsebaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(osebaOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void vrni(OsebaVrniZahteva request, StreamObserver<OsebaOdgovor> responseObserver) {

        OsebaOdgovor osebaOdgovor;

        try {
            Oseba o = osebaService.vrni(request.getId());
            osebaOdgovor = OsebaOdgovor.newBuilder()
                    .setId(o.getId())
                    .setIme(o.getIme())
                    .setPriimek(o.getPriimek())
                    .build();

        } catch (Exception e) {

            if (e instanceof OsebaNeObstajaException ||
                    e instanceof IllegalArgumentException ||
                    e instanceof ConstraintViolationException) {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            osebaOdgovor = OsebaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(osebaOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void posodobi(OsebaPosodobiZahteva request, StreamObserver<OsebaOdgovor> responseObserver) {

        OsebaOdgovor osebaOdgovor;

        try {
            Oseba o = osebaService.posodobi(OsebaDTO.kreirajDto(request));
            osebaOdgovor = OsebaOdgovor.newBuilder()
                    .setId(o.getId())
                    .setIme(o.getIme())
                    .setPriimek(o.getPriimek())
                    .build();

        } catch (Exception e) {

            if (e instanceof OsebaNeObstajaException ||
                    e instanceof IllegalArgumentException ||
                    e instanceof ConstraintViolationException) {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            osebaOdgovor = OsebaOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(osebaOdgovor);
        responseObserver.onCompleted();
    }
}
