package com.jrovtar.services.upravljalecdejavnostiosebe.osebe;

import com.jrovtar.services.upravljalecdejavnostiosebe.napake.OsebaNeObstajaException;

import javax.validation.Valid;

public interface OsebaService {

    /**
     * Dodaj osebo.
     *
     * @param dto oseba
     * @return dodana oseba
     */
    Oseba dodaj(@Valid OsebaDTO dto);

    /**
     * Vrni osebo.
     *
     * @param id id osebe
     * @return oseba
     */
    Oseba vrni(Integer id) throws OsebaNeObstajaException;

    /**
     * Posodobi osebo.
     *
     * @param dto oseba
     * @return posodobljena oseba
     * @throws OsebaNeObstajaException če oseba z podanim id-jem ne obstaja.
     */
    public Oseba posodobi(@Valid OsebaDTO dto) throws OsebaNeObstajaException;

}
