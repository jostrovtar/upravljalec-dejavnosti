CREATE TABLE IF NOT EXISTS mejnik
(
    `id`                 INT          NOT NULL AUTO_INCREMENT,
    `naziv`              VARCHAR(255) NOT NULL,
    `datum_zakljucka`    DATE         NOT NULL,
    `zadolzena_oseba_id` INT          NOT NULL,
    PRIMARY KEY (`id`)
)