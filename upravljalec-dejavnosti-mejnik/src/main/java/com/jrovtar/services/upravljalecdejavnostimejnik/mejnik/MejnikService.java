package com.jrovtar.services.upravljalecdejavnostimejnik.mejnik;

import com.jrovtar.services.upravljalecdejavnostimejnik.napake.MejnikNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaServiceException;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

public interface MejnikService {

    /**
     * Dodaj mejnik.
     *
     * @param dto mejnik
     * @return dodan mejnik
     * @throws OsebaServiceException   če je prišlo do napake pri preverjanju osebe
     * @throws OsebaNeObstajaException če zadolžena oseba s podanim id-jem ne obstaja
     */
    @Validated(MejnikDTO.ObDodajanju.class)
    Mejnik dodaj(@Valid MejnikDTO dto) throws OsebaNeObstajaException, OsebaServiceException;

    /**
     * Vrni mejnik.
     *
     * @param id id mejnika
     * @return mejnik
     * @throws MejnikNeObstajaException če mejnik z podanim id-jem ne obstaja.
     */
    Mejnik vrni(Integer id) throws MejnikNeObstajaException;

    /**
     * Posodobi mejnik.
     *
     * @param dto mejnik
     * @return posodobljen mejnik
     * @throws MejnikNeObstajaException če mejnik z podanim id-jem ne obstaja.
     * @throws OsebaServiceException    če je prišlo do napake pri preverjanju osebe
     * @throws OsebaNeObstajaException  če zadolžena oseba s podanim id-jem ne obstaja
     */
    @Validated(MejnikDTO.ObPosodabljanju.class)
    Mejnik posodobi(@Valid MejnikDTO dto) throws MejnikNeObstajaException, OsebaNeObstajaException, OsebaServiceException;


    /**
     * Filtriraj mejnike po nazivu in zadolženi osebi. Filtriranje po nazivu se ne izvede, če je nazivFilter null ali
     * prazen string. Filtriranje po zadolženi osebi se ne izvede, če je zadolzenaOsebaIdFilter null ali manjši od 0.
     *
     * @param nazivFilter            filter po nazivu mejnika
     * @param zadolzenaOsebaIdFilter filter po zadolzeni osebi.
     * @return seznam mejnikov, ki ustrezajo filtru
     */
    List<Mejnik> filriraj(String nazivFilter, Integer zadolzenaOsebaIdFilter);

    /**
     * Izbrisi mejnik.
     *
     * @param id id mejnika
     * @return mejnik
     * @throws MejnikNeObstajaException če mejnik z podanim id-jem ne obstaja.
     */
    Mejnik izbrisi(Integer id) throws MejnikNeObstajaException;

    /**
     * Preveri, ali oseba s podanim id obstaja.
     *
     * @param osebaId id osebe
     * @throws OsebaServiceException   če je prišlo do napake pri klicu
     * @throws OsebaNeObstajaException če oseba ne obstaja
     */
    void preveriObstojOsebe(Integer osebaId) throws OsebaServiceException, OsebaNeObstajaException;
}
