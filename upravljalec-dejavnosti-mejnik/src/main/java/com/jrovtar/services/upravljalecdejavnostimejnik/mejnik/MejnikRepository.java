package com.jrovtar.services.upravljalecdejavnostimejnik.mejnik;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MejnikRepository extends CrudRepository<Mejnik, Integer> {

    @Query("SELECT m FROM Mejnik m WHERE (:nazivFilter IS NULL OR m.naziv LIKE %:nazivFilter%) AND (:zadolzenaOsebaFilter IS NULL OR m.zadolzenaOsebaId = :zadolzenaOsebaFilter)")
    List<Mejnik> filtriraj(@Param("nazivFilter") String nazivFilter, @Param("zadolzenaOsebaFilter") Integer zadolzenaOsebaFilter);

}
