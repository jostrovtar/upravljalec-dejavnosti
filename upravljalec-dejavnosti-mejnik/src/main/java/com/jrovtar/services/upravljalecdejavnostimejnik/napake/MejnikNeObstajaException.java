package com.jrovtar.services.upravljalecdejavnostimejnik.napake;

public class MejnikNeObstajaException extends Exception {

    public MejnikNeObstajaException(String message) {
        super(message);
    }
}
