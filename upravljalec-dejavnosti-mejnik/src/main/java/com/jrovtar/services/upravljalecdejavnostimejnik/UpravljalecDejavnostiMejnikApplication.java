package com.jrovtar.services.upravljalecdejavnostimejnik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpravljalecDejavnostiMejnikApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpravljalecDejavnostiMejnikApplication.class, args);
    }

}
