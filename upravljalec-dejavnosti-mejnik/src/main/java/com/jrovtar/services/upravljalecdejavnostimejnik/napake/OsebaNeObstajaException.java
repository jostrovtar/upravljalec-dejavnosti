package com.jrovtar.services.upravljalecdejavnostimejnik.napake;

public class OsebaNeObstajaException extends Exception {

    public OsebaNeObstajaException(String message) {
        super(message);
    }
}
