package com.jrovtar.services.upravljalecdejavnostimejnik.mejnik;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class Mejnik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String naziv;
    private LocalDate datumZakljucka;
    private Integer zadolzenaOsebaId;

}
