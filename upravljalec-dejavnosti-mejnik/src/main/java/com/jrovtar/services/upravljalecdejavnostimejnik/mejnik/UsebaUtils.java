package com.jrovtar.services.upravljalecdejavnostimejnik.mejnik;

import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class UsebaUtils {

    static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private UsebaUtils() {
    }


    /**
     * Kreiraj LocalDate iz stringa. Zahtevan format dd.MM.yyyy .
     *
     * @param strDatum formatiran datum
     * @return LocalDate
     * @throws IllegalArgumentException če je strDatum null ali prazen string
     * @throws DateTimeParseException   če strDatum ni pravilno formatiran
     */
    public static LocalDate kreirajLocalDate(String strDatum) {

        Assert.hasText(strDatum, "Podan string je null ali prazen.");

        return LocalDate.parse(strDatum, formatter);
    }

    /**
     * Kreiraj String iz LocalDate. Format: dd.MM.yyyy .
     *
     * @param datum datum
     * @return formatiran datum
     */
    public static String formatiraj(LocalDate datum) {

        if (datum == null) {
            return "";
        }

        return datum.format(formatter);
    }

}
