package com.jrovtar.services.upravljalecdejavnostimejnik.grpc;

import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.Mejnik;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.MejnikDTO;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.MejnikService;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.UsebaUtils;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.*;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.MejnikNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaNeObstajaException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import javax.validation.ConstraintViolationException;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@GRpcService
public class MejnikGrpcServiceImpl extends MejnikServiceGrpc.MejnikServiceImplBase {

    public static final String DODAJ_NAPAKA_SPOROCILO = "Napaka pri dodajanju mejnika. ";
    public static final String VRNI_NAPAKA_SPOROCILO = "Napaka pri vnačanju mejnika. ";
    public static final String POSODOBI_NAPAKA_SPOROCILO = "Napaka pri posodabljanju mejnika. ";
    public static final String IZBRISI_NAPAKA_SPOROCILO = "Napaka pri brisanju mejnika. ";
    public static final String FILTRIRAJ_NAPAKA_SPOROCILO = "Napaka pri filtriranju mejnikov. ";

    private MejnikService mejnikService;

    public MejnikGrpcServiceImpl(MejnikService mejnikService) {
        this.mejnikService = mejnikService;
    }


    @Override
    public void dodaj(MejnikDodajZahteva request, StreamObserver<MejnikOdgovor> responseObserver) {

        MejnikOdgovor mejnikOdgovor;

        try {
            Mejnik m = mejnikService.dodaj(MejnikDTO.kreirajDTO(request));
            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setId(m.getId())
                    .setNaziv(m.getNaziv())
                    .setDatumZakljucka(
                            UsebaUtils.formatiraj(m.getDatumZakljucka()))
                    .setZadolzenaOsebaId(m.getZadolzenaOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException ||
                    e instanceof OsebaNeObstajaException) {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(DODAJ_NAPAKA_SPOROCILO + e.getMessage(), e);
            }
            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(mejnikOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void vrni(MejnikVrniZahteva request, StreamObserver<MejnikOdgovor> responseObserver) {

        MejnikOdgovor mejnikOdgovor;

        try {
            Mejnik m = mejnikService.vrni(request.getId());
            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setId(m.getId())
                    .setNaziv(m.getNaziv())
                    .setDatumZakljucka(
                            UsebaUtils.formatiraj(m.getDatumZakljucka()))
                    .setZadolzenaOsebaId(m.getZadolzenaOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof MejnikNeObstajaException ||
                    e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException) {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(VRNI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }
            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(mejnikOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void izbrisi(MejnikIzbrisiZahteva request, StreamObserver<MejnikOdgovor> responseObserver) {

        MejnikOdgovor mejnikOdgovor;

        try {
            Mejnik m = mejnikService.izbrisi(request.getId());
            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setId(m.getId())
                    .setNaziv(m.getNaziv())
                    .setDatumZakljucka(
                            UsebaUtils.formatiraj(m.getDatumZakljucka()))
                    .setZadolzenaOsebaId(m.getZadolzenaOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof MejnikNeObstajaException ||
                    e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException) {
                log.error(IZBRISI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(IZBRISI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(mejnikOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void posodobi(MejnikPosodobiZahteva request, StreamObserver<MejnikOdgovor> responseObserver) {

        MejnikOdgovor mejnikOdgovor;

        try {
            Mejnik m = mejnikService.posodobi(MejnikDTO.kreirajDTO(request));
            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setId(m.getId())
                    .setNaziv(m.getNaziv())
                    .setDatumZakljucka(
                            UsebaUtils.formatiraj(m.getDatumZakljucka()))
                    .setZadolzenaOsebaId(m.getZadolzenaOsebaId())
                    .build();

        } catch (Exception e) {

            if (e instanceof MejnikNeObstajaException ||
                    e instanceof IllegalArgumentException ||
                    e instanceof DateTimeParseException ||
                    e instanceof ConstraintViolationException ||
                    e instanceof OsebaNeObstajaException) {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage());
            } else {
                log.error(POSODOBI_NAPAKA_SPOROCILO + e.getMessage(), e);
            }

            mejnikOdgovor = MejnikOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(mejnikOdgovor);
        responseObserver.onCompleted();
    }

    @Override
    public void filtriraj(MejnikFiltrirajZahteva request, StreamObserver<MejnikFiltrirajOdgovor> responseObserver) {

        MejnikFiltrirajOdgovor odgovor;

        try {
            List<Mejnik> list = mejnikService.filriraj(request.getFilterNaziv(), request.getFilterZadolzenaOseba());

            odgovor = MejnikFiltrirajOdgovor.newBuilder()
                    .addAllItems(
                            list.stream().map(
                                    e -> MejnikNazivOdgovor.newBuilder().setNaziv(e.getNaziv()).build()
                            ).collect(Collectors.toList()))
                    .build();

        } catch (Exception e) {
            log.error(FILTRIRAJ_NAPAKA_SPOROCILO + e.getMessage(), e);

            odgovor = MejnikFiltrirajOdgovor.newBuilder()
                    .setNapaka(e.getClass().getSimpleName())
                    .setNapakaSporocilo(e.getMessage())
                    .build();
        }

        responseObserver.onNext(odgovor);
        responseObserver.onCompleted();

    }
}
