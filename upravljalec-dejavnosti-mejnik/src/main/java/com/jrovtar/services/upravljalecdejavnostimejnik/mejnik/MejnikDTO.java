package com.jrovtar.services.upravljalecdejavnostimejnik.mejnik;

import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikDodajZahteva;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.MejnikPosodobiZahteva;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class MejnikDTO {

    @NotNull(
            groups = {ObPosodabljanju.class},
            message = "Id je obvezen."
    )
    private Integer id;

    @NotBlank(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Naziv je obvezen.")
    @Length(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            max = 255,
            message = "Največja dovoljena dolžina naziva je {max} znakov.")
    private String naziv;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Datum zaključka je obvezen.")
    private LocalDate datumZakljucka;

    @NotNull(
            groups = {ObDodajanju.class, ObPosodabljanju.class},
            message = "Id zadolžene osebe je obvezen.")
    private Integer zadolzenaOsebaId;


    /**
     * Kreiraj MejnikDTO iz Mejnik.
     *
     * @param mejnik instanca tipa Mejnik
     * @return instanca tipa MejniKDTO
     */
    public static MejnikDTO kreirajDTO(Mejnik mejnik) {

        if (mejnik == null) {
            return null;
        }

        MejnikDTO dto = new MejnikDTO();
        dto.setId(mejnik.getId());
        dto.setNaziv(mejnik.getNaziv());
        dto.setDatumZakljucka(mejnik.getDatumZakljucka());
        dto.setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId());

        return dto;
    }

    /**
     * Kreiraj MejnikDTO iz MejnikDodajZahteva.
     *
     * @param mejnik instanca tipa MejnikDodajZahteva
     * @return instanca tipa MejniKDTO
     */
    public static MejnikDTO kreirajDTO(MejnikDodajZahteva mejnik) {

        if (mejnik == null) {
            return null;
        }

        MejnikDTO dto = new MejnikDTO();
        dto.setNaziv(mejnik.getNaziv());
        dto.setDatumZakljucka(
                StringUtils.isEmpty(mejnik.getDatumZakljucka()) ?
                        null : UsebaUtils.kreirajLocalDate(mejnik.getDatumZakljucka()));
        dto.setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId());

        return dto;
    }

    /**
     * Kreiraj MejnikDTO iz MejnikPosodobiZahteva.
     *
     * @param mejnik instanca tipa MejnikPosodobiZahteva
     * @return instanca tipa MejniKDTO
     */
    public static MejnikDTO kreirajDTO(MejnikPosodobiZahteva mejnik) {

        if (mejnik == null) {
            return null;
        }

        MejnikDTO dto = new MejnikDTO();
        dto.setId(mejnik.getId());
        dto.setNaziv(mejnik.getNaziv());
        dto.setDatumZakljucka(
                StringUtils.isEmpty(mejnik.getDatumZakljucka()) ?
                        null : UsebaUtils.kreirajLocalDate(mejnik.getDatumZakljucka()));
        dto.setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId());

        return dto;
    }

    /*
     * Tipa, ki se uporabljata za določanje validacijske skupne (validation group)
     * pri validiranju MejnikDTO v metodah razdreda MejnikServiceImpl.
     */

    public interface ObDodajanju {
    }

    public interface ObPosodabljanju {
    }
}
