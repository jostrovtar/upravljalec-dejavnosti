package com.jrovtar.services.upravljalecdejavnostimejnik.mejnik;

import com.jrovtar.services.upravljalecdejavnostimejnik.klienti.OsebaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.MejnikNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@Validated
public class MejnikServiceImpl implements MejnikService {

    public static final String MESSAGE_MEJNIK_NE_OBSTAJA = "Mejnik z id '%d' ne obstaja.";
    public static final String MESSAGE_MEJNIK_NE_SME_BITI_NULL = "Mejnik ne sme biti null.";
    public static final String MESSAGE_ID_MEJNIKA_NI_PODAN = "Id mejnika mora biti podan.";

    private MejnikRepository mejnikRepository;
    private OsebaServiceKlient osebaServiceKlient;

    public MejnikServiceImpl(MejnikRepository mejnikRepository, OsebaServiceKlient osebaServiceKlient) {
        this.mejnikRepository = mejnikRepository;
        this.osebaServiceKlient = osebaServiceKlient;
    }

    /**
     * Dodaj mejnik.
     *
     * @param dto mejnik
     * @return dodan mejnik
     * @throws OsebaServiceException   če je prišlo do napake pri preverjanju osebe
     * @throws OsebaNeObstajaException če zadolžena oseba s podanim id-jem ne obstaja
     */
    @Validated(MejnikDTO.ObDodajanju.class)
    public Mejnik dodaj(@Valid MejnikDTO dto) throws OsebaNeObstajaException, OsebaServiceException {

        Assert.notNull(dto, MESSAGE_MEJNIK_NE_SME_BITI_NULL);

        log.debug("Dodajanje mejnika: {}", dto);

        // preveri ali oseba obstaja zadolzene osebe
        preveriObstojOsebe(dto.getZadolzenaOsebaId());

        Mejnik m = new Mejnik();
        m.setNaziv(dto.getNaziv());
        m.setDatumZakljucka(dto.getDatumZakljucka());
        m.setZadolzenaOsebaId(dto.getZadolzenaOsebaId());

        m = mejnikRepository.save(m);

        return m;
    }

    /**
     * Vrni mejnik.
     *
     * @param id id mejnika
     * @return mejnik
     * @throws MejnikNeObstajaException če mejnik z podanim id-jem ne obstaja.
     */
    public Mejnik vrni(Integer id) throws MejnikNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_MEJNIKA_NI_PODAN);

        Optional<Mejnik> mejnikOptional = mejnikRepository.findById(id);

        if (!mejnikOptional.isPresent()) {
            throw new MejnikNeObstajaException(
                    String.format(MESSAGE_MEJNIK_NE_OBSTAJA, id));
        }

        return mejnikOptional.get();
    }


    /**
     * Posodobi mejnik.
     *
     * @param dto mejnik
     * @return posodobljen mejnik
     * @throws MejnikNeObstajaException če mejnik z podanim id-jem ne obstaja.
     */
    @Validated(MejnikDTO.ObPosodabljanju.class)
    public Mejnik posodobi(@Valid MejnikDTO dto) throws MejnikNeObstajaException, OsebaNeObstajaException, OsebaServiceException {

        Assert.notNull(dto, MESSAGE_MEJNIK_NE_SME_BITI_NULL);

        log.debug("Posodabljanje mejnika: {}", dto);

        // preveri ali oseba obstaja zadolzene osebe
        preveriObstojOsebe(dto.getZadolzenaOsebaId());

        Mejnik m = vrni(dto.getId());
        m.setNaziv(dto.getNaziv());
        m.setDatumZakljucka(dto.getDatumZakljucka());
        m.setZadolzenaOsebaId(dto.getZadolzenaOsebaId());

        m = mejnikRepository.save(m);

        return m;
    }


    /**
     * Filtriraj mejnike po nazivu in zadolženi osebi. Filtriranje po nazivu se ne izvede, če je nazivFilter null ali
     * prazen string. Filtriranje po zadolženi osebi se ne izvede, če je zadolzenaOsebaIdFilter null ali manjši od 0.
     *
     * @param nazivFilter            filter po nazivu mejnika
     * @param zadolzenaOsebaIdFilter filter po zadolzeni osebi.
     * @return seznam mejnikov, ki ustrezajo filtru
     */
    public List<Mejnik> filriraj(String nazivFilter, Integer zadolzenaOsebaIdFilter) {

        String fnaziv = "".equals(nazivFilter) ? null : nazivFilter;

        Integer foseba = zadolzenaOsebaIdFilter;

        if (foseba != null && foseba < 0) {
            foseba = null;
        }

        return mejnikRepository.filtriraj(fnaziv, foseba);
    }

    /**
     * Izbrisi mejnik.
     *
     * @param id id mejnika
     * @return izbrisan mejnik
     * @throws MejnikNeObstajaException če mejnik z podanim id-jem ne obstaja.
     */
    public Mejnik izbrisi(Integer id) throws MejnikNeObstajaException {

        Assert.notNull(id, MESSAGE_ID_MEJNIKA_NI_PODAN);

        Mejnik mejnik = vrni(id);
        mejnikRepository.delete(mejnik);

        return mejnik;
    }

    /**
     * Preveri, ali oseba s podanim id obstaja.
     *
     * @param osebaId id osebe
     * @throws OsebaServiceException   če je prišlo do napake pri klicu
     * @throws OsebaNeObstajaException če oseba ne obstaja
     */
    public void preveriObstojOsebe(Integer osebaId) throws OsebaServiceException, OsebaNeObstajaException {

        OsebaOdgovor osebaOdgovor = osebaServiceKlient.vrni(osebaId);

        if (!osebaOdgovor.getNapaka().equals("")) {

            if (osebaOdgovor.getNapaka().equals("OsebaNeObstajaException")) {
                throw new OsebaNeObstajaException(osebaOdgovor.getNapakaSporocilo());
            } else {
                throw new OsebaServiceException(
                        String.format("Napaka pri vračanju osebe. Tip napake: %s. Sporočilo: %s",
                                osebaOdgovor.getNapaka(), osebaOdgovor.getNapakaSporocilo()));
            }
        }
    }

}
