package com.jrovtar.services.upravljalecdejavnostimejnik;

import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.MejnikDTO;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class PosodabljanjeMejnikDTOTests {

    private static Validator validator;
    private static Class[] group;

    @BeforeAll
    public static void setupValidatorInstance() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        group = new Class[]{MejnikDTO.ObPosodabljanju.class};
    }

    @Test
    public void idNull() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(null);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("id");
    }

    @Test
    public void nazivNull() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(1);
        mejnik.setNaziv(null);
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void nazivPrazen() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(1);
        mejnik.setNaziv("");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void nazivPredolg() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(1);
        mejnik.setNaziv(StringUtils.repeat("*", 256));
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("naziv");
    }

    @Test
    public void datumZakljuckaNull() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(1);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(null);
        mejnik.setZadolzenaOsebaId(1);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("datumZakljucka");
    }

    @Test
    public void zadolzenaOsebaIdNull() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(1);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(null);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(1);
        assertThat(violations.iterator().next().getPropertyPath().toString()).isEqualTo("zadolzenaOsebaId");
    }

    @Test
    public void ok() {

        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(1);
        mejnik.setNaziv(StringUtils.repeat("*", 255));
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        Set<ConstraintViolation<MejnikDTO>> violations =
                validator.validate(mejnik, group);

        assertThat(violations.size()).isEqualTo(0);
    }
}
