package com.jrovtar.services.upravljalecdejavnostimejnik;


import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.UsebaUtils;
import org.junit.Assert;
import org.junit.Test;

import java.time.DateTimeException;
import java.time.LocalDate;

public class UsebaUtilsTest {

    @Test
    public void kreirajLocalDate_ok(){

        LocalDate ld1 = LocalDate.of(2020, 1, 1);
        LocalDate ld2 = UsebaUtils.kreirajLocalDate(UsebaUtils.formatiraj(ld1));

        Assert.assertEquals(ld1, ld2);
    }

    @Test
    public void formatiraj_null(){

        String str = UsebaUtils.formatiraj(null);
        Assert.assertEquals("", str);
    }

    @Test
    public void formatiraj_ok(){

        String str1 = "01.01.2020";
        String str2 = UsebaUtils.formatiraj(UsebaUtils.kreirajLocalDate(str1));

        Assert.assertEquals(str1, str2);
    }

    @Test(expected = DateTimeException.class)
    public void kreirajLocalDate_nepravilenFormat(){

        UsebaUtils.kreirajLocalDate("01012020");
    }

    @Test(expected = IllegalArgumentException.class)
    public void kreirajLocalDate_prazenString(){

        UsebaUtils.kreirajLocalDate("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void kreirajLocalDate_null(){

        UsebaUtils.kreirajLocalDate(null);
    }
}
