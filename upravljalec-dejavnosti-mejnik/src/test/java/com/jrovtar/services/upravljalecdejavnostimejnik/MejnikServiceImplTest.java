package com.jrovtar.services.upravljalecdejavnostimejnik;

import com.jrovtar.services.upravljalecdejavnostimejnik.klienti.OsebaServiceKlient;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.*;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.MejnikNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaServiceException;
import com.jrovtar.services.upravljalecdejavnostiosebe.osebe.grpc.OsebaOdgovor;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.annotation.Validated;

import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
public class MejnikServiceImplTest {

    @MockBean
    private MejnikRepository mejnikRepository;

    @MockBean
    private OsebaServiceKlient osebaServiceKlient;

    private MejnikService mejnikService;

    @Before
    public void init() {
        mejnikService = new MejnikServiceImpl(mejnikRepository, osebaServiceKlient);
    }

    @SneakyThrows
    @Test
    public void vrni_ok() {

        // priprava testnih podatkov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Mejnik");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        // mock metod
        Mockito.when(mejnikRepository.findById(mejnik.getId()))
                .thenReturn(java.util.Optional.of(mejnik));

        // klic testirane metode
        Mejnik m = mejnikService.vrni(mejnik.getId());

        // preverjanje rezultatov
        Mockito.verify(mejnikRepository, Mockito.times(1)).findById(ArgumentMatchers.anyInt());
        Assert.assertEquals(mejnik.getId(), m.getId());
        Assert.assertEquals(mejnik.getNaziv(), m.getNaziv());
        Assert.assertEquals(mejnik.getDatumZakljucka(), m.getDatumZakljucka());
        Assert.assertEquals(mejnik.getZadolzenaOsebaId(), m.getZadolzenaOsebaId());
    }

    @SneakyThrows
    @Test(expected = MejnikNeObstajaException.class)
    public void vrni_neObstaja() {

        mejnikService.vrni(0);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void vrni_idNull() {

        mejnikService.vrni(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_ok() {

        // priprava testnih podatkov
        MejnikDTO dto = new MejnikDTO();
        dto.setNaziv("Naziv");
        dto.setDatumZakljucka(LocalDate.now());
        dto.setZadolzenaOsebaId(1);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> {
                    Mejnik mejnik = invocation.getArgument(0);
                    mejnik.setId(new Random().nextInt(10) + 1);
                    return mejnik;
                });

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        // klic testirane metode
        Mejnik m = mejnikService.dodaj(dto);

        // preverjanje rezultatov
        Mockito.verify(mejnikRepository, Mockito.times(1)).save(ArgumentMatchers.any(Mejnik.class));
        Assert.assertNotNull(m);
        Assert.assertNotNull(m.getId());
        Assert.assertEquals(dto.getNaziv(), m.getNaziv());
        Assert.assertEquals(dto.getZadolzenaOsebaId(), m.getZadolzenaOsebaId());
        Assert.assertEquals(dto.getDatumZakljucka(), m.getDatumZakljucka());
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void dodaj_osebaNeObstaja() {

        // priprava testnih podatkov
        MejnikDTO dto = new MejnikDTO();
        dto.setNaziv("Naziv");
        dto.setDatumZakljucka(LocalDate.now());
        dto.setZadolzenaOsebaId(1);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> {
                    Mejnik mejnik = invocation.getArgument(0);
                    mejnik.setId(new Random().nextInt(10) + 1);
                    return mejnik;
                });

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        mejnikService.dodaj(dto);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void dodaj_osebaServiceNapaka() {

        // klic testnih podatkov
        MejnikDTO dto = new MejnikDTO();
        dto.setNaziv("Naziv");
        dto.setDatumZakljucka(LocalDate.now());
        dto.setZadolzenaOsebaId(1);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> {
                    Mejnik mejnik = invocation.getArgument(0);
                    mejnik.setId(new Random().nextInt(10) + 1);
                    return mejnik;
                });

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("Exception").build()
                );

        // klic testirane metode
        mejnikService.dodaj(dto);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void dodaj_null() {
        mejnikService.dodaj(null);
    }

    @SneakyThrows
    @Test
    public void dodaj_brezAnotacijeValidated() {

        Method method = MejnikServiceImpl.class.getDeclaredMethod("dodaj", MejnikDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(MejnikDTO.ObDodajanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void posodobi_ok() {

        // priprava testnih podatkov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        MejnikDTO dto = MejnikDTO.kreirajDTO(mejnik);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> invocation.getArgument(0));

        Mockito.when(mejnikRepository.findById(mejnik.getId()))
                .thenReturn(java.util.Optional.of(mejnik));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        // klic testirane metode
        Mejnik m = mejnikService.posodobi(dto);

        // preverjanje rezultatov
        Mockito.verify(mejnikRepository, Mockito.times(1)).save(ArgumentMatchers.any(Mejnik.class));
        Assert.assertNotNull(m);
        Assert.assertNotNull(m.getId());
        Assert.assertEquals(dto.getId(), m.getId());
        Assert.assertEquals(dto.getNaziv(), m.getNaziv());
        Assert.assertEquals(dto.getZadolzenaOsebaId(), m.getZadolzenaOsebaId());
        Assert.assertEquals(dto.getDatumZakljucka(), m.getDatumZakljucka());
    }

    @SneakyThrows
    @Test(expected = MejnikNeObstajaException.class)
    public void posodobi_neObstaja() {

        // priprava testnih podatkov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        MejnikDTO dto = MejnikDTO.kreirajDTO(mejnik);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> invocation.getArgument(0));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        // klic testirane metode
        mejnikService.posodobi(dto);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void posodobi_null() {
        mejnikService.posodobi(null);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void posodobi_osebaNeObstaja() {

        // priprava testnih podatkov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        MejnikDTO dto = MejnikDTO.kreirajDTO(mejnik);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> invocation.getArgument(0));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        mejnikService.posodobi(dto);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void posodobi_osebaServiceNapaka() {

        // priprava testnih podatkov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Naziv");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        MejnikDTO dto = MejnikDTO.kreirajDTO(mejnik);

        // mock metod
        Mockito
                .when(mejnikRepository.save(ArgumentMatchers.any(Mejnik.class)))
                .thenAnswer((Answer<Mejnik>) invocation -> invocation.getArgument(0));

        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("Exception").build()
                );

        // klic testirane metode
        mejnikService.posodobi(dto);
    }

    @SneakyThrows
    @Test
    public void posodobi_brezAnotacijeValidated() {

        Method method = MejnikServiceImpl.class.getDeclaredMethod("posodobi", MejnikDTO.class);
        Validated annotation = method.getAnnotation(Validated.class);

        Assert.assertNotNull(annotation);
        Assert.assertEquals(1, annotation.value().length);
        Assert.assertEquals(MejnikDTO.ObPosodabljanju.class.getName(), annotation.value()[0].getName());
    }

    @SneakyThrows
    @Test
    public void preveriObstojOsebe_ok() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("").build()
                );

        // klic testirane metode
        mejnikService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test(expected = OsebaNeObstajaException.class)
    public void preveriObstojOsebe_osebaNeObstaja() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("OsebaNeObstajaException").build()
                );

        // klic testirane metode
        mejnikService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test(expected = OsebaServiceException.class)
    public void preveriObstojOsebe_osebaServiceNapaka() {

        // mock metod
        Mockito
                .when(osebaServiceKlient.vrni(ArgumentMatchers.anyInt()))
                .thenAnswer((Answer<OsebaOdgovor>) invocation ->
                        OsebaOdgovor.newBuilder().setNapaka("Exception").build()
                );
        // klic testirane metode
        mejnikService.preveriObstojOsebe(1);
    }

    @SneakyThrows
    @Test
    public void izbrisi_ok() {

        // priprava testnih podatkov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Mejnik");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(1);

        // mock metod
        Mockito.when(mejnikRepository.findById(mejnik.getId()))
                .thenReturn(java.util.Optional.of(mejnik));

        Mockito
                .doNothing()
                .when(mejnikRepository)
                .delete(ArgumentMatchers.any(Mejnik.class));

        // klic testirane metode
        Mejnik m = mejnikService.izbrisi(mejnik.getId());

        // preverjanje rezultatov
        Mockito.verify(mejnikRepository, Mockito.times(1)).delete(ArgumentMatchers.any(Mejnik.class));
        Assert.assertEquals(mejnik.getId(), m.getId());
        Assert.assertEquals(mejnik.getNaziv(), m.getNaziv());
        Assert.assertEquals(mejnik.getDatumZakljucka(), m.getDatumZakljucka());
        Assert.assertEquals(mejnik.getZadolzenaOsebaId(), m.getZadolzenaOsebaId());
    }

    @SneakyThrows
    @Test(expected = MejnikNeObstajaException.class)
    public void izbrisi_neObstaja() {

        mejnikService.izbrisi(0);
    }

    @SneakyThrows
    @Test(expected = IllegalArgumentException.class)
    public void izbrisi_idNull() {

        mejnikService.izbrisi(null);
    }

    @Test
    public void filtriraj_ok_filtriNull() {

        // priprava testnih podatkov
        Mejnik mejnik1 = new Mejnik();
        mejnik1.setId(1);
        mejnik1.setNaziv("Mejnik1");
        mejnik1.setDatumZakljucka(LocalDate.now());
        mejnik1.setZadolzenaOsebaId(1);

        Mejnik mejnik2 = new Mejnik();
        mejnik2.setId(2);
        mejnik2.setNaziv("Mejnik2");
        mejnik2.setDatumZakljucka(LocalDate.now());
        mejnik2.setZadolzenaOsebaId(1);

        List<Mejnik> list = List.of(mejnik1, mejnik2);

        String filterNaziv = null;
        Integer filterOseba = null;

        // mock metod
        Mockito
                .when(mejnikRepository.filtriraj(filterNaziv, filterOseba))
                .thenAnswer((Answer<List<Mejnik>>) invocation ->
                        list
                );

        // klic testirane metode
        List<Mejnik> vrnjeniMejniki = mejnikService.filriraj(filterNaziv, filterOseba);

        // preverjanje rezultatov
        Mockito.verify(mejnikRepository, Mockito.times(1)).filtriraj(filterNaziv, filterOseba);
        Assert.assertEquals(list.size(), vrnjeniMejniki.size());

        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(list.get(i).getId(), vrnjeniMejniki.get(i).getId());
            Assert.assertEquals(list.get(i).getNaziv(), vrnjeniMejniki.get(i).getNaziv());
            Assert.assertEquals(list.get(i).getDatumZakljucka(), vrnjeniMejniki.get(i).getDatumZakljucka());
            Assert.assertEquals(list.get(i).getZadolzenaOsebaId(), vrnjeniMejniki.get(i).getZadolzenaOsebaId());
        }
    }

    @Test
    public void filtriraj_ok_filtriNotNull() {

        // priprava testnih podatkov
        Mejnik mejnik1 = new Mejnik();
        mejnik1.setId(1);
        mejnik1.setNaziv("Mejnik1");
        mejnik1.setDatumZakljucka(LocalDate.now());
        mejnik1.setZadolzenaOsebaId(1);

        Mejnik mejnik2 = new Mejnik();
        mejnik2.setId(2);
        mejnik2.setNaziv("Mejnik2");
        mejnik2.setDatumZakljucka(LocalDate.now());
        mejnik2.setZadolzenaOsebaId(1);

        List<Mejnik> list = List.of(mejnik1, mejnik2);

        String filterNaziv = "Mejnik";
        Integer filterOseba = 1;

        // mock metod
        Mockito
                .when(mejnikRepository.filtriraj(filterNaziv, filterOseba))
                .thenAnswer((Answer<List<Mejnik>>) invocation ->
                        list
                );

        // klic testirane metode
        List<Mejnik> vrnjeniMejniki = mejnikService.filriraj(filterNaziv, filterOseba);

        // preverjanje rezultatov
        Mockito.verify(mejnikRepository, Mockito.times(1)).filtriraj(ArgumentMatchers.anyString(), ArgumentMatchers.anyInt());
        Assert.assertEquals(list.size(), vrnjeniMejniki.size());

        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(list.get(i).getId(), vrnjeniMejniki.get(i).getId());
            Assert.assertEquals(list.get(i).getNaziv(), vrnjeniMejniki.get(i).getNaziv());
            Assert.assertEquals(list.get(i).getDatumZakljucka(), vrnjeniMejniki.get(i).getDatumZakljucka());
            Assert.assertEquals(list.get(i).getZadolzenaOsebaId(), vrnjeniMejniki.get(i).getZadolzenaOsebaId());
        }
    }

    @Test
    public void filtriraj_ok_prazenNaziv() {

        // priprava testnih podatkov
        Mejnik mejnik1 = new Mejnik();
        mejnik1.setId(1);
        mejnik1.setNaziv("Mejnik1");
        mejnik1.setDatumZakljucka(LocalDate.now());
        mejnik1.setZadolzenaOsebaId(1);

        Mejnik mejnik2 = new Mejnik();
        mejnik2.setId(2);
        mejnik2.setNaziv("Mejnik2");
        mejnik2.setDatumZakljucka(LocalDate.now());
        mejnik2.setZadolzenaOsebaId(1);

        List<Mejnik> list = List.of(mejnik1, mejnik2);

        // mock metod
        Mockito
                .when(mejnikRepository.filtriraj(null, null))
                .thenAnswer((Answer<List<Mejnik>>) invocation ->
                        list
                );

        // klic testirane metode
        List<Mejnik> vrnjeniMejniki = mejnikService.filriraj("", null);

        // preverjanje rezultatov
        Assert.assertEquals(list.size(), vrnjeniMejniki.size());

        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(list.get(i).getId(), vrnjeniMejniki.get(i).getId());
            Assert.assertEquals(list.get(i).getNaziv(), vrnjeniMejniki.get(i).getNaziv());
            Assert.assertEquals(list.get(i).getDatumZakljucka(), vrnjeniMejniki.get(i).getDatumZakljucka());
            Assert.assertEquals(list.get(i).getZadolzenaOsebaId(), vrnjeniMejniki.get(i).getZadolzenaOsebaId());
        }
    }

    @Test
    public void filtriraj_ok_negativenOsebaId() {

        // priprava testnih podatkov
        Mejnik mejnik1 = new Mejnik();
        mejnik1.setId(1);
        mejnik1.setNaziv("Mejnik1");
        mejnik1.setDatumZakljucka(LocalDate.now());
        mejnik1.setZadolzenaOsebaId(1);

        Mejnik mejnik2 = new Mejnik();
        mejnik2.setId(2);
        mejnik2.setNaziv("Mejnik2");
        mejnik2.setDatumZakljucka(LocalDate.now());
        mejnik2.setZadolzenaOsebaId(1);

        List<Mejnik> list = List.of(mejnik1, mejnik2);

        // mock metod
        Mockito
                .when(mejnikRepository.filtriraj(null, null))
                .thenAnswer((Answer<List<Mejnik>>) invocation ->
                        list
                );

        // klic testirane metode
        List<Mejnik> vrnjeniMejniki = mejnikService.filriraj(null, -1);

        // preverjanje rezultatov
        Assert.assertEquals(list.size(), vrnjeniMejniki.size());

        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(list.get(i).getId(), vrnjeniMejniki.get(i).getId());
            Assert.assertEquals(list.get(i).getNaziv(), vrnjeniMejniki.get(i).getNaziv());
            Assert.assertEquals(list.get(i).getDatumZakljucka(), vrnjeniMejniki.get(i).getDatumZakljucka());
            Assert.assertEquals(list.get(i).getZadolzenaOsebaId(), vrnjeniMejniki.get(i).getZadolzenaOsebaId());
        }
    }
}
