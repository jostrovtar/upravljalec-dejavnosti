package com.jrovtar.services.upravljalecdejavnostimejnik;

import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.*;
import com.jrovtar.services.upravljalecdejavnostimejnik.mejnik.grpc.*;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.MejnikNeObstajaException;
import com.jrovtar.services.upravljalecdejavnostimejnik.napake.OsebaNeObstajaException;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.context.LocalRunningGrpcPort;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {
        "klienti.osebaServicePort=6565",
        "klienti.osebaServiceUrl=localhost"
})
public class GrpcApiTest {

    @LocalRunningGrpcPort
    protected int runningPort;

    private ManagedChannel channel;

    @MockBean
    private MejnikService mejnikService;

    private MejnikServiceGrpc.MejnikServiceFutureStub mejnikServiceFutureStub = null;

    @Before
    public final void init() {
        channel = ManagedChannelBuilder.forAddress("localhost", runningPort)
                .usePlaintext()
                .build();

        mejnikServiceFutureStub = MejnikServiceGrpc.newFutureStub(channel);
    }

    @After
    public final void shutdownChannels() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @SneakyThrows
    @Test
    final public void vrni_ok() {

        // priprava testnih objektov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Mejnik");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(2);

        // mock metode
        Mockito
                .when(mejnikService.vrni(mejnik.getId()))
                .thenAnswer((Answer<Mejnik>) invocation -> {
                    Mejnik m = new Mejnik();
                    m.setId(invocation.getArgument(0));
                    m.setNaziv(mejnik.getNaziv());
                    m.setDatumZakljucka(mejnik.getDatumZakljucka());
                    m.setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId());
                    return m;
                });

        // klic testirane metode
        MejnikVrniZahteva mejnikVrniZahteva = MejnikVrniZahteva.newBuilder().setId(mejnik.getId()).build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.vrni(mejnikVrniZahteva).get();

        // preverjanje odgovora
        Assert.assertTrue(!(mejnikOdgovor.getId() == 0));
        Assert.assertEquals(mejnik.getId(), Integer.valueOf(mejnikOdgovor.getId()));
        Assert.assertEquals(mejnik.getNaziv(), mejnikOdgovor.getNaziv());
        Assert.assertEquals(UsebaUtils.formatiraj(mejnik.getDatumZakljucka()), mejnikOdgovor.getDatumZakljucka());
        Assert.assertEquals(mejnik.getZadolzenaOsebaId(), Integer.valueOf(mejnikOdgovor.getZadolzenaOsebaId()));
        Assert.assertEquals("", mejnikOdgovor.getNapaka());
        Assert.assertEquals("", mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void vrni_napaka() {

        // priprava testnih podatkov
        Integer id = 1;

        Exception exception = new MejnikNeObstajaException("Mejnik ne obstaja");

        // mock metode
        Mockito
                .when(mejnikService.vrni(id))
                .thenThrow(exception);

        // klic metode
        MejnikVrniZahteva mejnikVrniZahteva = MejnikVrniZahteva.newBuilder().setId(id).build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.vrni(mejnikVrniZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals(Integer.valueOf(0), Integer.valueOf(mejnikOdgovor.getId()));
        Assert.assertEquals("", mejnikOdgovor.getNaziv());
        Assert.assertEquals("", mejnikOdgovor.getDatumZakljucka());
        Assert.assertEquals(Integer.valueOf(0), Integer.valueOf(mejnikOdgovor.getZadolzenaOsebaId()));
        Assert.assertEquals(exception.getClass().getSimpleName(), mejnikOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_ok() {

        // priprava testnih objektov
        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setNaziv("Mejnik");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(2);

        // mock metode
        Mockito
                .when(mejnikService.dodaj(ArgumentMatchers.any()))
                .thenAnswer((Answer<Mejnik>) invocation -> {

                    MejnikDTO dto = invocation.getArgument(0);

                    Mejnik m = new Mejnik();
                    m.setId(new Random().nextInt(10) + 1);
                    m.setNaziv(dto.getNaziv());
                    m.setDatumZakljucka(dto.getDatumZakljucka());
                    m.setZadolzenaOsebaId(dto.getZadolzenaOsebaId());

                    return m;
                });

        // klic testirane metode
        MejnikDodajZahteva mejnikDodajZahteva = MejnikDodajZahteva.newBuilder()
                .setNaziv(mejnik.getNaziv())
                .setDatumZakljucka(UsebaUtils.formatiraj(mejnik.getDatumZakljucka()))
                .setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId())
                .build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.dodaj(mejnikDodajZahteva).get();

        // klic testirane metode
        Assert.assertTrue(!(mejnikOdgovor.getId() == 0));
        Assert.assertEquals(mejnik.getNaziv(), mejnikOdgovor.getNaziv());
        Assert.assertEquals(UsebaUtils.formatiraj(mejnik.getDatumZakljucka()), mejnikOdgovor.getDatumZakljucka());
        Assert.assertEquals(mejnik.getZadolzenaOsebaId(), Integer.valueOf(mejnikOdgovor.getZadolzenaOsebaId()));
        Assert.assertEquals("", mejnikOdgovor.getNapaka());
        Assert.assertEquals("", mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void dodaj_napaka() {

        // priprava testnih podatkov
        OsebaNeObstajaException exception =
                new OsebaNeObstajaException("Oseba ne obstaja");

        // mock metode
        Mockito
                .when(mejnikService.dodaj(ArgumentMatchers.any()))
                .thenThrow(exception);

        // klic testirane metode
        MejnikDodajZahteva mejnikDodajZahteva = MejnikDodajZahteva.newBuilder()
                .setNaziv("Naziv")
                .setDatumZakljucka(UsebaUtils.formatiraj(LocalDate.now()))
                .setZadolzenaOsebaId(0)
                .build();

        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.dodaj(mejnikDodajZahteva).get();

        // preverjanje odgovora
        Assert.assertTrue(mejnikOdgovor.getId() == 0);
        Assert.assertEquals("", mejnikOdgovor.getNaziv());
        Assert.assertEquals("", mejnikOdgovor.getDatumZakljucka());
        Assert.assertTrue(0 == mejnikOdgovor.getZadolzenaOsebaId());
        Assert.assertEquals(exception.getClass().getSimpleName(), mejnikOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_ok() {

        // priprava testnih objektov
        MejnikDTO mejnik = new MejnikDTO();
        mejnik.setId(new Random().nextInt(10) + 1);
        mejnik.setNaziv("Mejnik");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(new Random().nextInt(10) + 1);

        // mock metode
        Mockito
                .when(mejnikService.posodobi(ArgumentMatchers.any()))
                .thenAnswer((Answer<Mejnik>) invocation -> {

                    MejnikDTO dto = invocation.getArgument(0);

                    Mejnik m = new Mejnik();
                    m.setId(dto.getId());
                    m.setNaziv(dto.getNaziv());
                    m.setDatumZakljucka(dto.getDatumZakljucka());
                    m.setZadolzenaOsebaId(dto.getZadolzenaOsebaId());

                    return m;
                });

        // klic testirane metode
        MejnikPosodobiZahteva mejnikPosodobiZahteva = MejnikPosodobiZahteva.newBuilder()
                .setId(mejnik.getId())
                .setNaziv(mejnik.getNaziv())
                .setDatumZakljucka(UsebaUtils.formatiraj(mejnik.getDatumZakljucka()))
                .setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId())
                .build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.posodobi(mejnikPosodobiZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals(mejnik.getId(), Integer.valueOf(mejnikOdgovor.getId()));
        Assert.assertEquals(mejnik.getNaziv(), mejnikOdgovor.getNaziv());
        Assert.assertEquals(UsebaUtils.formatiraj(mejnik.getDatumZakljucka()), mejnikOdgovor.getDatumZakljucka());
        Assert.assertEquals(mejnik.getZadolzenaOsebaId(), Integer.valueOf(mejnikOdgovor.getZadolzenaOsebaId()));
        Assert.assertEquals("", mejnikOdgovor.getNapaka());
        Assert.assertEquals("", mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void posodobi_napaka() {

        // priprava testnih podatkov
        Integer id = 1;

        Exception exception = new MejnikNeObstajaException(
                String.format(MejnikServiceImpl.MESSAGE_MEJNIK_NE_OBSTAJA, id));

        // mock metode
        Mockito
                .when(mejnikService.posodobi(ArgumentMatchers.any()))
                .thenThrow(exception);


        // klic testirane metode
        MejnikPosodobiZahteva mejnikZahteva = MejnikPosodobiZahteva.newBuilder()
                .setId(id)
                .setNaziv("Naziv")
                .setZadolzenaOsebaId(0)
                .setDatumZakljucka(UsebaUtils.formatiraj(LocalDate.now()))
                .build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.posodobi(mejnikZahteva).get();

        // preverjanje odgovora
        Assert.assertTrue(0 == mejnikOdgovor.getId());
        Assert.assertEquals("", mejnikOdgovor.getNaziv());
        Assert.assertEquals("", mejnikOdgovor.getDatumZakljucka());
        Assert.assertTrue(0 == mejnikOdgovor.getZadolzenaOsebaId());
        Assert.assertEquals(exception.getClass().getSimpleName(), mejnikOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_ok() {

        // priprava testnih objektov
        Mejnik mejnik = new Mejnik();
        mejnik.setId(1);
        mejnik.setNaziv("Mejnik");
        mejnik.setDatumZakljucka(LocalDate.now());
        mejnik.setZadolzenaOsebaId(2);

        // mock metode
        Mockito
                .when(mejnikService.izbrisi(mejnik.getId()))
                .thenAnswer((Answer<Mejnik>) invocation -> {
                    Mejnik m = new Mejnik();
                    m.setId(invocation.getArgument(0));
                    m.setNaziv(mejnik.getNaziv());
                    m.setDatumZakljucka(mejnik.getDatumZakljucka());
                    m.setZadolzenaOsebaId(mejnik.getZadolzenaOsebaId());
                    return m;
                });

        // klic testirane metode
        MejnikIzbrisiZahteva mejnikIzbrisiZahteva = MejnikIzbrisiZahteva.newBuilder().setId(mejnik.getId()).build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.izbrisi(mejnikIzbrisiZahteva).get();

        // preverjanje odgovora
        Assert.assertEquals(mejnik.getId(), Integer.valueOf(mejnikOdgovor.getId()));
        Assert.assertEquals(mejnik.getNaziv(), mejnikOdgovor.getNaziv());
        Assert.assertEquals(UsebaUtils.formatiraj(mejnik.getDatumZakljucka()), mejnikOdgovor.getDatumZakljucka());
        Assert.assertEquals(mejnik.getZadolzenaOsebaId(), Integer.valueOf(mejnikOdgovor.getZadolzenaOsebaId()));
        Assert.assertEquals("", mejnikOdgovor.getNapaka());
        Assert.assertEquals("", mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void izbrisi_napaka() {

        // priprava testnih podatkov
        Integer id = 1;

        Exception exception = new MejnikNeObstajaException(
                String.format(MejnikServiceImpl.MESSAGE_MEJNIK_NE_OBSTAJA, id));

        // mock metode
        Mockito
                .when(mejnikService.izbrisi(ArgumentMatchers.any()))
                .thenThrow(exception);


        // klic testirane metode
        MejnikIzbrisiZahteva mejnikZahteva = MejnikIzbrisiZahteva.newBuilder()
                .setId(id)
                .build();
        MejnikOdgovor mejnikOdgovor = mejnikServiceFutureStub.izbrisi(mejnikZahteva).get();

        // preverjanje odgovora
        Assert.assertTrue(0 == mejnikOdgovor.getId());
        Assert.assertEquals("", mejnikOdgovor.getNaziv());
        Assert.assertEquals("", mejnikOdgovor.getDatumZakljucka());
        Assert.assertTrue(0 == mejnikOdgovor.getZadolzenaOsebaId());
        Assert.assertEquals(exception.getClass().getSimpleName(), mejnikOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), mejnikOdgovor.getNapakaSporocilo());
    }

    @SneakyThrows
    @Test
    final public void filtriraj_ok() {

        // priprava testnih objektov
        Mejnik mejnik1 = new Mejnik();
        mejnik1.setNaziv("Mejnik1");

        Mejnik mejnik2 = new Mejnik();
        mejnik2.setNaziv("Mejnik2");

        List<Mejnik> list = List.of(mejnik1, mejnik2);

        String filterNaziv = "";
        Integer filterZadolzenaOsebaId = 0;

        // mock metode
        Mockito
                .when(mejnikService.filriraj(filterNaziv, filterZadolzenaOsebaId))
                .thenAnswer((Answer<List<Mejnik>>) invocation -> list);

        // klic testirane metode
        MejnikFiltrirajZahteva mejnikFiltrirajZahteva = MejnikFiltrirajZahteva.newBuilder()
                .setFilterNaziv(filterNaziv)
                .setFilterZadolzenaOseba(filterZadolzenaOsebaId)
                .build();
        MejnikFiltrirajOdgovor mejnikOdgovor = mejnikServiceFutureStub.filtriraj(mejnikFiltrirajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(list.size(), mejnikOdgovor.getItemsCount());
        Assert.assertEquals("", mejnikOdgovor.getNapaka());
        Assert.assertEquals("", mejnikOdgovor.getNapakaSporocilo());

        for (int i = 0; i < list.size(); i++) {
            Assert.assertEquals(list.get(i).getNaziv(), mejnikOdgovor.getItems(i).getNaziv());
        }
    }

    @SneakyThrows
    @Test
    final public void filtriraj_napaka() {

        // priprava testnih objektov
        String filterNaziv = "";
        Integer filterZadolzenaOsebaId = 0;

        Exception exception = new IllegalArgumentException("Napaka");

        // mock metode
        Mockito
                .when(mejnikService.filriraj(filterNaziv, filterZadolzenaOsebaId))
                .thenThrow(exception);

        // klic testirane metode
        MejnikFiltrirajZahteva mejnikFiltrirajZahteva = MejnikFiltrirajZahteva.newBuilder()
                .setFilterNaziv(filterNaziv)
                .setFilterZadolzenaOseba(filterZadolzenaOsebaId)
                .build();
        MejnikFiltrirajOdgovor mejnikOdgovor = mejnikServiceFutureStub.filtriraj(mejnikFiltrirajZahteva).get();

        // preverjanje rezultatov
        Assert.assertEquals(0, mejnikOdgovor.getItemsCount());
        Assert.assertEquals(exception.getClass().getSimpleName(), mejnikOdgovor.getNapaka());
        Assert.assertEquals(exception.getMessage(), mejnikOdgovor.getNapakaSporocilo());
    }
}
